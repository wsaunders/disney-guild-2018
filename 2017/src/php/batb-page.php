	<?php if(!$isMobile || $isTablet) { ?>
	<div class="bg-holder">
		<div class="bg one"></div>
		<div class="bg synopsis"></div>
		<div class="bg cast"></div>
		<div class="bg filmmakers">
			<div class="bg condon"></div>
			<div class="bg hoberman"></div>
			<div class="bg lieberman"></div>
			<div class="bg tobias"></div>
			<div class="bg katz"></div>
		</div>
		<div class="bg music"></div>
		<div class="bg press"></div>
		<div class="bg makingof"></div>
		<div class="bg screenplay"></div>
	</div> 
	<?php } ?>
	<div class="limiter">
		<div class="consider">
			<div class="quote">
					<img src="/img/batb/fyc-quote.png"/>
				</div>
			<div class="content">

				<div class="logo">
					<img src="/img/batb/batb-tt.png" alt="Beauty and the Beast"/>
				</div>
				<div class="first">
					<p>FOR YOUR CONSIDERATION IN ALL CATEGORIES</p>
					<h3>BEST PICTURE</h3>
					<div class="subline">PRODUCED BY</div>
					<div class="name">DAVID HOBERMAN, <span class="guild">p.g.a.</span><br/>TODD LIEBERMAN, <span class="guild">p.g.a.</span></div>
				</div>
				<div class="left">
					<h3>BEST DIRECTOR</h3>
					<div class="name">BILL CONDON</div>
					<h3>BEST ADAPTED SCREENPLAY</h3>
					<div class="name">STEPHEN CHBOSKY<div class="and">AND</div>EVAN SPILIOTOPOULOS</div>
					<h3>BEST ACTRESS</h3>
					<div class="name">EMMA WATSON</div>
					<h3>BEST ACTOR</h3>
					<div class="name">DAN STEVENS</div>
					<h3>BEST SUPPORTING ACTOR</h3>
					<div class="name">
						LUKE EVANS<br/>
						KEVIN KLINE<br/>
						JOSH GAD<br/>
						EWAN McGREGOR<br/>
						STANLEY TUCCI<br/>
						IAN McKELLEN<br/>
					</div>
					<h3>BEST SUPPORTING ACTRESS</h3>
					<div class="name">
						AUDRA McDONALD<br/>
						GUGU MBATHA-RAW<br/>
						EMMA THOMPSON
					</div>
					<h3>BEST CINEMATOGRAPHY</h3>
					<div class="name">TOBIAS SCHLIESSLER, <span class="guild">ASC</span></div>
					<h3>BEST FILM EDITING</h3>
					<div class="name">VIRGINIA KATZ, <span class="guild">ACE</span></div>
					<h3>BEST PRODUCTION DESIGN</h3>
					<div class="subline">PRODUCTION DESIGNER</div>
					<div class="name">SARAH GREENWOOD</div>
					<div class="subline">SET DECORATOR</div>
					<div class="name">KATIE SPENCER</div>
					
				</div>
				<div class="right">
					<h3>BEST COSTUME DESIGN</h3>
					<div class="name">JACQUELINE DURRAN</div>
					<h3>BEST MAKEUP AND HAIRSTYLING</h3>
					<div class="name">JENNY SHIRCORE</div>
					<h3>BEST SOUND MIXING</h3>
					<div class="subline">RE-RECORDING MIXERS</div>
					<div class="name">MICHAEL MINKLER, <span class="guild">CAS</span></div>
					<div class="name">CHRISTIAN P. MINKLER</div>
					<div class="subline">SOUND MIXER</div>
					<div class="name">JOHN CASALI, <span class="guild">AMPS</span></div>
					<h3>BEST SOUND EDITING</h3>
					<div class="subline">SUPERVISING SOUND EDITOR/SOUND DESIGNER</div>
					<div class="name">WARREN SHAW</div>
					<h3>BEST VISUAL EFFECTS</h3>
					<div class="name">
						KYLE McCULLOCH<br/>
						KELLY PORT<br/>
						STEVE PREEG<br/>
						GLEN PRATT<br/>
					</div>
					<h3>BEST ORIGINAL SCORE</h3>
					<div class="name">ALAN MENKEN</div>
					<h3>BEST ORIGINAL SONG</h3>
					<div class="song">"EVERMORE"</div>
					<div class="subline">MUSIC BY</div>
					<div class='name'>ALAN MENKEN</div>
					<div class="subline">LYRICS BY</div>
					<div class="name">TIM RICE</div>
					<div class="song down5">"HOW DOES A MOMENT<br/>LAST FOREVER"</div>
					<div class="subline">MUSIC BY</div>
					<div class="name">ALAN MENKEN</div>
					<div class="subline">LYRICS BY</div>
					<div class="name">TIM RICE</div>
				</div>
			</div>
		</div>
		<div class="screenings">
			<div class="special-bg">
				<div class="quote"><img src="/img/batb/synopsis-quote.png"/></div>
			</div>
			<div class="thumb one">
				<div class="image">
					<img src='/img/batb/batb-synopsis-image-1.jpg'/>
				</div>
			</div>
			<div class="thumb two">
				<div class="image">
					<img src='/img/batb/batb-synopsis-image-2.jpg'/>
				</div>
			</div>
			<div class="thumb three">
				<div class="image">
					<img src='/img/batb/batb-synopsis-image-3.jpg'/>
				</div>
			</div>
			<div class="content">
				<!-- <img class="loader" src="/img/loader.gif"> -->
				<div class="scrollable">
					<div class="quote">
						<img src="/img/batb/synopsis-quote.png">
					</div>
					<div class="synopsis">
						<p>The story and characters audiences know and love come to spectacular life in Disney’s live-action adaptation “Beauty and the Beast,” a stunning, cinematic event celebrating one of the most beloved tales ever told. “Beauty and the Beast” is the fantastic journey of Belle, a bright, beautiful and independent young woman who is taken prisoner by a Beast in his castle. Despite her fears, she befriends the castle’s enchanted staff and learns to look beyond the Beast’s hideous exterior and realize the kind heart of the true Prince within.
							</p>
						<div id="batb-synopsis" class=" openOverlay read-more">READ MORE</div>
					</div>
					<h3>UPCOMING SCREENINGS</h3>
					<div class="screenings-holder">
						
					</div>
					<div class="disclaimer">You must be an invited member of a voting organization to attend <span class="nowrap">For Your Consideration screenings</span>. Your membership card is required for entry.</div>
					<div class="netflix">NOW AVAILABLE ON NETFLIX</div>
				</div>
			</div>
		</div>
		<div class="cast">
			<?php if(!$isMobile && !$isTablet) { ?>
			<div class="cast-bgs">
				<div class="cast-bg" style="background-image:url(/img/batb/cast/emma-bg.jpg);"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/dan-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/luke-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/kevin-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/josh-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/ewan-bg.jpg)"></div>		
				<div class="cast-bg" style="background-image:url(/img/batb/cast/tucci-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/audra-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/gugu-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/ian-bg.jpg)"></div>
				<div class="cast-bg" style="background-image:url(/img/batb/cast/emmaT-bg.jpg)"></div>
			</div>
			<?php } ?>
			<div class="quote"><img src="/img/batb/cast/cast-quote.png"></div>
			<div class="article">
				<a target="_blank" href="/media/pdf/enchanting_ensemble.pdf"><div><img src="/img/batb/cast/cast-article-image.jpg"/></div>
				<div class="title">ENTERTAINMENT WEEKLY:<span> “AN ENCHANTING ENSEMBLE”</span></div>
				<div class="read">VIEW ARTICLE</div></a>
			</div>
			<?php if(!$isMobile && !$isTablet) { ?>
			<div class="severed-heads">
				<div class="head" data-cast="Emma Watson"><img src="/img/batb/cast/emma-dot.png"></div>
				<div class="head" data-cast="Dan Stevens"><img src="/img/batb/cast/dan-dot.png"></div>
				<div class="head" data-cast="Luke Evans"><img src="/img/batb/cast/luke-dot.png"></div>
				<div class="head" data-cast="Kevin Kline"><img src="/img/batb/cast/kevin-dot.png"></div>
				<div class="head" data-cast="Josh Gad"><img src="/img/batb/cast/josh-dot.png"></div>
				<div class="head" data-cast="Ewan McGregor"><img src="/img/batb/cast/ewan-dot.png"></div>		
				<div class="head" data-cast="Stanley Tucci"><img src="/img/batb/cast/tucci-dot.png"></div>
				<div class="head" data-cast="Audra McDonald"><img src="/img/batb/cast/audra-dot.png"></div>
				<div class="head" data-cast="Gugu Mbatha-Raw"><img src="/img/batb/cast/gugu-dot.png"></div>
				<div class="head" data-cast="Ian McKellen"><img src="/img/batb/cast/ian-dot.png"></div>
				<div class="head" data-cast="Emma Thompson"><img src="/img/batb/cast/emmaT-dot.png"></div>
				
			</div>
			<?php } ?>
		</div>
		<div class="filmmakers">	
			<div class="bottom-quote">
				<p>“CONDON ALSO BRINGS HIS EXPERIENCE TO THE TABLE <span>&nbsp;&nbsp;</span><br/>FOR THE BIG MUSICAL NUMBERS, WHICH ARE AMONG THE <span>&nbsp;&nbsp;</span><br/>BEST BITS OF FILM, ESPECIALLY ‘GASTON,’ THE <span>&nbsp;&nbsp;</span><br/>LeFOU-LED TRIBUTE TO OUR BOASTFUL VILLAIN. FILMED <span>&nbsp;&nbsp;</span><br/>REFRESHINGLY STRAIGHT, IN A SERIES OF WIDE, STABLE <span>&nbsp;&nbsp;</span><br/>SHOTS THAT ESCHEW THE FIDGETY EDITING OF MOST POP <span>&nbsp;&nbsp;</span><br/>VIDEOS IN FAVOR OF AN OLD-FASHIONED, MGM-STYLE <span>&nbsp;&nbsp;</span><br/>PROSCENIUM SPACE, IT'S A DELICIOUS MOMENT, <span>&nbsp;&nbsp;</span><br/>TRADITIONAL IN ALL THE RIGHT WAYS.”<span>&nbsp;&nbsp;</span></p>
				<div class="author" style="">- LESLIE FELPERIN, 
					<img class="auth-logo" style="width:90px;" src="/img/press/quote-thr.png">
				</div>
			</div>
			<div class="content">
				<div class="quote">
					<div>
						<p>“‘Beauty and the Beast’ has the feeling of old-fashioned Hollywood<br><span> &nbsp;&nbsp;</span>grandeur, calling to mind Technicolor extravaganzas such as ‘The Sound of<br><span> &nbsp;&nbsp;</span>Music’ even as it’s loaded with the best of modern effects and techniques.<br><span> &nbsp;&nbsp;</span>In one scene, a gorgeous realization of a meeting between old and new,<br><span> &nbsp;&nbsp;</span>the various pots and objects create a Busby Berkeley-like spectacle on<br><span> &nbsp;&nbsp;</span>the dining room table. The scene is more than clever, more than a historical<br><span> &nbsp;&nbsp;</span>reference. It’s poetry. ’Beauty and the Beast’ creates an air of enchantment<br><span> &nbsp;&nbsp;</span>from its first moments, one that lingers and builds and takes on qualities<br><span> &nbsp;&nbsp;</span>of warmth and generosity as it goes along. It’s a beautiful movie, both in<br><span> &nbsp;&nbsp;</span>look and spirit, one of the joys of 2017.”</p>

						<div class="author">- MICK LaSALLE, <img class="auth-logo" src="/img/press/quote-san-fran.png"></div>
					</div>
					<div>
						<p>“‘Beauty and the Beast’ creates an air of enchantment<br/> <span>&nbsp;&nbsp;</span>from its first moments, one that lingers and builds and<br/> <span>&nbsp;&nbsp;</span>takes on qualities of warmth and generosity as it<br/> <span>&nbsp;&nbsp;</span>goes along. It’s a beautiful movie, both in look and<br/> <span>&nbsp;&nbsp;</span>spirit, one of the joys of 2017.”</p>
						<div class="author">- MICK LaSALLE, <img class="auth-logo" src="/img/press/quote-san-fran.png"></div>
					</div>
					<div>
						<p>“It’s worth mentioning here because of the nature<br/> <span>&nbsp;&nbsp;</span>of the happiness, that it’s happiness for everybody.<br/> <span>&nbsp;&nbsp;</span>This is Condon’s vision of a fairy tale ending,<br/> <span>&nbsp;&nbsp;</span>one of happiness and inclusion.”</p>
						<div class="author">- MICK LaSALLE, <img class="auth-logo" src="/img/press/quote-san-fran.png"></div>
					</div>
					<div>
						<p>“‘Beauty and the Beast’ has the feeling of old-fashioned<br/> <span>&nbsp;&nbsp;</span>Hollywood grandeur, calling to mind Technicolor<br/> <span>&nbsp;&nbsp;</span>extravaganzas such as ‘The Sound of Music.’ In one scene, a<br/> <span>&nbsp;&nbsp;</span>gorgeous realization of a meeting between old and new,<br/> <span>&nbsp;&nbsp;</span>the various pots and objects create a Busby Berkeley-like<br/> <span>&nbsp;&nbsp;</span>spectacle on 
						the dining room table to one of the many<br/> <span>&nbsp;&nbsp;</span>lively songs by Alan Menken and Howard Ashman.<br/> <span>&nbsp;&nbsp;</span>The scene is more than clever, more than a historical<br/> <span>&nbsp;&nbsp;</span>reference. It’s poetry.” 
						</p>

						<div class="author">- MICK LaSALLE, <img class="auth-logo" src="/img/press/quote-san-fran.png"></div>
					</div>
					<div>
						<p>“It’s worth mentioning here because of the nature<br/> <span>&nbsp;&nbsp;</span>of the happiness, that it’s happiness for everybody.<br/> <span>&nbsp;&nbsp;</span>This is Condon’s vision of a fairy tale ending,<br/> <span>&nbsp;&nbsp;</span>one of happiness and inclusion.”</p>
						<div class="author">- MICK LaSALLE, <img class="auth-logo" src="/img/press/quote-san-fran.png"></div>
					</div>
				</div>
				<div class="name-list">
					<span class="name current">BILL CONDON<span> | </span></span>
					<span class="name">DAVID HOBERMAN<span> | </span></span>
					<span class="name">TODD LIEBERMAN<span> | </span></span>
					<span class="name">TOBIAS SCHLIESSLER<span> | </span></span>
					<span class="name">VIRGINA KATZ</span>
				</div>
				<div class="bio" id="condon-bio">
					<div class="name">BILL CONDON (DIRECTOR)</div>
					<div class="text">
						<div class="scrollable">
							<p><strong>BILL CONDON (Director)</strong> is a celebrated film director and Oscar®-winning screenwriter. His film adaptation of the Broadway smash “Dreamgirls” won two Academy Awards® and three Golden Globes, including Best Motion Picture – Musical or Comedy. Condon directed from his own screenplay and was nominated for a Directors Guild of America Award. </p>
							<p>He wrote and directed “Gods and Monsters,” which earned Condon an Academy Award® for Best Adapted Screenplay. He also wrote the screenplay for the big-screen version of the musical “Chicago,” for which he received a second Oscar® nomination. His most recent projects include the drama “Mr. Holmes,” and on stage, the celebrated revival of the musical “Side Show.”</p>
						</div>
					</div>
				</div>
				<div class="bio" id="hoberman-bio">
					<div class="name">DAVID HOBERMAN, <span class="guild">p.g.a.</span> (PRODUCER)</div>
					<div class="text">
						<div class="scrollable">
							<p>DAVID HOBERMAN, p.g.a. (Producer) is the founder and co-owner of Mandeville Films and Television. He is one of the leading producers in the entertainment industry today, having made his mark on more than 100 movies. His Disney-based company is among the most profitable and respected production labels in the entertainment industry.</p>
							<p>Since its founding in 1995, Mandeville Films has produced feature films that have grossed more than $1 billion in domestic box office receipts. Mandeville partners David Hoberman and Todd Lieberman produced the critically-hailed Academy Award®-nominated feature “The Fighter,” starring Mark Wahlberg, Christian Bale, Melissa Leo and Amy Adams and directed by David O. Russell. Produced for $25 million, the film grossed more than $125 million worldwide and earned a host of awards, including an Academy Award&reg; nomination for Best Picture, and Best Supporting Actor and Actress Awards for Bale and Leo.</p>
							<p>Upcoming movies include: “Stronger,” the inspirational story of  Boston marathon survivor Jeff Bauman   directed by David Gordon Green, starring Jake Gyllenhaal, Tatiana Maslany, Miranda Richardson and Clancy Brown; “Wonder,” based on R.J. Palacio’s best-selling young adult novel, to be directed by Stephen Chbosky and starring Julia Roberts, Owen Wilson, Jacob Tremblay and Mandy Patinkin, to be released by Lionsgate.</p>
							<p>Under the Mandeville banner, Hoberman also produced “The Muppets,” starring Jason Segel, Amy Adams and Walter, the newest Muppet. Directed by James Bobin and written by Segel and Nick Stoller, “The Muppets” was one of the best-reviewed films of 2011 and earned the Academy Award® for Best Song. Mandeville also produced the next Muppet installment, “Muppets Most Wanted.” Mandeville also produced “Warm Bodies,” the genre-bending “zombie romance” based on Isaac Marion’s novel, starring Nicholas Hoult, John Malkovich and Teresa Palmer, written and directed by Jonathan Levine, for Summit/Lionsgate. Mandeville also executive produced “Insurgent” and “Allegiant,” parts of the “Divergent” movie series.</p>
							<p>Disney has been Mandeville’s home for over 20 years, and Hoberman has worked at Disney in some capacity since 1985.</p>
							<p>Under the Disney banner, Mandeville produced a string of hits, including the romantic comedy “The Proposal,” starring Sandra Bullock and Ryan Reynolds. “The Proposal” became the highest-grossing romantic comedy of 2009, earning over $317 million worldwide. It was the People’s Choice Award winner for Favorite Comedy Movie. </p>
							<p>Hoberman founded Mandeville Films in 1995 and signed a five-year first-look pact with the Walt Disney Studios. In 1999, Hoberman signed a first-look deal for Mandeville at the Walt Disney Studios with Lieberman. Two years later, Lieberman became a co-partner in the company. 
							The company created the award-winning “Monk,” a one-hour series for USA Network. Executive-produced by Mandeville, “Monk” aired for eight seasons.</p>
							<p>Prior to forming Mandeville Films, Hoberman served as president of the Motion Picture Group of the Walt Disney Studios, where he was responsible for overseeing development and production for all feature films for Walt Disney Pictures, Touchstone and Hollywood Pictures. During Hoberman’s tenure, Disney was often the top studio in domestic box office grosses. In 1990, “Pretty Woman,” supervised by Hoberman, was the top-grossing film of the year and its soundtrack was the top-selling soundtrack of the year. Hoberman was also behind major blockbusters at the studio, including “Father of the Bride,” “What About Bob,” “Dead Poets Society,” “Ed Wood,” “Dangerous Minds,” “Ruthless People,” “Beaches,” “Three Men and a Baby” and “Honey, I Shrunk the Kids.” While an executive at Disney, Hoberman championed the first-ever full-length stop-motion animation feature, Tim Burton’s “The Nightmare Before Christmas.”</p>
							<p>Hoberman started his career working in the mailroom at ABC and quickly ascended in the entertainment business, working for Norman Lear’s Tandem/T.A.T. in television and film. He worked as a talent agent at ICM before joining Disney in 1985.</p>
							<p>
							Hoberman is also a member of the Academy of Motion Picture Arts and Sciences and the Academy of Television Arts and Sciences. He is a member of the Board of Suffolk University in Boston and on the Board of Overseers at the Hammer Museum, Los Angeles.</p>
						</div>
					</div>
				</div>
				<div class="bio" id="lieberman-bio">
					<div class="name">TODD LIEBERMAN, <span class="guild">p.g.a.</span> (PRODUCER)</div>
					<div class="text">
						<div class="scrollable">
							<p>TODD LIEBERMAN, p.g.a. (Producer) is a co-owner of Mandeville Films and Television, where he is one of the leading producers in the entertainment industry today. Since its founding in 1995, Mandeville Films has produced feature films that have grossed more than <span class="nowrap">$1 billion</span> in domestic box office receipts. Mandeville partners David Hoberman and Todd Lieberman produced the critically-hailed Academy Award®-nominated feature “The Fighter,” starring Mark Wahlberg, Christian Bale, Melissa Leo and Amy Adams and directed by David O. Russell. Produced for $25 million, the film grossed more than $125 million worldwide and earned a host of awards, including an Academy Award&reg; nomination for Best Picture, and Best Supporting Actor and Actress awards for Bale and Leo.</p>
							<p>Upcoming movies include: “Stronger,” the inspirational story of Boston marathon survivor Jeff Bauman, directed by David Gordon Green and starring Jake Gyllenhaal, Tatiana Maslany, Miranda Richardson and Clancy Brown; “Wonder,” based on R.J. Palacio’s best-selling young adult novel, to be directed by Stephen Chbosky, starring Julia Roberts and Jason Tremblay, to be released by Lionsgate and the indie thriller “The Duel,” starring Liam Hemsworth and Woody Harrelson, directed by Kieran Darcy-Smith, also from Lionsgate. Under the Mandeville banner, Lieberman also produced “The Muppets,” starring Jason Segel, Amy Adams and Walter, the newest Muppet. Directed by James Bobin and written by Segel and Nick Stoller, “The Muppets” was one of the best-reviewed films of 2011 and earned the Academy Award® for Best Song. </p>
							<p>Mandeville also produced the next Muppet installment, “Muppets Most Wanted.” Lieberman also produced “Warm Bodies,” the genre-bending “zombie romance” based on Isaac Marion’s novel, starring Nicholas Hoult, John Malkovich and Teresa Palmer written and directed by Jonathan Levine, for Summit/Lionsgate. Mandeville also executive produced “Insurgent” and “Allegiant,” parts of the “Divergent” movie series.</p>
							<p>Mandeville has the scripted comedy series “Sing It!” for YouTube with the Fine Brothers. Mandeville recently re-upped its first look deal with Disney, which has been Mandeville’s home for over 20 years. Under the Disney banner, Mandeville produced a string of hits, including the romantic comedy “The Proposal,” starring Sandra Bullock and Ryan Reynolds. “The Proposal” became the highest-grossing romantic comedy of 2009, earning over $317 million worldwide. It was the People’s Choice Award winner for Best Comedy of the Year.</p>
							<p>Prior to joining Mandeville, Lieberman served as senior vice president for international finance and production company Hyde Park Entertainment, which produced and co-financed such films as “Anti-Trust,” “Bandits” and “Moonlight Mile.” Lieberman established himself at international sales and distribution giant Summit Entertainment, where he quickly moved up the ranks after pushing indie sensation “Memento” into production and acquiring the Universal box office smash “American Pie.”</p>
							<p>In 2001, Lieberman was named one of the “35 under 35” people to watch in the business by The Hollywood Reporter. He holds a B.A. from the University of Pennsylvania. Lieberman is a member of the Motion Picture Academy of Arts and Sciences and a judge for the Academy’s Nicholl Fellowship in Screenwriting. He is also a member of the Television Academy and a Producer’s Guild mentor, as well as an active member of the Los Angeles chapter of the Young Presidents’ Organization.</p>
						</div>
					</div>
				</div>
				<div class="bio" id="tobias-bio">
					<div class="name">TOBIAS SCHLIESSLER, <span class="guild">ASC</span> <br/>(DIRECTOR OF PHOTOGRAPHY)</div>
					<div class="text">
						<div class="scrollable">
							<p>TOBIAS SCHLIESSLER, <span class="guild">ASC</span> (Director of Photography) is currently lensing Ava DuVernay’s adaptation of “A Wrinkle in Time” for Disney, starring Chris Pine, Gugu Mbatha-Raw and Reese Witherspoon. He has enjoyed a successful partnership with director Bill Condon, as the two first teamed up on Condon’s Academy Award®-winning film “Dreamgirls,” followed by “The Fifth Estate,” starring Benedict Cumberbatch and more recently, the beautifully-shot “Mr. Holmes,” starring Ian McKellen.</p>
							<p>Schliessler most recently shot Peter Berg’s Boston Marathon drama “Patriots Day,” starring Mark Wahlberg. Berg and Schliessler previously collaborated on a number of films including the action/drama “Lone Survivor,” also starring Mark Wahlberg; the sci-fi thriller “Battleship”; “Hancock, starring Will Smith; the high school football drama “Friday Night Lights”; and “The Rundown,” starring Dwayne “The Rock” Johnson.</p>
							<p>Other notable credits include Tony Scott’s crime thriller “The Taking of Pelham 123,” as well as Antoine Fuqua’s “Bait,” starring Jamie Foxx.</p>
							<p>A native of Germany, Schliessler studied cinematography at Simon Fraser University in British Columbia, Canada. He began his career shooting documentaries, and then segued into independent features, television movies, music videos and commercials. Schliessler was honored by the Association of Independent Commercial Producers (AICP) for his cinematography on Audi’s commercial “Wake Up,” in 2000, and Lincoln’s Financial spot “Doctor” in 2001. Both are now part of the permanent archives of The Museum of Modern Art’s Department of Film and Video in New York City. His commercial work also includes ads for such high-end brands as Mercedes-Benz, BMW, Bank of America, Citibank, AT&T and Verizon.</p>
						</div>
					</div>
				</div>
				<div class="bio" id="katz-bio">
					<div class="name">VIRGINIA KATZ, <span class="guild">ACE</span></div>
					<div class="text">
						<div class="scrollable">
							<p>VIRGINIA KATZ, ACE (Edited by) has worked with Bill Condon for close to 25 years, a feat which is quite rare in the motion picture industry. During that time, Katz edited “Gods and Monsters”; “Kinsey,” for which she was nominated for an America Cinema Editors Eddie Award; “Dreamgirls,” for which she won an Eddie Award; “The Twilight Saga: Breaking Dawn – Part 1” and “The Twilight Saga: Breaking Dawn – Part 2”; “The Fifth Estate”; “Mr. Holmes”; and “Beauty and The Beast.”</p>
							<p>She honed her craft working with her father, veteran film editor Sidney Katz, for whom she served as assistant editor and co-editor.</p>
						</div>
					</div>
				</div>
				<div class="featurette openOverlay" id="back-to-life">
					<img src="/img/batb/filmmakers/featurette-img.png"/>
				</div>
			</div>
		</div>
		<div class="music">
			<div class="quote">
				<p>“‘BEAUTY AND THE BEAST’ MARRIES VISUAL SPECTACLE<br/> AND SUMPTUOUS DESIGN WORK WITH A BETTER STORY<br/> THAN ITS ORIGINAL, CASTING A SPELL ON OLD FANS<br/> AND NEWCOMERS ALIKE.<br/><br/>LYRICS BY THE LATE HOWARD ASHMAN THAT WERE CUT<br/> FROM THE ANIMATED FILM ARE ADDED BACK HERE,<br/> AND ORIGINAL COMPOSER ALAN MENKEN AND LYRICIST<br/> TIM RICE ARE OBVIOUSLY GOING FOR NEXT YEAR'S<br/>ORIGINAL SONG OSCAR WITH NEW NUMBERS.”
				</p>
				<div class="author">- BRIAN TRUITT, <img class="auth-logo" src="/img/press/quote-usa-today.png"></div>
			</div>
			<div class="content">
				<div class="song">
					<audio><source src="/media/audio/batb/HowDoesAMomentLastForever-CelineDion.mp3" type="audio/mp3"></audio>
					<div data-song="How Does A Moment Last Forever" class="play-btn"></div>
					<div class="song-title"><span>“HOW DOES A MOMENT LAST FOREVER”</span></div>
					<div class="credits">
						<div>MUSIC BY</div>
						ALAN MENKEN
						<div>LYRICS BY</div>
						TIM RICE
					</div>
					<div class="artist">
						<img src="/img/batb/music/dion.jpg">
					</div>
				</div>
				<div class="song">
					<audio><source src="/media/audio/batb/Evermore-JoshGroban.mp3" type="audio/mp3"></audio>
					<div data-song="Evermore" class="play-btn"></div>
					<div class="song-title"><span>“EVERMORE”</span></div>
					<div class="credits">
						<div>MUSIC BY</div>
						ALAN MENKEN
						<div>LYRICS BY</div>
						TIM RICE
					</div>
					<div class="artist">
						<img src="/img/batb/music/groban.jpg">
					</div>
				</div>
			</div>
		</div>
		<div class="press">
			<div class="quote">
				<p>
					“THIS LIVE-ACTION/DIGITAL HYBRID, DIRECTED BY BILL CONDON AND STARRING<br/>EMMA WATSON AND DAN STEVENS IN THE TITLE ROLES, IS MORE THAN A FLESH-<br/>AND-BLOOD (AND PROSTHETIC FUR-AND-HORNS) REVIVAL OF THE 26-YEAR-OLD<br/>CARTOON, AND MORE THAN A DUTIFUL TRIP BACK TO THE POP-CULTURE FAIRY-TALE<br/>WELL. ITS CLASSICISM FEELS UNFORCED AND FRESH. ITS ROMANCE NEITHER WINKS<br/>NOR PANDERS. IT LOOKS GOOD, MOVES GRACEFULLY AND LEAVES A CLEAN AND<br/>INVIGORATING AFTERTASTE. I ALMOST DIDN'T RECOGNIZE THE FLAVOR:<br/>I THINK THE NAME FOR IT IS JOY.”
				</p>
				<div class="author">
					- A. O. Scott, <img class="auth-logo" src="/img/press/quote-nyt.png">
				</div>
			</div>
			<div class="content">
				<div class="article" id="ew"><a href="/media/pdf/EW_DAILY_BUZZ_2.pdf" target="_blank"><img src="/img/batb/press/EW.png"></a></div>
				<div class="article" id="ew-2"><a target="_blank" href="/media/pdf/inside_the_magic.pdf"><img src="/img/batb/press/EW-2.png"></a></div>
				<div class="article" id="ny-times"><a target="_blank" href="/media/pdf/BATB_NYT_ARTS_AND_LEISURE.pdf"><img src="/img/batb/press/NY-TIMES.png"></a></div>
				<!-- <div class="article" id="daily-buzz"><a href="#"><img src="/img/batb/press/DAILY-BUZZ.png"></a></div> -->
				<div class="article" id="la-times"><a href="/media/pdf/BATB_LA_TIMES.pdf" target="_blank"> <img src="/img/batb/press/LA-TIMES.png"></a></div>
				<div class="article" id="corden"><a target="_blank" href="http://www.cbs.com/shows/late-late-show/news/1006808/james-corden-produces-beauty-and-the-beast-in-a-crosswalk/"><img src="/img/batb/press/CORDEN.png"></a></div>
			</div>
		</div>
		<div class="making-of">		
			<div class="content">
				<div class="quote">
					<div>
						<p>“Visually, ‘Beauty and the Beast’ is spectacular.<br/> Costumes, hair and makeup, visual effects, art direction,<br/> and set designs are top notch and likely top contenders<br/> for some award season love at year’s end. The film<br/> blends real life actors and sets with the CGI and other<br/> effects work to excellent effect. It’s a gorgeous,<br/> entertaining, amusing fairy tale come to vivid life.”</p>
						<div class="author">- MARK HUGHES, <img class="auth-logo forbes" src="/img/press/quote-forbes.png"/></div>
					</div>
					<div>
						<p>“A Rococo confection featuring fiendishly intricate<br/> production values, a bravura, coloratura-rich<br/> musical score and whizz-pop state-of-the-art effects…<br/> ‘Beauty and the Beast’ is more than just eye candy.”</p>
						<div class="author">- LESLIE FELPERIN, <img src="/img/press/quote-thr.png" class="auth-logo thr"></div>
					</div>
					<div>
						<p>“Most strikingly, the film has taken Belle, one of Disney’s<br/> stalwart princesses for nearly three decades,<br/> and upgraded her for the 21st century.”</p>
						<div class="author">- TUFAYEL AHMED, <img class="auth-logo newsweek" src="/img/press/quote-newsweek.png"></div>
					</div>
					<div><p>
						“Beauty and the Beast’s feminist reimagining is empowering<br/> for the millions of young girls who will fill theaters<br/> around the world to see it.”</p>
						<div class="author">- TUFAYEL AHMED, <img class="auth-logo newsweek" src="/img/press/quote-newsweek.png"></div>
					</div>
					<div>
						<p>
							“‘A tale as old as time’, yes. But Disney and Condon have found<br/> a way to breathe some new life into it.”
						</p>
						<div class="author">- TUFAYEL AHMED, <img class="auth-logo newsweek" src="/img/press/quote-newsweek.png"></div>
					</div>
				</div>
				<div class="articles">
					<div class="article openOverlay" id="production-design"><img src="/img/batb/makingof/production-design.png"></div>
					<div class="article openOverlay" id="costume-design"><img src="/img/batb/makingof/costume-design.png"></div>
				</div>
				<div class="articles2">
					<div class="article openOverlay" id="makeup"><img src="/img/batb/makingof/makeup.png"></div>
					<div class="article openOverlay" id="women-of"><img src="/img/batb/makingof/women-of.png"></div>
					<div class="article openOverlay" id="dressing-the-part"><img src="/img/batb/makingof/dressing-the-part.png"></div>
				</div>
			</div>
		</div>
		<div class="screenplay">
			<div class="quote">
					<p>“Most strikingly, the film has taken Belle, one of Disney’s stalwart princesses for nearly three decades, and upgraded her for the 21st century. Beauty and the Beast’s feminist reimagining is empowering for the millions of young girls who will fill theaters around the world to see it.  
					'A tale as old as time', yes. But Disney and Condon have found a way to breathe some new life into it.”</p>
					<div class="author">- TUFAYEL AHMED, <img class="auth-logo newsweek" src="/img/press/quote-newsweek.png"></div>
				</div>
			<div class='content'>

				<div class="download-button"><a href="/media/scripts/beauty_and_the_beast.pdf" download="Beauty_and_the_Beast-FYC.pdf"><img src="/img/batb/download-btn.png"></a></div>
				<div class="text">
					<h2>BEST ADAPTED SCREENPLAY</h2>
					<div class="written">WRITTEN BY</div>
					<div class="names">
						<div>STEPHEN CHBOSKY</div><div class="and">AND</div><div>EVAN SPILIOTOPOULOS</div>
					</div>
				</div>
			</div>
		</div>
		<div class="accolades">
			<div class="banner"><img src="/img/batb/batb-accolades-banner.jpg"></div>
			<div class="content">
				<div class="award large" style="margin-top:2vh;">
					<img src="/img/batb/awards/2ACADEMY.jpg"/>
				</div>
				<br/>
				
				<div class="award large">
					<img src="/img/batb/awards/BAFTA.jpg">
				</div>

				<br/>
				<div class="award large">
					<img src="/img/batb/awards/CRITICS-CHOICE-AWARDS.jpg">
				</div>
				<br/>
				<div class="award">
					<img src="/img/batb/awards/VES.jpg"/>
				</div>
				<div class="award middle">
					<img src="/img/batb/awards/ADG.jpg"/>
				</div>
				<div class="award">
					<img src="/img/batb/awards/COSTUME-GUILD.jpg"/>
				</div>
				<br/>

				<div class="award">
					<img src="/img/batb/awards/COSTUME-DESIGN.jpg"/>
				</div>
				<div class="award middle">
					<img src="/img/batb/awards/HFA.jpg"/>
				</div>
				<div class="award">
					<img src="/img/batb/awards/SAN-DIEGO.jpg"/>
				</div>
				<br/>
				
				
				<div class="award">
					<img src="/img/batb/awards/PHOENIX.jpg"/>
				</div>
				<div class="award middle">
					<img src="/img/batb/awards/NAACP.jpg"/>
				</div>
				<div class="award">
					<img src="/img/batb/awards/MAKEUP.jpg"/>
				</div>
				<br/>
				<div class="award pushRight" style="width:30%;">
					<img src="/img/batb/awards/MUAHG.jpg"/>
				</div>
				<div class="award" style="width:30%;">
					<img src="/img/batb/awards/MPSE.jpg"/>
				</div>
			</div>
		</div>
		<?php //if($isMobile) { ?>
		<footer>
			<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
			<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
			<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
			<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
			<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
			<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
			<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
		</footer>
		<?php //} ?>
	</div>