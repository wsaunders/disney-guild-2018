<div class="bg-holder">
	<div class="bg one"></div>
	<div class="bg synopsis"></div>
	<div class="bg filmmakers">
		<div class="bg dave"></div>
		<div class="bg dana"></div>
	</div>
	<div class="bg press"></div>
	<div class="bg photos"></div>
</div> 
<div class="limiter">
	<div class="consider">

		<div class="content">
			<div class="nom">
				<img src="/img/lou/ACADEMY.png"/>
			</div>
			
			<div class="logo">
				<img src="/img/lou/lou-tt.png" alt="Lou Logo">
			</div>
			<div class="first">
				<p>FOR YOUR CONSIDERATION</p>
				<h3 class="single">BEST ANIMATED SHORT</h3>
			</div>
			<div>
				<h3>DIRECTED BY</h3>
				<div class="name">DAVE MULLINS</div>

				<h3>PRODUCED BY</h3>
				<div class="name">DANA MURRAY</div>
			</div>
		</div>
	</div>
	<div class="synopsis">
		<div class="content">
			When a toy-stealing bully ruins recess for a playground full of kids, only one thing stands in his way: the “Lost and Found” box.
		</div>
	</div>
	<div class="filmmakers">
		<div class="content">
			<div class="name-list">
				<span class="name current">DAVE MULLINS</span> | 
				<span class="name">DANA MURRAY</span>
			</div>
			<div class="bio" id="dave-bio">
				<div class="name">DAVE MULLINS (DIRECTOR)</div>
				<div class="studio">PIXAR ANIMATION STUDIOS</div>
				<div class="text">
					<div class="scrollable">
					<p>Dave Mullins joined Pixar Animation Studios in September 2000.  His first project was working as a pre-production animator on the Academy Award®-winning film, “Finding Nemo.”  From there he went on to animate on a number of Pixar feature films including “Monsters Inc.,” “The Incredibles” and “Ratatouille.”  Mullins was an animator on the Golden Globe-winner, “Cars” and the short film “One Man Band” and worked as directing animator on another Academy Award®-winner, “Up.”  Mullins then was tasked as a supervising animator on, “Cars 2,” and additionally contributed his animation skills to “Brave,” “Inside Out” and “The Good Dinosaur.” Most recently, Mullins made his directorial debut on Pixar’s new short, “Lou,” set to open in front of “Cars 3” in Summer 2017. 
					</p>
					<p>
					A lifelong lover of movies, cartoons and drawing, Mullins was determined to land a job as an artist. While studying painting in art school, he found a RISC 6000 computer in the computer lab storage closet and taught himself how to use it. After creating his first animation of a walking coat hanger, he was hooked. He graduated Rhode Island School of Design with a Bachelor’s of Fine Art in Illustration and was able to secure his first job in the movies creating animated concession stand ads 
					for theatres.
					</p>
					<p>
					Prior to coming to Pixar, Mullins' early résumé includes such production companies as Pike Productions, The Post Group, Acclaim Entertainment, Walt Disney Animation Studios, Dream Quest Images, Digital Domain and Sony Pictures Imageworks.
					</p>
					<p>
					Mullins currently lives in Moraga, California with his wife, their two children and 
					dog, Pickles.
					</p>
				</div>
				</div>
			</div>
			<div class="bio" id="dana-bio">
				<div class="name">DANA MURRAY (PRODUCER)</div>
				<div class="studio">PIXAR ANIMATION STUDIOS</div>
				<div class="text">
					<div class="scrollable">
					<p>
					Dana Murray joined Pixar Animation Studios in June 2001 as a desk production assistant on “Finding Nemo.” She soon served as the art and technology coordinator on several short films including “Boundin’,” “One Man Band” and “Lifted.” Murray continued on as a lighting coordinator for “Cars,” and held a variety of department manager positions on the Academy Award©-winning feature films “Ratatouille,” “Up,” and “Brave.” Murray moved on to be the production manager for the Academy Award®-winning film, “Inside Out,” and most recently produced her first short film, “Lou,” set to release on Summer 2017 in front of “Cars 3.” </p>
					<p>
					Raised in Placerville, CA, Murray attended Sonoma State University. She currently resides in Oakland, CA with her husband, their two girls and 
					dog, Gracie. </p>
				</div>
				</div>
			</div>
		</div>
	</div>
	<div class="press">
		<div class="scroll-down">SCROLL DOWN FOR MORE</div>
		<div class="content">
			<div class="scrollable">
				<div class="snippet">
					<div class="press-logo">
						<img style="max-height:32px;" src="/img/press/cinema-blend.png"/>
					</div>
					<div class="title">
						“It's got all of Pixar's heart but it's also got some of the most amazing animation we've ever seen from Pixar.”
					</div>
					<a target="_blank" href="http://www.cinemablend.com/news/1665580/5-fascinating-facts-about-pixars-newest-short-film-lou">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="max-height:32px;" src="/img/press/vanity-fair.svg"/>
					</div>
					<div class="title">
						 “It’s a dazzling display of an animation team playing both within and outside the laws of physics and gravity.”
					</div>
					
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="max-height:32px;" src="/img/press/the-wrap.svg"/>
					</div>
					<div class="title">
					“A sweetly poignant tale of a sentient lost-and-found box that teaches a subtle lesson about schoolyard bullying. “Lou” (written and directed by Dave Mullins) shows once again that the short subject is alive and well in the halls of this animated studio.”
					</div>
					
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="max-height:27px;" src="/img/press/huffpost.svg"/>
					</div>
					<div class="title">
						“It’s one of my favorite Pixar shorts to date”
					</div>
					
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="max-height:46px;" src="/img/press/telegraph.svg"/>
					</div>
					<div class="title">
						“Written and directed by Dave Mullins, it’s ingeniously animated, almost instantly moving, and springs from an astute and precise observation of human nature: the only way to defeat our demons is to dismantle them in good faith, piece by piece.”
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="photos">
		<div class="slider">
			
		</div>
		<div class="left-arrow arrow"></div>
		<div class="right-arrow arrow"></div>
		<div class="dots">
		</div>
	</div>
	<?php //if($isMobile) { ?>
		<footer>
			<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
			<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
			<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
			<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
			<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
			<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
			<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
		</footer>
		<?php //} ?>
</div>