<div class="bg-holder">
	<div class="bg fyc"></div>
	<div class="bg screenings"></div>
	<div class="bg synopsis"></div>
	<div class="bg press"></div>
	<div class="bg photos"></div>
	<div class="bg videos"></div>
	<div class="bg screenplay"></div>
	<div class="bg song"></div>
</div> 
<div class="limiter">
	<div class="consider">
		<div class="content">
			<div class="logo">
				<img src="/img/gotg/gotg-tt.png" alt="Coco"/>
			</div>
			<div class="first">
				<p>FOR YOUR CONSIDERATION IN ALL CATEGORIES</p>
				<h3>BEST PICTURE</h3>
				<div class="subline">PRODUCED BY</div>
				<div class="name">KEVIN FEIGE, <span class="guild">p.g.a.</span></div>
			</div>
			<div class="left">
				<h3>BEST DIRECTOR</h3>
				<div class="name">JAMES GUNN</div>
				<h3>BEST ADAPTED SCREENPLAY</h3>
				<div class="name">JAMES GUNN</div>
				<h3>BEST ACTOR</h3>
				<div class="name">CHRIS PRATT</div>
				<h3>BEST SUPPORTING ACTOR</h3>
				<div class="name">
					DAVE BAUTISTA<br/>
					VIN DIESEL<br/>
					BRADLEY COOPER<br/>
					MICHAEL ROOKER<br/>
					SYLVESTER STALLONE<br/>
					KURT RUSSELL<br/>
				</div>
				<h3>BEST SUPPORTING ACTRESS</h3>
				<div class="name">
					ZOE SALDANA<br/>
					KAREN GILLAN<br/>
					POM KLEMENTIEFF
				</div>
				<h3>BEST CINEMATOGRAPHY</h3>
				<div class="name">HENRY BRAHAM, <span class="guild">BSC</span></div>
				<h3>BEST FILM EDITING</h3>
				<div class="name">FRED RASKIN, <span class="guild">ACE</span></div>
				<div class="name">CRAIG WOOD, <span class="guild">ACE</span></div>
				<h3>BEST PRODUCTION DESIGN</h3>
				<div class="subline">PRODUCTION DESIGNER</div>
				<div class="name">SCOTT CHAMBLISS</div>
				<div class="subline">SET DECORATOR</div>
				<div class="name">JAY HART</div>
			
			</div>
			<div class="right">
				<h3>BEST COSTUME DESIGN</h3>
				<div class="name">JUDIANNA MAKOVSKY</div>
				<h3>BEST MAKEUP & HAIRSTYLING</h3>
				<div class="name">JOHN BLAKE<br/>
					CAMILLE FRIEND<br/>BRIAN SIPE</div>
				<h3>BEST SOUND MIXING</h3>
				<div class="subline">RE-RECORDING MIXERS</div>
				<div class="name">CHRISTOPHER BOYES<br/>LORA HIRSCHBERG</div>
				<div class="subline">SOUND MIXER</div>
				<div class="name">LEE ORLOFF</div>
				<h3>BEST SOUND EDITING</h3>
				<div class="subline">SUPERVISING SOUND EDITORS</div>
				<div class="name">ADDISON TEAGUE<br/>DAVID ACORD</div>
				<h3>BEST VISUAL EFFECTS</h3>
				<div class="name">
					CHRISTOPHER TOWNSEND<br/>
					GUY WILLIAMS<br/>
					JONATHAN FAWKNER<br/>
					DAN SUDICK
				</div>
				<h3>BEST ORIGINAL SCORE</h3>
				<div class="name">TYLER BATES</div>
				<h3>BEST ORIGINAL SONG</h3>
				<div class="song">"GUARDIANS INFERNO"</div>
				<div class="subline">WRITTEN BY</div>
				<div class='name'>JAMES GUNN AND TYLER BATES</div>
				<div class="subline">PERFORMED BY</div>
				<div class="name">THE SNEEPERS<br/>FT. DAVID HASSELHOFF</div>
			</div>
		</div>
	</div>

	<div class="screenings">
		<div class="content">
			<div class="cities-list">
				<div class="city selected">Los Angeles</div>
				<div class="city">New York</div>
				<div class="city">San Francisco</div>
				<div class="city">London</div>
				
			</div>
			<div class="city-holder London-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder New_York-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder Los_Angeles-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder San_Francisco-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
		<div class="disclaimer">You must be an invited member of a voting organization to attend <span class="nowrap">For Your Consideration screenings</span>. Your membership card is required for entry.</div>
		</div>
	</div>
	<div class="synopsis">
		<div class="content">
			Set to the all-new sonic backdrop of Awesome Mixtape #2, Marvel Studios’ “Guardians of the Galaxy Vol. 2” continues the team’s adventures as they traverse the outer reaches of the cosmos. The Guardians must fight to keep their newfound family together as they unravel the mystery of Peter Quill’s true parentage. Old foes become new allies and fan-favorite characters from the classic comics will come to our heroes’ aid as the Marvel Cinematic Universe continues to expand.
		</div>
	</div>
	<div class="press">
		<div class="scroll-down">SCROLL DOWN FOR MORE</div>
		<div class="content">
			<div class="scrollable">
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/HOLLYWOOD_REPORTER.png"/>
					</div>
					<div class="title">
						'GUARDIANS OF THE GALAXY VOL.2': A DIGITAL KURT RUSSELL AND OTHER VFX TRICKS REVEALED
					</div>
					<a target="_blank" href="http://www.hollywoodreporter.com/behind-screen/guardians-galaxy-vol-2-vfx-tricks-revealed-1001266">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/yahoo-logo.png"/>
					</div>
					<div class="title">
						'Guardians of the Galaxy Vol. 2' VFX spotlight: De-aging Kurt Russell gracefully
					</div>
					<a target="_blank" href="https://www.yahoo.com/movies/guardians-galaxy-vol-2-vfx-spotlight-de-aging-kurt-russell-gracefully-221943027.html">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/wash-post-logo.png"/>
					</div>
					<div class="title">
						Is young Kurt Russell in ‘Guardians of the Galaxy Vol. 2’ the best de-aging of an actor ever?
					</div>
					<a target="_blank" href="https://www.washingtonpost.com/news/comic-riffs/wp/2017/05/05/is-young-kurt-russell-in-guardians-of-the-galaxy-vol-2-the-best-de-aging-of-an-actor-ever/?utm_term=.2c423a36ebbe">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/FORBES-logo.png"/>
					</div>
					<div class="title">
						Guardians Of The Galaxy Vol. 2: Not Just An Awesome Movie, A Technological Masterpiece Too
					</div>
					<a target="_blank" href="https://www.forbes.com/sites/ianmorris/2017/05/04/guardians-of-the-galaxy-vol-2-not-just-an-awesome-movie-a-technological-masterpiece-too/#24a2c2b06a0c">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/BTL_front_logo.png"/>
					</div>
					<div class="title">
						Chris Townsend, Visual Effects Supervisor Guardians of the Galaxy Vol. 2
					</div>
					<a target="_blank" href="http://www.btlnews.com/crafts/chris-townsend-visual-effects-supervisor-guardians-galaxy/">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/fxguide-logo.png"/>
					</div>
					<div class="title">
						Guardians of Kurt: Young Kurt Russell & Ego Suppression
					</div>
					<a target="_blank" href="https://www.fxguide.com/featured/guardians-of-kurt/">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/awn_logo.png"/>
					</div>
					<div class="title">
						Weta Digital Confronts the Monstrous Ego of ‘Guardians of the Galaxy Vol. 2’
					</div>
					<a target="_blank" href="https://www.awn.com/vfxworld/weta-digital-confronts-monstrous-ego-guardians-galaxy-vol-2">READ MORE</a>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/UPROXX_LOGO.png"/>
					</div>
					<div class="title">
						‘Guardians Of The Galaxy Vol. 2’ Is The Redemption Of CGI
					</div>
					<a target="_blank" href="http://uproxx.com/filmdrunk/guardians-galaxy-2-visual-brilliance/">READ MORE</a>
				</div>
			</div>
		</div>
	</div>
	<div class="photos">
		<div class="slider">
			
		</div>
		<div class="left-arrow arrow"></div>
		<div class="right-arrow arrow"></div>
		<div class="dots">
			
		</div>
	</div>
	<div class="videos">
		<div class="content">
			<div class="video-list">
				<!-- <div class="video-title" data-src="media/video/gotg-trailer-1.mp4"><span>WATCH</span><br/>TRAILER #1</div> -->
				<div class="video-title" data-src="/media/video/reunited.mp4">“REUNITED”<br/><span>WATCH THE FEATURETTE</span></div>
				<div class="video-title" data-src="/media/video/level-up.mp4">“IN THE DIRECTOR'S CHAIR WITH JAMES GUNN”<br/><span>WATCH THE FEATURETTE</span></div>
			</div>
			<div class="video-container" id="gotg-video">
				<video controls>
					<source src="/media/video/gotg-trailer-1.mp4" type="video/mp4">
				</video>
				<div class="video-play-btn"></div>
				<div class="close-btn">+</div>
			</div>
		</div>
	</div>
	<div class="screenplay">
		
		<div class='content'>
			<div class="quote">
				<p>“Just like the first one,<br/> 'Guardians of the Galaxy Vol. 2'<br/> is a winning and wonderfully<br/> relatable gem of crazy. The<br/> creative mind of<br/> writer/director James Gunn<br/> comes alive again in this<br/> hysterical and surprisingly<br/> touching sequel filled with<br/> misfits saving the universe once<br/> more, a guy who’s a living<br/> planet, a cosmic biker gang with<br/> loyalty issues and people<br/> pinballing through space like<br/> Bugs Bunny cartoons<br/> on fast-forward.”</p>
				<div class="author">- BRIAN TRUITT, <img class="auth-logo" src="/img/press/quote-usa-today.png"></div>
			</div>
			<div class="download-button"><a href="/media/scripts/GotG2.pdf" download="Guardians_of_the_GalaxyVol2-FYC.pdf"><img src="/img/batb/download-btn.png"></a></div>
			<div class="text">
				<h2>BEST ADAPTED <br/>SCREENPLAY</h2>
				<div class="written">WRITTEN BY</div>
				<div class="names">
					<div>James Gunn</div>
				</div>
			</div>
		</div>
	</div>
	<div class="song">
		<div class="content">
			<div class="song">
					<h3>BEST ORIGINAL SONG</h3>
					<div class="song-title"><span>“GUARDIANS INFERNO”</span></div>
					<div class="credits">
						<div>WRITTEN BY</div>
						JAMES GUNN <span class="and">AND</span> TYLER BATES
					</div>
					<div class="artist">
						<img src="/img/gotg/james_gunn.jpg">
					
					</div>
					<div class="artist">
						<img src="/img/gotg/tyler_bates.jpg">
					</div>
					<audio><source src="/media/audio/gotg/Guardians_Inferno.mp3" type="audio/mp3"></audio>
					<div data-song="Guardians_Inferno" class="play-btn"><div>PLAY SONG</div></div>
				</div>
		</div>
	</div>
	<div class="accolades">
		<div class="banner"><img src="/img/gotg/gotg-accolades-banner.jpg"></div>
		<div class="content">
			<div class="award academy large pushRight">
				<img src="/img/gotg/awards/ACADEMY-visual.jpg">
			</div>
			
			<div class="award large">
				<img src="/img/gotg/awards/ves-winner.jpg">
			</div>
			<br/>
			<div class="award">
				<img src="/img/gotg/awards/MAKEUP.jpg">
			</div>
			
			<div class="award medium middle">
				<img src="/img/gotg/awards/MUAHG-hair-winner.jpg"/>
				
			</div>
			
			<div class="award">
				<img src="/img/gotg/awards/COSTUME-DESIGN.jpg"/>
			</div>
			<br/>
			<div class="award pushRight">
				<img src="/img/gotg/awards/ANNIE-AWARD.jpg">
			</div>
			
			<div class="award medium">
				<img src="/img/gotg/awards/MUAHG-fx.jpg"/>
			</div>
		</div>
	</div>
	<?php //if($isMobile) { ?>
	<footer>
		<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
		<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
		<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
		<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
		<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
		<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
		<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
	</footer>
	<?php //} ?>
</div>