/* CUSTOM HTML ELEMENT POLYFILL */
if (!Element.prototype.matches) {
    Element.prototype.matches = 
    Element.prototype.matchesSelector || 
    Element.prototype.mozMatchesSelector ||
    Element.prototype.msMatchesSelector || 
    Element.prototype.oMatchesSelector || 
    Element.prototype.webkitMatchesSelector ||
    function(s) {
        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
            i = matches.length;
        while (--i >= 0 && matches.item(i) !== this) {}
        return i > -1;            
    };
}
var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
var headerHeight;
var loaded = null;
//var isMobile = (w<600) ? true:false;
var Orientation = (w >=h) ? 'landscape':'portrait';
var currPageIndex = 0;
var animating = false;
var movieIDIndex = ['batb','cars','coco','lou','gotg','thor','sw'];
var $loaded = null;
if(history.state){
	switch(history.state.film) {
		case 'thor':
			history.replaceState({film:'thor',page:'consider'},'Thor Ragnarok', '');
			break;
		case 'gotg':
			history.replaceState({film:'gotg',page:'consider'},'Guardians of the Galaxy Vol 2', '');
			break;
		case 'coco':
			history.replaceState({film:'coco',page:'consider'},'Coco', '');
			break;
		case 'cars':
			history.replaceState({film:'cars',page:'consider'},'Cars 3', '');
			break;
		case 'lou':
			history.replaceState({film:'lou',page:'consider'},'Lou', '');
			break;
		case 'batb':
			history.replaceState({film:'batb',page:'consider'},'Beauty and the Beast', '');
			break;
		case 'batb':
			history.replaceState({film:'sw',page:'consider'},'Star Wars: The Last Jedi', '');
			break;
		default:
		break;
	}
}
window.onpopstate = function(e){
	console.log(e.state)
	if(e.state !== null){
		if(e.state.film == 'home')
			loadHome();
		else{
			slideFilmPage(e.state.film,e.state.page);
		}
	}else{
		loadHome();
	}
	
}

var initialState = {
	width:null,
	height:null,
	offset:null
}
var filmInfo = {
	"coco":{
		url:'coco',
		title:'Coco',
		page:'coco',
		bgRatios:[0.8,1.2,0.8,1.4,0.8,0.8,0.8],
		bgRatio:[1.6,1.9443535188216,1.61943319838057,1.6,1.6,1.6],
		pageList:['consider','screenings','synopsis','song','score','screenplay','accolades'],
		photos:8,
		pageLoaded:false,
		scoreLoaded:false,
		score:{
			titles:[
				"Will He Shoemaker?",
				"A Run on the Market",
				"Sighs and Shine",
				"Ofrenda Room Vámonos",
				"Shrine and Dash",
				"Día de los Muertos has Begun",
				"Miguel's Got an Axe to Find",
				"The Strum of Destiny",
				"It's All Relative",
				"Crossing the Marigold Bridge",
				"Dept. of Family Reunions",
				"The Skeleton Key to Escape",
				"Dis-Guys Got Tunnel Vision",
				"Backstage Sass",
				"Art for Art's Sake",
				"Monkey Sí",
				"Adiós Chicharrón",
				"Plaza de la Cruz",
				"The Bandas that Tie Us",
				"Family Doubtings",
				"Taking Sides",
				"Fiesta Espectacular",
				"Fiesta con de la Cruz",
				"I Have a Great-Great-Grandson",
				"The Family Showoff",
				"The World is our Family",
				"A Blessing and a Fessing",
				"Somos Familia",
				"Reunión Familiar de Rivera",
				"A Family Dysfunction",
				"Fanfarria de Frida",
				"Be Frida Dance",
				"Grabbing a Photo Opportunity",
				"The Show Must Go On",
				"For Whom the Bell Tolls",
				"A Run for the Ages",
				"One Year Later",
				"Cuckoo For Coco Credits"
			],
			files:[
				"01_Will_He_Shoemaker.mp3",   
				"02_A_Run_on_the_Market.mp3",	    
				"03_Sighs_and_Shine.mp3",  
				"04_Ofrenda_Room_Vamonos.mp3",    
				"05_Shrine_and_Dash.mp3",
				"06_Dia_de_los_Muertos_has_Begun.mp3",	
				"07_Miguels_Got_an_Axe_to_Find.mp3",	
				"08_The_Strum_of_Destiny.mp3",		    
				"09_Its_All_Relative.mp3",			    
				"10_Crossing_the_Marigold_Bridge.mp3",	
				"11_Dept_of_Family_Reunions.mp3",		
				"12_The_Skeleton_Key_to_Escape.mp3",	
				"13_Dis-Guys_Got_Tunnel_Vision.mp3",	
				"14_Backstage_Sass.mp3",				
				"15_Art_for_Arts_Sake.mp3",			
				"16_Monkey_Si.mp3",					
				"17_Adios_Chicharron.mp3",				
				"18_Plaza_de_la_Cruz.mp3",				
				"19_The_Bandas_that_Tie_Us.mp3",
				"20_Family_Doubtings.mp3",
				"21_Taking_Sides.mp3",
				"22_Fiesta_Espectacular.mp3",
				"23_Fiesta_con_de_la_Cruz.mp3",
				"24_I_Have_a_Great-Great-Grandson.mp3",
				"25_The_Family_Showoff.mp3",
				"26_The_World_is_our_Family.mp3",
				"27_A_Blessing_and_a_Fessing.mp3",
				"28_Somos_Familia.mp3",
				"29_Reunion_Familiar_de_Rivera.mp3",
				"30_A_Family_Dysfunction.mp3",
				"31_Fanfarria_de_Frida.mp3",
				"32_Be_Frida_Dance.mp3",
				"33_Grabbing_a_Photo_Opportunity.mp3",
				"34_The_Show_Must_Go_On.mp3",
				"35_For_Whom_the_Bell_Tolls.mp3",
				"36_A_Run_for_the_Ages-.mp3",
				"37_One_Year_Later.mp3",
				"38_Cuckoo_For_Coco_Credits.mp3"	
			]
		}
	},
	"thor":{
		url:'thor-ragnarok',
		title:'Thor Ragnarok',
		page:'thor',
		bgRatios:[0.8,0.8,0.8,1.3,0.8,0.8,0.8],
		bgRatio:[1.6,1.6,1.6,1.641005498821681,1.6,1.51515,1.6],
		pageList:['consider','screenings','synopsis','press','photos','score','screenplay'],
		photos:12,
		pageLoaded:false,
		scoreLoaded:false,
		score:{
			titles:[
				"HOT TIME",
				"RUNNING SHORT ON OPTIONS",
				"THOR:RAGNAROK MAIN TITLE",
				"ONE HELA-VAN AUTOCRAT",
				"WHERE AM I?",
				"VAL’S SHIP SOURCE (VIKING HORNS)",
				"GRAND MASTER’S CHAMBERS",
				"HOT DIDDLE STUFF",
				"THE VAULT",
				"ON ONE ESCAPES",
				"ARENA FIGHT",
				"MORE ARENA FIGHT",
				"GO",
				"WHAT HEROES DO",
				"COUNTERPRODUCTIVE ANGER RELEASE",
				"DREAMING OF AN EXECUTION",
				"TRONICA KALIMBA – THOR AND BANNER IN THE ALLEY",
				"WEAPONS ROOM SOURCE",
				"THE REVOLUTION HAS BEGUN",
				"SAKAAR CHASE",
				"GRAND MASTER CRASH",
				"THE DEVIL’S ANUS",
				"EMPTY HAVEN / SUITING UP",
				"DOES YOUR DOG BITE?",
				"WHERE TO?",
				"MAIN ON ENDS"
			],
			files:[
				"01_Hot_Time.mp3",
				"02_Running_Short_on_Options.mp3",
				"03_Thor_Ragnarok_Main_Title.mp3",
				"04_One_Hela-Van_Autocrat.mp3",
				"05_Where_Am_I.mp3",
				"06_Vals_Ship_Source.mp3",
				"07_Grand_Masters_Chambers.mp3",
				"08_Hot_Diddle_Stuff.mp3",
				"09_The_Vault.mp3",
				"10_On_One_Escapes.mp3",
				"11_Arena_Fight.mp3",
				"12_More_Arena_Fight.mp3",
				"13_Go.mp3",
				"14_What_Heroes_Do.mp3",
				"15_Counterproductive_Anger_Release.mp3",
				"16_Dreaming_of_an_Execution.mp3",
				"17_Tronica_Kalimba-Thor_and_Banner_in_the_Alley.mp3",
				"18_Weapons_Room_Source.mp3",
				"19_The_Revolution_Has_Begun.mp3",
				"20_Sakaar_Chase.mp3",
				"21_Grand_Master_Crash.mp3",
				"22_The_Devils_Anus.mp3",
				"23_Empty_Haven_Suiting_Up.mp3",
				"24_Does_Your_Dog_Bite.mp3",
				"25_Where_To.mp3",
				"26_Main_on_Ends.mp3"
			]
		}
	},
	"gotg":{
		url:'guardians-of-the-galaxy-vol-2',
		title:'Guardians of the Galaxy Vol 2',
		page:'gotg',
		pageLoaded:false,
		bgRatios:[1.8,0.8,1.3,0.8,0.8,0,0.8],
		bgRatio:[1.57,1.57,1.6286644,1.57,1.57,1.57,1.6,1.6],
		pageList:['consider','screenings','synopsis','press','photos','videos','screenplay','song','accolades'],
		photos:7
	},
	"sw":{
		url:'star-wars-the-last-jedi',
		title:'Star Wars: The Last Jedi',
		page:'sw',
		pageLoaded:false,
		bgRatios:[1,0.2,0.8,0.8,0.8,0.8,0.8,0.8],
		bgRatio:[1,1.6,1.6,1.6,1.6,1.6,1.6,1.6],
		pageList:['consider','screenings','synopsis','score','photos','production','accolades'],
		score:{
			titles:[
				"ESCAPE",
		        "AHCH-TO ISLAND",
		        "THE SUPREMACY",
		        "ADMIRAL HOLDO",
		        "FUN WITH FINN AND ROSE",
		        "CONNECTION",
		        "LESSON ONE",
		        "CANTO BIGHT",
		        "THE MASTER CODEBREAKER",
		        "THE FATHIERS",
		        "THE CAVE",
		        "REY’S JOURNEY",
		        "A NEW ALLIANCE",
		        "HOLDO’S RESOLVE",
		        "“CHROME DOME”",
		        "THE BATTLE OF CRAIT",
		        "THE SPARK",
		        "THE LAST JEDI",
		        "OLD FRIENDS",
		        "FINALE"
			],
			files:[
				"01_ESCAPE.mp3",
		        "02_AHCH-TO_ISLAND.mp3",
		        "03_THE_SUPREMACY.mp3",
		        "04_ADMIRAL_HOLDO.mp3",
		        "05_FUN_WITH_FINN_AND_ROSE.mp3",
		        "06_CONNECTION.mp3",
		        "07_LESSON_ONE.mp3",
		        "08_CANTO_BIGHT.mp3",
		        "09_THE_MASTER_CODEBREAKER.mp3",
		        "10_THE_FATHIERS.mp3",
		        "11_THE_CAVE.mp3",
		        "12_REYS_JOURNEY.mp3",
		        "13_A_NEW_ALLIANCE.mp3",
		        "14_HOLDOS_RESOLVE.mp3",
		        "15_CHROME_DOME.mp3",
		        "16_THE_BATTLE_OF_CRAIT.mp3",
		        "17_THE_SPARK.mp3",
		        "18_THE_LAST_JEDI.mp3",
		        "19_OLD_FRIENDS.mp3",
		        "20_FINALE.mp3"
			]
		},
		photos:10
	},
	"lou":{
		url:'lou',
		title:'Lou',
		page:'lou',
		pageLoaded:false,
		bgRatios:[1,1.9,0.8,0.8,0.8,0.8,0.8,0.8],
		bgRatio:[1.6,1.6,1.6,1.6,1.6,1.6,1.6,1.6],
		pageList:['consider','synopsis','filmmakers','press','photos'],
		photos:4
	},
	"batb":{
		url:'beauty-and-the-beast',
		title:'Beauty and the Beast',
		page:'batb',
		pageLoaded:false,
		bgRatios:[1.6,1,1.85,1,1,1,1,1,1],
		bgRatio:[1.6,1.47,1.63,1.62866449511401,1.6,1.6,1.6,1.6],
		pageList:['consider','screenings','cast','filmmakers','music','press','making-of','screenplay','accolades']
	},
	"cars":{
		url:'cars-3',
		title:'Cars 3',
		page:'cars',
		pageLoaded:false,
		bgRatios:[1.5,0.8,0.8,1.7,1,1,1],
		bgRatio:[2.407,1.6,1.6,1.51,1.6,1.6,1.6],
		pageList:['consider','screenings','synopsis','score','songs','press','photos','accolades'],
		scoreLoaded: false,
		score:{
			titles:["STORM'S WINNING STREAK","WHEN ALL YOUR FRIENDS ARE GONE / CRASH","DOC'S PAINFUL DEMISE",'MATER ON THE HORN','SISTINE CHAPEL ON WHEELS','TEMPLE OF RUST-EZE','A CAREER ON A WALL / ELECTRONIC SUIT','DRIP PAN',"McQUEEN'S WILD RIDE",'BIGGEST BRAND IN RACING','FIREBALL BEACH',"PULL OVER NOW / CRUZ's RACING DREAMS",'1.2%','IF THIS TRACK COULD TALK','LETTERS ABOUT YOU','SMOKEY STARTS TRAINING / A BLAZE OF GLORY','STARTING DEAD LAST','FLASHBACK & PIT STOP','THROUGH THE PACK','VICTORY LANE','THE FABULOUS LIGHTNING McQUEEN'],
			files:[
				"01_Storms_Winning_Streak.mp3",
				"02_When_All_Your_Friends_Are_Gone_Crash.mp3",
				"03_Docs_Painful_Demise.mp3",
				"04_Mater_On_the_Horn.mp3",
				"05_Sistine_Chapel_On_Wheels.mp3",
				"06_Temple_of_Rust-eze.mp3",
				"07_A_Career_On_A_Wall_Electronic_Suit.mp3",
				"08_Drip_Pan.mp3",
				"09_McQueens_Wild_Ride.mp3",
				"10_Biggest_Brand_in_Racing.mp3",
				"11_Fireball_Beach.mp3",
				"12_Pull_Over_Now_Cruzs_Racing_Dreams.mp3",
				"13_1_2.mp3",
				"14_If_This_Track_Could_Talk.mp3",
				"15_Letters_About_You.mp3",
				"16_Smokey_Starts_Training_A_Blaze_of_Glory.mp3",
				"17_Starting_Dead_Last.mp3",
				"18_Flashback_And_Pit_Stop.mp3",
				"19_Through_the_Pack.mp3",
				"20_Victory_Lane.mp3",
				"21_The_Fabulous_Lightning_McQueen.mp3"
			],
			credits:'By Randy Newman'
		},
		photos:11
	}
}
var ajax = $.Deferred();
var path = location.pathname.split('/');
var currVideo = null;
$(document).ready(function(){
		
		window.onload = function(){
			headerHeight = $('header').height();
			$('#page-holder').css({height:h-headerHeight});
			//DIRECT TO PAGE IF URL IS MODIFIED
			if(loaded != null)
				resize();

			if(!isMobile || isTablet){
				//preload rest of images
				var imgs=[
					//'cars/cars-press-bg.jpg',
					// 'cars/cars-score-bg.jpg',
					// 'cars/cars-screenings-bg.jpg',
					// 'cars/cars-press-bg.jpg',
					// 'cars/cars-synopsis-bg.jpg',
					// 'cars/cars-videos-bg.jpg',
					// 'cars/cars-song-bg.jpg',
					// 'coco/coco-screenings-bg.jpg',
					// 'coco/coco-screenplay-bg2.jpg',
					// 'coco/coco-accolades-banner.jpg',
					// 'coco/coco-song-bg.jpg',
					// //'coco/coco-videos-bg.jpg',
					// 'coco/coco-synopsis-bg.jpg',
					// 'gotg/gotg-press-bg.jpg',
					// 'gotg/gotg-synopsis-bg.jpg',
					// 'gotg/gotg-screenings-bg.jpg',
					// 'gotg/gotg-videos-bg.jpg',
					// 'gotg/gotg-screenplay-bg.jpg',
					// 'thor/thor-screenings-bg.jpg',
					// 'thor/thor-synopsis-bg.jpg',
					// 'thor/thor-press-bg.jpg',
					// 'thor/thor-score-bg.jpg',
					// 'thor/thor-screenplay-bg.jpg',
					// 'thor/thor-score-video-poster.jpg',
					// 'thor/green-play-btn.png',
					// 'thor/thor-screenplay-bg.jpg',
					// 'lou/lou-synopsis-bg.jpg',
					// 'lou/lou-bg-dave.jpg',
					// 'lou/lou-press-bg.jpg',
					// 'batb/batb-cast-bg.jpg',
					// 'batb/batb-synopsis-bg2.jpg',
					// 'batb/music/batb-music-bg.jpg',
					// 'batb/batb-screenplay-bg.jpg',
					// 'batb/makingof/bg.jpg',
					// 'batb/press/bg.jpg',
					// 'batb/filmmakers/condon-bg.jpg',
					// 'batb/batb-synopsis-image-1.jpg',
					// 'batb/batb-synopsis-image-2.jpg',
					// 'batb/batb-synopsis-image-3.jpg',
					// 'sw/sw-consider-page.jpg',
					// 'sw/sw-screenings-bg.jpg',
					// 'sw/sw-synopsis-bg.jpg',
					// 'sw/sw-title_white.png'
				];
				for(var i=0;i<imgs.length;i++){
					var img = new Image();
					img.onerror = function(){
						console.log(imgs[i]+' failed to load.')
					}
					img.src = '/img/'+imgs[i];
				}
			}
			
		}
		
		if(path[1]){
			if(location.pathname != '/' && location.pathname !='/guild/web/'){
				switch(path[1]){
					case 'thor-ragnarok':
						loaded = 'thor';
						break;
					case 'guardians-of-the-galaxy-vol-2':
						loaded = 'gotg';
						break;
					case 'beauty-and-the-beast':
						loaded = 'batb';
						break;
					case 'cars-3':
						loaded = 'cars';
						break;
					case 'coco':
						loaded = 'coco';
						break;
					case 'lou':
						loaded = 'lou';
						break;
					case 'star-wars-the-last-jedi':
						loaded = 'sw';
						break;
					default:
						location.href= '/';
					break;
				}
				loadFilmPage(loaded,(path[2]) ? path[2].toLowerCase().split(' ').join('-'):null,true);
				
			}
		}
	//},100);

	//HOME PAGE BOX CLICK
	$('.box').on('click', function(e){
		var index = $(this).index();
		if(currVideo == $(this).find('video')[0] && animating){
			console.log('check')
			currVideo.removeEventListener('ended',onEnd);
			animating = false;
			currVideo.pause();
			currVideo.currentTime = 0;
			currVideo.load();
			onEnd()
		}	
		if(currVideo != null && currVideo != $(this).find('video')[0]){
			currVideo.removeEventListener('ended',onEnd);
			animating = false;
			currVideo.pause();
			currVideo.currentTime = 0;
			currVideo.load();
		}

		if(!animating){
			animating = true;
			$(this).find('.logo').fadeOut(300);
			currVideo = $(this).find('video')[0];		
			loaded = $(this).attr('id').split('-')[0]
			$(this).off('mouseleave');
			currVideo.addEventListener('ended', onEnd);
			currVideo.play();

			setTimeout(function(){
				if(currVideo.paused && currVideo.readyState != 4){
					animating = false;
					//onEnd();
				}
			},1500);
		}
	});
	/********************* MENU EVENTS ************************/
	//////////// MOBILE MENU BTN ///////////////////
	$('#menu-btn').on('touchstart click', function(e){
		e.preventDefault();e.stopPropagation();
		$('#menu-btn').toggleClass('active');
		if(!$(this).hasClass('active'))
			$('#menu').css({left:'-100%'});
		else{
			$('.page').one('touchstart click', function(){
				$('#menu').css({left:'-100%'});
				$('#menu-btn').removeClass('active');
			})
			$('#menu').css({left:0}).find(' > .selected').addClass('open');
		}
	});
	//////////////////////////////////////////////////
	$('#menu').find('li').on('touchstart click', function(e){
		e.preventDefault();e.stopPropagation();e.stopImmediatePropagation();
		console.log('li click');
		closeRSVP();	
		if(isTablet || isMobile){
			$(this).toggleClass('open').siblings('.open').removeClass('open');
			$(this).siblings('.open').removeClass('open');
			$('body').one('touchstart click',function(){
				$('.open').removeClass('open');
			});
		}else{
			if(!$(this).hasClass('selected')){
				var className = $(this)[0].className.split('-')[0];
				//prevent animation if already on page.
				if(loaded !=null)
					slideFilmPage(className,'consider')
				else
					loadFilmPage(className,'consider')
			}
		}
	}).hover(function(){
		$(this).addClass('open');
	},function(){
		$(this).removeClass('open');
	});
	//MENU ROOT CLICK
	//HEADER SUBMENU CLICK
	$('#menu').find('.submenu > div').on('click touchstart', function(e){
		e.preventDefault();e.stopPropagation();e.stopImmediatePropagation();
		console.log('subclick')
		if(isMobile && !isTablet){
			$('#menu-btn').removeClass('active');
			$('#menu').css({left:'-100%'});
			$('.page').scrollTop(0);
		}
		if(isTablet || w <1000){
			$(this).parents('.open').removeClass('open');
		}
		var elem = $(this);
		var film = elem.parents('#menu ul li')[0].className.split('-')[0];
		elem.trigger('mouseleave');
		if(!elem.hasClass('selected')){
			$('#menu').find('.submenu .selected').removeClass('selected');
			elem.addClass('selected');
			closeRSVP();
			var page = elem.html().toLowerCase();

			if(page==='screenings &amp; synopsis'){
				page = 'screenings';
			}

			page = page.split(' ').join('-');
			var index = elem.index();
			currPageIndex = index;
			
			//Load new page or subpage
			slideFilmPage(film,page);
		}		
	});
	$('body').on('click', '.pop-up-close', function(){
		$('.pop-up').fadeOut(400);
	})
	$('body').on('click','.guild-members-co', function(){
		$('.pop-up').fadeIn(400);
	})
	/*********************************************************************/
	////////////////////////////// MEDIA EVENTS ///////////////////////////
	$('.page').on('click touchstart', '.play-btn',function(e){
		e.preventDefault();e.stopPropagation();e.stopImmediatePropagation();
		var audio = $(this).prev()[0];
		if($(this).parent().hasClass('playing')){
			audio.pause();
			audio.currentTime = 0;
			$(this).parent().removeClass('playing');
		}else{
			stopMedia();
			audio.play();
			//$('.play-btn').removeClass('playing');
			$(this).parent().addClass('playing');
			
		}
	});

	$('video').not($('.box').find('video')).each(function(){
		this.addEventListener('play', function(e){
			console.log(e.target)
			$(e.target).next().addClass('playing').animate({opacity:0},300);
			//dataLayer.push({event:this.src})
		});
		this.addEventListener('pause',function(e){
			$(e.target).next().removeClass('playing').animate({opacity:1},300);
		});
		this.addEventListener('ended',function(e){
			$(e.target).next().removeClass('playing').animate({opacity:1},300);
			e.target.load();
		})
	})
	$('body').on('touchstart click', '.video-play-btn',function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var video = $(this).prev()[0];
		if($(this).hasClass('playing')){
			video.pause();
			//video.currentTime = 0;
			$(this).removeClass('playing');
			//video.load();
			$(this).animate({opacity:1},300);
		}else{
			stopMedia();
			video.play();
			$(this).addClass('playing');
			$(this).animate({opacity:0},300);
		}
	});
	$('body').on('click touchstart','.poster', function(e){
		e.preventDefault();
		$(this).fadeOut();
		$(this).prev()[0].play();
	})
	$('#gotg-page').on('click','.video-title', function(){
		$('#gotg-video').fadeIn();
		$('#gotg-video').find('video')[0].src=$(this).attr('data-src');
		$('#gotg-video').find('video')[0].load();
		$('#gotg-video').find('video')[0].play();
	});
	$('body').on('click', '#gotg-video .close-btn', function(){
		$(this).siblings('video')[0].pause();
		$(this).parent().fadeOut();
	});
	/////////////////////////////////////////////////////////
	//FILM MAKER NAMES EVENT
	$('body').on('click', '.name',function(){
		var index = $(this).index();
		console.log(index);
		$(this).siblings('.current').removeClass('current');
		$(this).addClass('current');
		$(this).parents('.content').find('.bio').fadeOut(300);
		$loaded.find('.bg.filmmakers').find('.bg').eq(index).css({zIndex:2,opacity:1});
		$loaded.find('.bg.filmmakers').find('.bg').not($loaded.find('.bg.filmmakers').find('.bg').eq(index)).css({zIndex:3}).animate({opacity:0},300, function(){
			$(this).css({zIndex:3});
		});
		$(this).parents('.content').find('.bio').eq(index).stop().delay(300).fadeIn(300, function(){
			var that= this;
			setTimeout(function(){checkScroll($(that).find('.scrollable'))},100);
		});
	});
	////////////////// BEAUTY EVENTS /////////////////////////
	$('#batb-page').on('click', '.name', function(){
		if($(this).index() == 0){
			$('.bottom-quote').fadeIn()
		}else{
			$('.bottom-quote').fadeOut()
		}
		$('#batb-page').find('.filmmakers').find('.quote').find('> div').hide().eq($(this).index()).show();
	});
	$('body').on('click','.head', function(){
		var index = $(this).index();
		if(!$(this).hasClass('active')){
			$(this).siblings('.active').removeClass('active');
			$(this).addClass('active');
			$('.cast-bg').eq(index).fadeIn().nextAll().fadeOut();
		}
	})
	$('body').on('touchstart click','.thumb', function(e){
		e.preventDefault();e.stopImmediatePropagation();e.stopPropagation();
		if($(this).hasClass('active'))
			$(this).removeClass('active');
		else{
			$('body').one('touchstart click', function(){
				$('.thumb.active').removeClass('active');
			});
			$(this).addClass('active').siblings('.active').removeClass('active');
		}
	});
	var overlayOpened = false;
	$('body').on('click', '.openOverlay',function(){
		var id = $(this).attr('id');
		if(!overlayOpened){
			overlayOpened = true;
			$.get('../php/overlay-content.php').done(function(data){
				$('#batb-overlay .overlay-content').html(data);

				$('#batb-overlay').find('.overlay-content > div').hide().filter($('.'+id)).show();
				$('#batb-overlay').fadeIn( function(){
					if(!isMobile)
					checkScroll($('.women-of').find('.scrollable'));
				});
				$('#batb-overlay').find('.close-btn').one('click', function(){
					$('#batb-overlay').fadeOut();
					$('#batb-overlay').find('video').each(function(){
						this.pause();
						this.load();
					});
				})
			})
		}else{
			var id = $(this).attr('id');
			$('#batb-overlay').find('.overlay-content > div').hide().filter($('.'+id)).show();
			$('#batb-overlay').fadeIn( function(){
				if(!isMobile)
				checkScroll($('.women-of').find('.scrollable'));
			});
			$('#batb-overlay').find('.close-btn').one('click', function(){
				$('#batb-overlay').fadeOut();
				$('#batb-overlay').find('video').each(function(){
					this.pause();
					this.load();
				});
			});
		}
	})
	//////////////////////////////// RSVP FORM STUFF /////////////////////////////////
	$('body').on('click','.rsvp-btn',function(){
		var screeningId = $(this).attr('data-screening-id');
		var screeningInfo = filmInfo[loaded].screenings[screeningId];
		if(screeningInfo.status == 'open' && screeningInfo.official !=='1'){
			console.log(screeningInfo);
			$('#screening-id').val(screeningInfo.id);
			var form = $('#rsvp-form');
			if(screeningInfo.theater.metro=='London'){
				form.find('.london').show();
				form.find('.not-london').hide();
			}else{
			//LOAD DROPDOWNS
				form.find('.london').hide();
				form.find('.not-london').show();
				form.find('.dropdown').html('');
				for(var i=0;i<Number(screeningInfo.guests)+1;i++){
					form.find('.guests').find('.dropdown').append('<div class="value">'+i+'</div>')
				}
				for(var i=0;i<screeningInfo.affil.length;i++){
					form.find('.guilds').find('.dropdown').append('<div class="value">'+screeningInfo.affil[i]+'</div>')
				}
				$('.guestname').remove();
				if(screeningInfo.theater.id == 25 || screeningInfo.theater.id == 56 || screeningInfo.theater.id == 64 || screeningInfo.theater.id == 48){

					for(var i=0;i<Number(screeningInfo.guests);i++){
						form.find('#rsvp-submit').before("<div style='display:none' class='input fullWidth guestname'><input type='text' name='guest_name"+(i+1)+"' placeholder='GUEST NAME'><div class='error'></div></div>");
					}
				}
			}
			form.find('.movie').html(filmInfo[loaded].title);
			form.find('.date').html(parseDate(screeningInfo['datetime'],screeningInfo.theater.metro)+', ');
			form.find('.theater').html(screeningInfo['theater'].name);
			//form.find('.time').html(parseTime(screeningInfo['datetime']));
			form.find('.rsvp-close-btn').one('touchstart click', closeRSVP)

			form.show();
		}
		if(screeningInfo.official ==='1'){
			window.open('http://www1.oscars.org/screenings/nominations/index.html#LA')
		}
	});
	function closeRSVP(){
		var form = $('#rsvp-form');
		form.hide();
		form.find('form').show()[0].reset();
		form.find('.guests').find('span').html('GUESTS');
		form.find('.guilds span').html('GUILD');
		form.find('#rsvp-message').html('');
		path[3] = false;
	}
	$('body').on('click touchstart','.hasDropdown', function(e){
		e.stopPropagation();
		$('.dropdown').hide();
		if($(this).hasClass('guilds'))
			$('.guilds').removeClass('error').next('.error').html('');
		$(this).find('.dropdown').show().one('mouseleave', function(){
			$(this).hide();

		});
		$(this).find(".dropdown").find('.value').on('click', function(e){
			e.preventDefault();e.stopPropagation();e.stopImmediatePropagation();
			
			if($(this).parents('.hasDropdown').hasClass('guests')){
				$('#guests').val($(this).html());
				$('.guestname').hide();
				if(Number($(this).html()) > 0){
					$('.guestname').hide();
					for(var i=0;i<Number($(this).html());i++){
						$('input[name="guest_name'+(i+1)+'"]').parent().show();
					}
				}
			}else if($(this).parents('.hasDropdown').hasClass('guilds')){
				$('#guild').val($(this).html());
			
			}
			$(this).parents('.hasDropdown').find('span').html($(this).html());
			
		});
	});
	$('body').on('focus', 'input', function(){
		$(this).removeClass('error').next('.error').html('');
	})
	//RSVP SUBMIT
	var processing = false;
	$('body').on('click','#rsvp-submit', function(){
		if(!processing){
			processing = true;
			var form = $('#rsvp-form');
			var error = false;
			if($('#first_name').val()==''){
				$('#first_name').addClass('error').next('.error').html('Field Required.');
				error = true;
			}
			console.log("error:",error)
			if($('#last_name').val()==''){
				$('#last_name').addClass('error').next('.error').html('Field Required.');
				error=true;
			}
			console.log("error:",error)
			if(!$('#email').val().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
				$('#email').addClass('error').next('.error').html('Invalid Email');
				error = true;
			}
			console.log("error:",error)
			if($('#guild').val()==''){
				$('.guilds').addClass('error').prev('.error').html('Please choose a guild affiliation.');
				error = true;
			}
			console.log("error:",error)
			if($('#guests').val()==''){
				$('#guests').val('0');
			}else{
				for(var i = 0;i<Number($('#guests').val());i++) {
					if($('.guestname').eq(i).find('input').val()==''){
						error = true;
						$('.guestname').eq(i).find('input').addClass('error').next('.error').html('Field Required.');
					}
				}
			}
			console.log("error:",error)
			if(!error){
				var url = 'disney-ampas';
				if(location.host=='dev-waltdisneystudiosawards.wds.io' || location.host=='www.dev-waltdisneystudiosawards.wds.io')
					url = 'dev-disneystudiosawards.wds.io';
				if(location.host=='waltdisneystudiosawards.com' || location.host=='www.waltdisneystudiosawards.com')
					url ='disneystudiosawards.com';
				var data = form.find('form').serialize();
				$.post('http://'+url+'/admin/attendees/register', data).done(function(data){
					console.log(data);
					processing = false;
					form.find('form').hide().next().show().html(data.message);
					if(data.err!='true'){
						form.find('form')[0].reset();
						form.find('.guests').find('span').html('GUESTS');
						form.find('.guilds').find('span').html('GUILD');
					}
					
		          	addtocalendar.load();
		            
				
				}).fail(function(data){
					console.log(data);
					console.log('Fail')
					processing = false;
				})
			}else{
				processing = false;
			}
		}
	});
	$('body').find('.content').on('scroll','.press', function(){
		console.log(this);
		$(this).prev().hide();
		//$(this).parent().prev().hide();
	});
    ///////////////////////////////////////////////////////////////////////////////

	$('body').on('click touchend','.city', function(){
		if(!$(this).hasClass('selected')){
			$(this).siblings('.selected').removeClass('selected');
			$(this).addClass('selected');
			var city = $(this).html();
			$(this).parent().siblings('.city-holder').fadeOut(200);
			$(this).parent().siblings('.'+city.split(' ').join('_')+'-holder').stop().delay(200).fadeIn(400, function(){
				checkScroll($(this).find('.scrollable'));
			});
		}
	})
	// PHOTOS ARROW
	$('body').on('click', '.arrow', function(){
		var photos = $(this).siblings('.slider').find('.photo');
		var dots = $(this).siblings('.dots');
		var currImgIndex = photos.index(photos.parent().find('.viewing'))
		var totalImages = photos.length-1;
		if($(this).hasClass('left-arrow')){
			//LEFT ARROW
			if(currImgIndex == 0){
				currImgIndex = totalImages;
				photos.eq(0).animate({left:'100%'});
				photos.eq(currImgIndex).css({left:'-100%'}).animate({left:'0%'}, function(){
					photos.eq(0).removeClass('viewing');
					$(this).addClass('viewing');
					dots.find('.active').removeClass('active');
					dots.find('.dot').eq(totalImages).addClass('active');
				});
			}else{
				photos.eq(currImgIndex).animate({left:'100%'});
				photos.eq(currImgIndex-1).css({left:'-100%'}).animate({left:'0%'}, function(){
					photos.eq(currImgIndex).removeClass('viewing');
					$(this).addClass('viewing');
					dots.find('.active').removeClass('active');
					dots.find('.dot').eq(currImgIndex-1).addClass('active');
				});
			}
	
		}else{
			//RIGHT ARROW
			if(currImgIndex == totalImages){
				currImgIndex = 0;
				photos.eq(totalImages).animate({left:'-100%'});
				photos.eq(currImgIndex).css({left:'100%'}).animate({left:'0%'}, function(){
					photos.eq(totalImages).removeClass('viewing');
					$(this).addClass('viewing');
					dots.find('.active').removeClass('active');
					dots.find('.dot').eq(0).addClass('active');
				});
			}else{
				photos.eq(currImgIndex).animate({left:'-100%'});
				photos.eq(currImgIndex+1).css({left:'100%'}).animate({left:'0%'}, function(){
					photos.eq(currImgIndex).removeClass('viewing');
					$(this).addClass('viewing');
					dots.find('.active').removeClass('active');
					dots.find('.dot').eq(currImgIndex+1).addClass('active');
				});
			}
		}
	});
	//HANDLE RESIZE
	$(window).resize(resize);

	//SCREENINGS RELOAD
	$('body').on('click','.reload', function(e){
		e.preventDefault();
		$(this).parents('.scrollable').html('<img class="loader" src="img/loader.gif">');
		getScreenings(loaded);
	})
});
function onEnd(){
	console.log('video end')
	history.pushState({page:filmInfo[loaded].page},filmInfo[loaded].title,'/'+filmInfo[loaded].url);
	loadFilmPage(loaded);
	animating = false;
}
function moveFooter(film,index){
	if(film=='batb'){
		if(index==3 || index == 4 || index == 5)
			$loaded.find('footer').css({textAlign:'right',color:'#fff'});
		else if(index==8)
			$loaded.find('footer').css({textAlign:'left',color:'#153f7b'});
		else
			$loaded.find('footer').css({textAlign:'left',color:'#fff'});
	}else if(film=='cars'){
		if(index == 1 || index==4 || index==3)
			$loaded.find('footer').css({textAlign:'right',color:'#fff'});
		else if(index==7)
			$loaded.find('footer').css({textAlign:'left',color:'#153f7b'});
		else
			$loaded.find('footer').css({textAlign:'left',color:'#fff'});
	}else if(film=='lou'){
		if(index==3)
			$loaded.find('footer').css({textAlign:'right'});
		else
			$loaded.find('footer').css({textAlign:'left'});
	}else if(film=="coco"){
		if(index==4)
			$loaded.find('footer').css({textAlign:'right',color:'#fff'});
		else if(index==6){
			$loaded.find('footer').css({textAlign:'left',color:'#153f7b'});
		}else
			$loaded.find('footer').css({textAlign:'left',color:'#fff'});
	}else if(film=="thor"){
		if(index==3 || index==5)
			$loaded.find('footer').css({textAlign:'right'});
		else
			$loaded.find('footer').css({textAlign:'left'});
	}else if(film=='gotg'){
		if(index==6)
			$loaded.find('footer').css({textAlign:'right',color:'#fff'});
		else if(index==8)
			$loaded.find('footer').css({textAlign:'left',color:'#153f7b'});
		else
			$loaded.find('footer').css({textAlign:'left',color:'#fff'});
	}else if(film=='sw'){
		if(index==0)
			$loaded.find('footer').css({textAlign:'right'});
		else if(index==6)
			$loaded.find('footer').css({textAlign:'left',color:'#153f7b'});
		else
			$loaded.find('footer').css({textAlign:'left'});
	}else{
		$loaded.find('footer').css({textAlign:'left'});
	}
}
function getScreenings(film){
	var url = 'disneystudiosawards.com';
	if(location.host=='dev-waltdisneystudiosawards.wds.io' || location.host=='www.dev-waltdisneystudiosawards.wds.io')
		url = 'dev-disneystudiosawards.wds.io';
	if(location.host=='waltdisneystudiosawards.com' || location.host=='www.waltdisneystudiosawards.com')
		url ='disneystudiosawards.com';
	$.get('http://'+url+'/screenings.php?movie_id='+(movieIDIndex.indexOf(film)+1)).done(function(data){
		
		console.log(JSON.parse(data));
		data = JSON.parse(data);
		filmInfo[film].screenings = data;
		//$('#'+film+'-page').find('.screenings .content').html('<div id="'+film+'-screenings-table"></div>');
		if(data !=false) {
			$('#'+film+'-page').find('.city-holder .scrollable').html('');
			for (var key in data) {
				if(film == 'batb'){
					$('#'+film+'-page').find('.screenings .content').find('.screenings-holder').append('<div class="screening"><div class="date">'+data[key].theater.metro +': '+parseDate(data[key].datetime,data[key].theater.metro)+'</div>'+((data[key].official==='1') ? '<h4>OFFICIAL AMPAS SCREENING</h4>':'')+'<div class="theater">'+data[key].theater.name+' - '+data[key].theater.address+'</div></div>');
						if(data[key].status=='closed'){
							$('#'+film+'-page').find('.screenings .content').find('.screenings-holder').find('.screening').last().append('<div class="btn" data-screening-id="'+key+'">COMING SOON</div>');
						}else if(parseInt(data[key].rsvps) >= parseInt(data[key].capacity)){
							$('#'+film+'-page').find('.screenings .content').find('.screenings-holder').find('.screening').last().append('<div class=" btn" data-screening-id="'+key+'">SCREENING FULL</div>');
						}else{
							$('#'+film+'-page').find('.screenings .content').find('.screenings-holder').find('.screening').last().append('<div class="rsvp-btn btn" data-screening-id="'+key+'">RSVP</div>');
						}
				}else{
					$('#'+film+'-page').find('.screenings .content').find('.'+data[key].theater.metro.split(' ').join('_')+'-holder').find('.scrollable').append('<div class="screening"><div class="date">'+parseDate(data[key].datetime,data[key].theater.metro)+'</div>'+((data[key].official==='1') ? '<h4>OFFICIAL AMPAS SCREENING</h4>':'')+'<div class="theater">'+data[key].theater.name+'</div><div class="address">'+data[key].theater.address+'<a href="https://maps.google.com/?q='+data[key].theater.address+', '+data[key].theater.address_2+'" target="_blank"></a></div>'+((data[key].id=="148") ? '<div class="qa">Followed by Q&A with Neal Scanlan, Steve Aplin and James Martin.</div>':'')+((data[key].id=="153") ? '<div class="qa">Followed by a Q&A with Ben Morris, Mike Mulholland, Chris Corbould, Michael Semanick,<br/> and Stuart Wilson.</div>':'')+'</div>');
					if(data[key].status=='closed'){
						$('#'+film+'-page').find('.screenings .content').find('.'+data[key].theater.metro.split(' ').join('_')+'-holder').find('.scrollable').find('.screening').last().append('<div class="btn" data-screening-id="'+key+'">COMING SOON</div>');
					}else if(parseInt(data[key].rsvps) >= parseInt(data[key].capacity)){
						$('#'+film+'-page').find('.screenings .content').find('.'+data[key].theater.metro.split(' ').join('_')+'-holder').find('.scrollable').find('.screening').last().append('<div class=" btn" data-screening-id="'+key+'">SCREENING FULL</div>');
					}else{
						$('#'+film+'-page').find('.screenings .content').find('.'+data[key].theater.metro.split(' ').join('_')+'-holder').find('.scrollable').find('.screening').last().append('<div class="rsvp-btn btn" data-screening-id="'+key+'">RSVP</div>');
					}
				}
				
			}
			
		}
		
		if(film=='batb'){
			checkScroll($('#batb-page').find('.scrollable'));
			if($('#'+film+'-page').find('.screenings').find('.scrollable').find('.screenings-holder').find('.screening').length==0){
				$('#'+film+'-page').find('.screenings').find('.scrollable').find('.screenings-holder').html('No screenings available.');
			}
		}else{
			$('#'+film+'-page').find('.screenings').find('.scrollable').each(function(){
				if($(this).find('.screening').length == 0){
					$(this).html('No screenings available.');
				}
			});
			checkScroll($('#'+film+'-page').find('.Los_Angeles-holder').find('.scrollable'));
		}
		ajax.resolve({status:true});
	}).fail(function(data){
		console.log('fail')
		$('#'+film+'-page').find('.screenings').find('.scrollable').each(function(){
			if($(this).find('.screening').length == 0){
				$(this).html('<h3>There was an error retrieving screenings! Click <span class="reload">here</span> to try again.</h3>');
			}
		});

	});
}
var months = ['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'];
function parseDate(timestamp, city){

	var date = new Date(timestamp*1000);
	//console.log(isDST(date));
	//console.log('isDST?',isDST(date))
	if(city=='Los Angeles' || city == 'San Francisco' || city=='Other'){
		var offset = 7+isDST(date);
	}else if(city == 'New York')
		var offset = 4+isDST(date);
	else if (city == 'London')
		var offset = 0;

	var month = months[date.getUTCMonth()]
	var day = date.getUTCDate();
	//console.log(date.getUTCDate(), date.getUTCHours())
	var hours = date.getUTCHours() - offset;
	if(hours < 0){
		day -=1;
		hours +=24;
	}
	var minutes = date.getMinutes();
	var time = [hours,(minutes < 10) ? '0'+minutes:minutes];
	var ampPM = (time[0]>=12) ? 'PM':'AM';
	//return;
	if (time[0] > 12)
		time[0] -= 12;
	return  month +' '+day+', '+time[0]+':'+time[1]+' '+ampPM;
}
function isDST(t) { //t is the date object to check, returns true if daylight saving time is in effect.
    var jan = new Date(t.getFullYear(),0,1).getTimezoneOffset();
    var jul = new Date(t.getFullYear(),6,1).getTimezoneOffset();
    if(Math.max(jan,jul) == t.getTimezoneOffset())
    	return 1;
    else
    	return 0;
}
function resize(){
	headerHeight = $('header').height();
	w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	Orientation = (w >= h) ? 'landscape':'portrait';
	if(loaded !=null){
		var bgW = (h-headerHeight) *filmInfo[loaded].bgRatio[currPageIndex];
		var alg = (-(bgW-w)/2)*filmInfo[loaded].bgRatios[currPageIndex];
		if(alg > 0)
			alg = 0;
		else if(alg < -(bgW-w))
			alg = -(bgW-w);
		$('#page-holder').css({height:h-headerHeight})
		$loaded.find('.bg-holder > .bg').eq(currPageIndex).css({'background-position-x':alg+'px'});
		checkScroll();
	}
}
function loadHome(){
	if(isMobile && !isTablet){
		$('#wrapper').fadeIn();
		$('header').css({position:'fixed'});
	}
	console.log('loadHome');
	$('.limiter').removeAttr('style')
	$('.page').find('.limiter .vertical').find('> div').not('.logo').fadeOut(400);
	$('#menu').find('.selected').removeClass('selected');
	$('#'+loaded+'-page').addClass('shrink');
	var oldLoaded = loaded;
	setTimeout(function(){
		//$('.page.loaded').removeClass('loaded').removeAttr('style');
		$('#page-holder').hide();
		$('#'+oldLoaded+'-page').removeClass('shrink loaded').removeAttr('style');
		
	},500);
	loaded =null;
	$loaded = null;
	$('header').animate({top:'-100%'},500);
	$('video').each(function(){
		this.currentTime =0;
		this.load();
		$(this).next().removeAttr('style');
	});
}

function loadFilmPage(film,page,stat){
	console.log('Load Page ',film,page);
	if(!filmInfo[film].pageLoaded) {
		$.get('../php/'+film+'-page.php').done(function(data){
			$('#'+film+'-page').html(data);
			filmInfo[film].pageLoaded = true;
			load(film,page,stat);
		});
	}else{
		load(film,page,stat);
	}
}
function load(film,page,stat){
	if(isMobile && !isTablet){
		$('#wrapper').fadeOut();
		$('header').css({position:'relative'});
	}
	$('#menu ul li').removeClass('selected');
	$('#menu').find('.'+film+'-btn').addClass('selected');
	var height = $('#'+film+'-box').height();
	var width = $('#'+film+'-box').width();
	var offset = $('#'+film+'-box').offset();
	headerHeight = $('header').height();
	if(!page || page==''){
		page = 'consider';
	}else{
		//history.replaceState({page:film},filmInfo[film].title, '/');
		history.replaceState({film:film,page:page},filmInfo[film].title, '/'+filmInfo[film].url+'/'+page);
	}
	var pageIndex = filmInfo[loaded].pageList.indexOf(page);
	currPageIndex = pageIndex;
	loaded = film;
	$loaded = $('#'+film+'-page');
	$('header').animate({top:0},((stat)? 0:300));
	$('#page-holder').show();
	$loaded .show();
	if(film=='thor' || film =='coco' || film=='sw'){
		if(page=='screenings') {
			$('.pop-up').fadeIn();
			$('.guild-members-co').fadeIn();
		}else{
			$('.pop-up').fadeOut();
			$('.guild-members-co').fadeOut();
		}
	}else{
		$('.pop-up').fadeOut();
		$('.guild-members-co').fadeOut();
	}
	moveFooter(film,pageIndex);
	if(page =='screenings'){
		getScreenings(film);
	}else if(page=='score'){
		loadScore(film);
	}else if(page=='photos'){
		loadPhotos(film);
	}
	if(path[3]){
		$.when(ajax).done(function(){
			var screeningId = path[3];
			var screeningInfo = filmInfo[film].screenings.find(function(currValue){
				return currValue.id==screeningId;
			})

			if(screeningInfo.status == 'open'){
				console.log(screeningInfo);
				$('#screening-id').val(screeningInfo.id);
				var form = $('#rsvp-form');
				if(screeningInfo.theater.metro=='London'){
					form.find('.london').show();
					form.find('.not-london').hide();
				}else{
				//LOAD DROPDOWNS
					form.find('.london').hide();
					form.find('.not-london').show();
					form.find('.dropdown').html('');
					for(var i=0;i<Number(screeningInfo.guests)+1;i++){
						form.find('.guests').find('.dropdown').append('<div class="value">'+i+'</div>')
					}
					for(var i=0;i<screeningInfo.affil.length;i++){
						form.find('.guilds').find('.dropdown').append('<div class="value">'+screeningInfo.affil[i]+'</div>')
					}
					
				}
				form.find('.movie').html(filmInfo[loaded].title);
				form.find('.date').html(parseDate(screeningInfo['datetime'],screeningInfo.theater.metro)+', ');
				form.find('.theater').html(screeningInfo['theater'].name);
				//form.find('.time').html(parseTime(screeningInfo['datetime']));
				form.find('.rsvp-close-btn').one('touchstart click', function(){
					form.hide();
					form.find('form').show()[0].reset();
					form.find('#rsvp-message').html('');
				})

				form.show();
			}
		});
	}

	/* SETS BACKGROUND OFFSET BASED ON RATIO SET IN FILMINFO */
	var bgW = (h-headerHeight) *filmInfo[film].bgRatio[pageIndex];
	var alg = (-(bgW-w)/2)*filmInfo[film].bgRatios[pageIndex];
	if(alg > 0)
		alg = 0;
	else if(alg < -(bgW-w))
		alg = -(bgW-w);
	$loaded.find('.bg-holder > .bg').eq(pageIndex).animate({'background-position-x':alg+'px',opacity:1},((stat)? 0:300));
	$loaded.find('.'+page).delay(((stat)? 0:200)).fadeIn(((stat)? 0:400));
	$loaded.find('footer').delay(((stat)? 0:200)).fadeIn(((stat)? 0:400));
	$loaded.addClass((stat)?'stat':'').css({'transform':'scale(1)',left:0}).animate({},((stat)? 0:300), function(){
	 	$(this).removeClass('animating')
	 	
	  	headerHeight = $('header').height();

	  	//console.log('headerHeight loadFilm: ',headerHeight);
	 	$('#page-holder').css({height:h - headerHeight});
	 	$('.page').css({'transform':'scale(1)'});
		
		$('.'+film+'-btn').find('.submenu > div').eq(pageIndex).addClass('selected');
		//$('.limiter').css({width:(h-headerHeight)*1.4})
		setTimeout(checkScroll,500);
	});
}
var interval;

function slideFilmPage(film, page){
	console.log('Slide Page',film,page,loaded)
	stopMedia();
	var pageIndex;
	//film == selected film
	//loaded == old film
	if(!filmInfo[film].pageLoaded) {
		$.get('../php/'+film+'-page.php').done(function(data){
			$('#'+film+'-page').html(data);
			filmInfo[film].pageLoaded = true;
			slide(film,page);
		});
	}else{
		slide(film, page);
	}
}
function slide(film,page){
	if(page==="screenings &amp; synopsis")
		page='screenings';
	page.split(' ').join('-');
	if(loaded==null){
		$('.page').css({'transform':'scale(1)'});
		$('#'+film+'-page').css({left:0});
		$('#wrapper').fadeOut();
		$('header').css({position:'relative'});
		$('#page-holder').fadeIn();
		loaded = film;
		history.pushState({film:film,page:page}, filmInfo[film].title, filmInfo[film].url);
	}
	if(film != loaded){
		//loaded = film;
		$('#menu ul').find('.selected').removeClass('selected');
		$('#menu').find('.'+film+'-btn').addClass('selected');
		
	}else{
		
	}
	history.pushState({film:film,page:page}, filmInfo[film].title, '/'+filmInfo[film].url+'/'+page);
	if(!page){
		page = 'consider';
		pageIndex = 0;
		
	}else{
		pageIndex = filmInfo[film].pageList.indexOf(page);
		
	}
	
	$('.'+film+'-btn').find('.submenu > div').eq(pageIndex).addClass('selected');
	var bgW = (h-headerHeight) *filmInfo[film].bgRatio[pageIndex];
	var alg = (-(bgW-w)/2)*filmInfo[film].bgRatios[pageIndex];
	if(alg > 0)
		alg = 0;
	else if(alg < -(bgW-w))
		alg = -(bgW-w);
	$('video').each(function(){
		if(!this.paused){
			this.pause();
			$(this).next().fadeIn();
		}
	})
	//Adjust footer
	$loaded = $('#'+film+'-page');
	moveFooter(film,pageIndex);
	if(page =='screenings'){
		getScreenings(film);
	}else if(page=='score'){
		loadScore(film);
	}else if(page=='photos'){
		loadPhotos(film);
	}
	if(film=='thor' || film=='coco' || film=='sw') {
		if(page=='screenings') {
			$('.pop-up').fadeIn();
			$('.guild-members-co').fadeIn();
		}else{
			$('.pop-up').fadeOut();
			$('.guild-members-co').fadeOut();
		}
	}else{
		$('.pop-up').fadeOut();
		$('.guild-members-co').fadeOut();
	}
	if(film=='batb' && page== 'makingof') {
		var i = 0;
		interval = setInterval(function(){
			$('#batb-page').find('.makingof').find('.quote > div').eq(i).animate({opacity:0},1000);
			i++;
			if(i>4)
				i = 0;
			$('#batb-page').find('.makingof').find('.quote > div').eq(i).animate({opacity:1},1000)
		},5000)
	}else{
		clearInterval(interval);
	}
	if(film=='batb'){
		$('.cast-bg').fadeOut();
	}
	if(film ==loaded){
		$('#'+film+'-page').find('.bg-holder').find('> .bg').eq(pageIndex).css({opacity:1, zIndex:2,'background-position-x':alg+'px'});
		$('#'+film+'-page').find('.bg-holder > .bg').not($('#'+film+'-page').find('.bg-holder > .bg').eq(pageIndex)).css({zIndex:3}).animate({opacity:0});
		$('#'+film+'-page').find('.limiter').children().fadeOut(400);
		$('#'+film+'-page').find('.'+page+',footer').stop(true).delay(400).fadeIn(400);
	}else{
		var i = 0;
		$('#'+loaded+'-page').find('.limiter').children().fadeOut(400, function(){
			if(i==0){
				
				$('#'+film+'-page').find('.bg-holder > .bg').eq(pageIndex).css({opacity:1, zIndex:2,'background-position-x':alg+'px'});
				$('#'+film+'-page').find('.bg-holder > .bg').not($('#'+film+'-page').find('.bg-holder > .bg').eq(pageIndex)).css({zIndex:3}).animate({opacity:0});
				$('#'+film+'-page').css({zIndex:3}).show().animate({left:0},400, function(){
					console.log('loaded: ',loaded)
					$('#'+loaded+'-page').css({left:'100%',zIndex:2})
					$(this).css({zIndex:2});
					loaded = film;
				});

				$('#'+film+'-page').find('.'+page).stop(true,true).delay(200).fadeIn(400);
				$('#'+film+'-page').find('footer').stop(true,true).delay(200).fadeIn(400);
				
				//
			}
			i++;
		
		});
	}
	setTimeout(checkScroll,500);
}
function loadScore(film){
	if(!filmInfo[film].scoreLoaded) {
		filmInfo[film].scoreLoaded = true;
		for(var i = 0; i < filmInfo[film].score.titles.length; i ++){
			$('#'+film+'-page').find('.score').find('.track-list').find('.scrollable').append('<div class="track"><audio class="score-song" preload="auto"><source src="/play.php?sub='+film+'&file='+filmInfo[film].score.files[i]+'"></audio><div data-song="'+filmInfo[film].score.titles[i]+'" class="play-btn"></div><div class="track-info"><div class="track-name">'+filmInfo[film].score.titles[i]+'</div></div></div>')
		}
		$('#'+film+'-page').find('.score').find('.track-list').find('.scrollable').find('.score-song').each(function(){
			this.addEventListener('ended', function(){
				playNextSong($(this).parent());
			})
		})
			

		
	}
}
function playNextSong(parent){
	console.log('song ended');
	console.log(parent.next('.track').length)
	if(parent.next('.track').length==0){
		console.log(parent.siblings('.track').first())
		parent.removeClass('playing').siblings('.track').first().addClass('playing').find('audio')[0].play();
	}else
		parent.removeClass('playing').next('.track').addClass('playing').find('audio')[0].play();
}
function loadPhotos(film){
	console.log('load photos', film,filmInfo[film].photos)
	$('#'+film+'-page').find('.photos').find('.slider').html('');
	$('#'+film+'-page').find('.dots').html('');
	for(var i=1;i<filmInfo[film].photos+1;i++){
		$('#'+film+'-page').find('.photos').find('.slider').append('<div class="photo"><img src="/img/'+film+'/photos/'+i+'.jpg"/></div>')
		$('#'+film+'-page').find('.dots').append('<div class="dot"></div>');
		
	}
	$('#'+film+'-page').find('.photos').find('.photo').eq(0).addClass('viewing');
	$('#'+film+'-page').find('.dot').eq(0).addClass('active');
}
function checkScroll(elem){
	if(!isMobile && !isTablet){
		if(elem != null){
			addScroller(elem);
		}else{
			$('.scrollable').each(function(){
				addScroller($(this));
			});
		}
	}
}
function addScroller(elem){
	var parent = elem.parent();
	var elemH = elem.innerHeight();
	var parentH = parent.height();
	var diff = parentH - elemH;
	//console.log(elemH,parentH)
	var trackH = parent.height();
	var ctrlSize = (parentH /elemH)*trackH;
	//console.log(elemH,parentH)
	
	parent.removeClass('scrollerActive').find('.scroller').remove();
	parent.off('wheel');
	elem.css({top:0})
	if(elemH > parentH){
		//console.log(elem,'Needs scroll')
		parent.prev('.scroll-down').show();
		ctrlSize = 40;
		parent.css({'overflow':'hidden'}).addClass('scrollerActive').append("<div class='scroller'><div class='scroll-track'></div><div class='scroll-ctrl' style='height:"+ctrlSize+"px;'></div></div>");
		parent.find('.scroll-ctrl').on('mousedown',function(e){
			var ctrl = $(this);
			var ctrlH = ctrl.height();
			var upperLimit = 0;
			var bottomLimit = parentH - ctrl.height();
			var scrollerOffset = ctrl.parent().offset().top;
			
			
			
			$('body').on('mousemove',function(e){
				var x = e.pageY;
				//console.log(e.pageY,scrollerOffset,x-scrollerOffset)
				var move = x - scrollerOffset-(ctrlH/2);
				if(move>bottomLimit)
					move = bottomLimit;
				else if(move < upperLimit)
					move = upperLimit;
				ctrl.css({top:move});
				elem.css({top:((move)/(trackH-ctrlH))*diff})
			}).on('mouseup',function(){
				$(this).off('mousemove')
			})
		}).on('mouseup', function(){

		});
		parent.on('wheel', function(e){
			var ctrl = $(this).find('.scroll-ctrl');
			var ctrlH = ctrl.height();
			var upperLimit = 0;
			var bottomLimit = parentH - ctrl.height();
			var currTop = parseInt(ctrl.css('top'));
			parent.prev('.scroll-down').fadeOut();
			var move = e.originalEvent.deltaY+currTop;
			if(move>bottomLimit)
				move = bottomLimit;
			else if(move < upperLimit)
				move = upperLimit;
			ctrl.css({top:move});
			elem.css({top:((move)/(trackH-ctrlH))*diff})
		})
	}else{
		parent.find('.scroller').remove();
	}
}
function stopMedia(){
	console.log($('.playing').length);
	if($('.playing').length != 0){
		$('.playing').find('.play-btn').prev()[0].pause();
		$('.playing').find('.play-btn').prev()[0].currentTime = 0;
		$('.playing').find('.play-btn').prev()[0].load();
		$('.playing').removeClass('playing').removeAttr('style');
	}
}