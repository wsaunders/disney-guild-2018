<?php require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>
<!DOCTYPE html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W972SMX');</script> 
<!-- End Google Tag Manager -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=eE5evalxQk">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=eE5evalxQk">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=eE5evalxQk">
<link rel="manifest" href="/manifest.json?v=eE5evalxQk">
<link rel="mask-icon" href="/safari-pinned-tab.svg?v=eE5evalxQk" color="#5bbad5">
<link rel="shortcut icon" href="/favicon.ico?v=eE5evalxQk">
<meta name="theme-color" content="#ffffff">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="description" property="og:description" content="Walt Disney Studios 2017 For Your Consideration"/>
<meta property="og:image" content="http://waltdisneystudiosawards.com/img/preview-image.jpg"/>
<title>Disney Guild Awards 2017</title>

<?php if(!$isMobile && !$isTablet) { ?>
<link rel="stylesheet" type="text/css" href="/css/style.css">
<?php } ?>
<?php if($isTablet){ ?>
<link rel="stylesheet" type="text/css" href="/css/tablet.css">
<?php } ?>
<?php if($isMobile && !$isTablet){ ?>
<link rel="stylesheet" type="text/css" href="/css/mobile.css">
<?php } ?>
<head>
<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W972SMX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="wrapper">
		
		<div class="fyc-banner clear"><img src="/img/ui/disney-logo.svg" alt="Disney Logo"/> <span class="nowrap"><t>F</t><t>O</t><t>R</t><t> </t><t>Y</t><t>O</t><t>U</t><t>R</t><t> </t><t>C</t><t>O</t><t>N</t><t>S</t><t>I</t><t>D</t><t>E</t><t>R</t><t>A</t><t>T</t><t>I</t><t>O</t><t>N</t></t></span></div>
		<div class="title">AWARDS 2017</div>

		<div id="boxes" class="clear">
			<div class="box" id="batb-box">
				<div class="logo"><img src="/img/batb/batb-tt.png" alt="Beauty and the Beast Logo"/></div>
				<video preload playsinline muted poster="/img/batb/batb-poster.jpg">
					<source type="video/mp4" src="/media/video/batb_1_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<div class="box" id="sw-box">
				<div class="logo"><img src="/img/sw/sw-tt.png" alt="Star Wars Logo"/></div>
				<video poster="/img/sw/sw_poster.jpg" preload playsinline muted>
					<source type="video/mp4" src="/media/video/sw_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<div class="box" id="cars-box">
				<div class="logo"><img src="/img/cars/cars-tt.png" alt="Cars 3 Logo"/></div>
				<video poster="/img/cars/cars-poster.jpg" preload playsinline muted >
					<source type="video/mp4" src="/media/video/cars_1_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<div class="box" id="coco-box">
				<div class="logo"><img src="/img/coco/coco-tt.png" alt="Coco Logo"/></div>
				<video poster="/img/coco/coco-poster.jpg" preload playsinline muted >
					<source type="video/mp4" src="/media/video/coco_1_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<div class="box" id="lou-box">
				<div class="logo"><img src="/img/lou/lou-tt.png" alt="Lou Logo"/></div>
				<video preload playsinline muted poster="/img/lou/lou-poster.jpg" >
					<source type="video/mp4" src="/media/video/lou_1_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<div class="box" id="gotg-box">
				<video poster="/img/gotg/gotg-poster.jpg" preload playsinline muted >
					<source type="video/mp4" src="/media/video/gotg2_1_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<div class="box" id="thor-box">
				<div class="logo"><img src="/img/thor/thor-tt.png" alt="Thor Logo"/></div>
				<video poster="/img/thor/thor-poster.jpg" preload playsinline muted>
					<source type="video/mp4" src="/media/video/thor_1_1.mp4">
				</video>
				<div class="light"><img src="/img/ui/light.png"></div>
			</div>
			<footer>
				<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
				<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
				<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
				<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
				<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
				<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
				<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
			</footer>
		</div>
		<!-- <div class="footer" style="width: 100%;position: absolute;bottom: 5vh;text-align: center;font-weight: 300;font-size: 15px;line-height: 1.3em;">
			Walt Disney Studios Motion Pictures Awards Office  •  500 South Buena Vista Street Burbank, CA 91521-0222<br>
			PHONE: 818.560.6212 or 855.560.6212  •  EMAIL:<a style="color:#fff;text-decoration:none;" href="mailto:DISNEYAWARDSOFFICE@DISNEY.COM">DISNEYAWARDSOFFICE@DISNEY.COM</a>
		</div> -->
		<?php if($isMobile && !$isTablet) { ?>
			<footer>
				<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
				<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
				<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
				<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
				<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
				<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
				<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
			</footer>
		<?php } ?>
	</div>
	<header>
		<div class="fyc-banner clear"><a href="/"><img src="/img/ui/disney-logo.svg" alt="Disney Logo"/></a> <span class="nowrap"><t>F</t><t>O</t><t>R</t><t> </t><t>Y</t><t>O</t><t>U</t><t>R</t><t> </t><t>C</t><t>O</t><t>N</t><t>S</t><t>I</t><t>D</t><t>E</t><t>R</t><t>A</t><t>T</t><t>I</t><t>O</t><t>N</t></t></span><div class="title">AWARDS 2017</div></div>
		<?php if($isMobile && !$isTablet){ echo '<div id="menu-btn"><div class="top-line line"></div><div class="middle-line line"></div><div class=" bottom-line line"></div></div>'; } ?>
		<nav id="menu">
			<ul>
				<li class="batb-btn">Beauty and the Beast
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SCREENINGS & SYNOPSIS</div>
							<div>CAST</div>
							<div>FILMMAKERS</div>
							<div>MUSIC</div>
							<div>PRESS</div>
							<div>MAKING OF</div>
							<div>SCREENPLAY</div>
							<div>ACCOLADES</div>
						</div>
					</div>
				</li>
				<li class="cars-btn">Cars 3
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SCREENINGS</div>
							<div>SYNOPSIS</div>
							<div>SCORE</div>
							<div>SONGS</div>
							<div>PRESS</div>
							<div>PHOTOS</div>
							<div>ACCOLADES</div>
						</div>
					</div>
				</li>
				<li class="coco-btn">Coco
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SCREENINGS</div>
							<div>SYNOPSIS</div>
							<div>SONG</div>
							<div>SCORE</div>
							<div>SCREENPLAY</div>
							<div>ACCOLADES</div>
						</div>
					</div>
				</li>
				<li class="lou-btn">Lou
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SYNOPSIS</div>
							<div>FILMMAKERS</div>
							<div>PRESS</div>
							<div>PHOTOS</div>
						</div>
					</div>
				</li>
				<li class="gotg-btn">Guardians of the Galaxy Vol. 2
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SCREENINGS</div>
							<div>SYNOPSIS</div>
							<div>PRESS</div>
							<div>PHOTOS</div>
							<div>VIDEOS</div>
							<div>SCREENPLAY</div>
							<div>SONG</div>
							<div>ACCOLADES</div>
						</div>
					</div>
				</li>
				<li class="thor-btn">Thor: Ragnarok
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SCREENINGS</div>
							<div>SYNOPSIS</div>
							<div>PRESS</div>
							<div>PHOTOS</div>
							<div>SCORE</div>
							<div>SCREENPLAY</div>
						</div>
					</div>
				</li>
				<li class="sw-btn">SW: The Last Jedi
					<div class="submenu-holder">
						<div class="submenu">
							<div>CONSIDER</div>
							<div>SCREENINGS</div>
							<div>SYNOPSIS</div>
							<div>SCORE</div>
							<div>PHOTOS</div>
							<div>PRODUCTION</div>
							<div>ACCOLADES</div>
						</div>
					</div>
				</li>
			</ul>
		</nav>
	</header>
	<div id="page-holder">
		<div class="page" id="thor-page">
			<?php// include 'php/thor-page.php'; ?>
		</div>
		<div class="page" id="gotg-page">
			<?php //include 'php/gotg-page.php'; ?>
		</div>
		<div class="page" id="coco-page">
			<?php//include 'php/coco-page.php'; ?>
		</div>
		<div class="page" id="batb-page">
			<?php// include 'php/batb-page.php'; ?>
		</div>
		<div class="page" id="cars-page">
			<?php //include 'php/cars-page.php'; ?>
		</div>
		<div class="page" id="lou-page">
			<?php //include 'php/lou-page.php'; ?>
		</div>
		<div class="page" id="sw-page">
			<?php //include 'php/sw-page.php'; ?>
		</div>
	</div>
	<div id="batb-overlay">
		<div class="close-btn"></div>
		<div class="overlay-content">
			<?php //include 'php/overlay-content.php' ?>
		</div>
	</div>

	<div id="preload">
		<?php if($isMobile && $isTablet) {?>
			<img src="/img/thor/thor-mobile-bg.jpg"/>
			<img src="/img/coco/coco-mobile-bg.jpg"/>
			<img src="/img/lou/lou-mobile-bg.jpg"/>
			<img src="/img/cars/cars-mobile-bg.jpg"/>
			<img src="/img/batb/batb-mobile-bg.jpg"/>
			<img src="/img/gotg/gotg-mobile-bg.jpg"/>
		<?php } else { ?>
			<img src="/img/thor/thor-fyc-bg.jpg"/>
			<img src="/img/coco/coco-fyc-bg.jpg"/>
			<img src="/img/lou/lou-bg.jpg"/>
			<img src="/img/cars/cars-fyc-bg.jpg"/>
			<img src="/img/batb/batb-fyc-bg.jpg"/>
			<img src="/img/gotg/gotg-fyc-bg.jpg"/>
			<img src="/img/sw/sw-consider-page.jpg"/>
		<?php } ?>
	</div>
	<div id="rsvp-form">
		<div class="rsvp-close-btn">CLOSE<div class="x">+</div></div>
		<form>
			<div class="clear">
				<h2>RSVP</h2>
				<div class="screening-info">
					<div class="movie">CARS 3</div>
					<div>
						<div class="date">Nov 23</div> 
						<div class="theater"> ArcLight Hollywood </div>
					</div>
				</div>
			</div>
			<div class="london">
				Please RSVP to the following email address: <a href="mailto:disney@premiercomms.com">disney@premiercomms.com</a><br/><br/>
				Or call <a href="tel:44 20 7292 7394">+44 20 7292 7394</a>
			</div>
			<div class="not-london">
				<input name="screening_id" id="screening-id" value="" type="hidden" maxlength="15">
				<input name="referrer" value="guild" type="hidden" maxlength="15">	
				
				<div class="input pushRight">
					<input id="first_name" name="first_name" placeholder="First Name" maxlength="15">
					<div class="error"></div>
				</div>
				<div class="input">
					<input id="last_name" name="last_name" placeholder="Last Name" maxlength="15">
					<div class="error"></div>
				</div>
				<br/>
				<div class="input fullWidth">
					<input id="email" name="email" placeholder="Email Address" maxlength="50">
					<div class="error"></div>
				</div>
				<div class="input pushRight">
					<div class="hasDropdown guests"><span>GUESTS</span>
						<div class="dropdown">
						</div>
					</div>
					<input type="hidden" id="guests" name="guests" value=""/>
				</div>
				
				<div class="input">
					<div class="guilds hasDropdown"><span>GUILD</span>
						<div class="dropdown">
							
						</div>
					</div>
					
					<div class="error"></div>
					<input type="hidden" id="guild" name="guild"/>
				</div>
				<div class="submit-btn" id="rsvp-submit">CONFIRM RSVP</div>
			</div>
		</form>
		<div id="rsvp-message">

		</div>
	</div>
	<div class="rotate"></div>
	
		

	<?PHP if($_SERVER['HTTP_HOST'] == 'www.waltdisneystudiosawards.com' || $_SERVER['HTTP_HOST']=='waltdisneystudiosawards.com') { ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108370396-1"></script>
	<script>
		if(location.host == 'www.waltdisneystudiosawards.com'|| location.host=='waltdisneystudiosawards.com'){
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-108370396-1');
		}
	</script>
	<?php } ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script>
	(function(){
		if (window.addtocalendar)
			if(typeof window.addtocalendar.start == "function") return;
			if (window.ifaddtocalendar == undefined) { 
   			window.ifaddtocalendar = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
            var h = d[g]('body')[0];h.appendChild(s); 
        }
	})();
	</script>
	<script>
		<?php 
		$isMobile = ($detect->isMobile() == 1 ? 'true':'false');
		$isTablet = ($detect->isTablet()==1 ? 'true':'false');
			echo 'var isMobile = '.$isMobile .';';
			echo 'var isTablet ='. $isTablet.';'; 
		?>
	</script>
	<script src="/js/scripts.js"></script>
	<script>
// Let's wrap everything inside a function so variables are not defined as globals 
(function() {
    // This is gonna our percent buckets ( 10%-90% ) 
    var divisor = 10;
    // We're going to save our players status on this object. 
    var videos_status = {};
    // This is the funcion that is gonna handle the event sent by the player listeners 
    function eventHandler(e) {
        switch (e.type) {
            // This event type is sent everytime the player updated it's current time, 
            // We're using for the % of the video played. 
        case 'timeupdate':
            // Let's set the save the current player's video time in our status object 
            videos_status[e.target.id].current = Math.round(e.target.currentTime);
            // We just want to send the percent events once 
            var pct = Math.floor(100 * videos_status[e.target.id].current / e.target.duration);
            for (var j in videos_status[e.target.id]._progress_markers) {
                if (pct >= j && j > videos_status[e.target.id].greatest_marker) {
                    videos_status[e.target.id].greatest_marker = j;
                }
            }
            // current bucket hasn't been already sent to GA?, let's push it to GTM
            if (videos_status[e.target.id].greatest_marker && !videos_status[e.target.id]._progress_markers[videos_status[e.target.id].greatest_marker]) {
                videos_status[e.target.id]._progress_markers[videos_status[e.target.id].greatest_marker] = true;
                dataLayer.push({
                    'event': 'gaEvent',
                    'gaEventCategory': filmInfo[loaded].title,
                    'gaEventAction': 'Video Progress %' + videos_status[e.target.id].greatest_marker,
                    // We are using sanitizing the current video src string, and getting just the video name part
                    'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1])
                });
            }
            break;
            // This event is fired when user's click on the play button
        case 'play':
            dataLayer.push({
                'event': 'gaEvent',
                'gaEventCategory': filmInfo[loaded].title,
                'gaEventAction': 'Video Played',
                'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1])
            });
            break;
            // This event is fied when user's click on the pause button
        case 'pause':
            dataLayer.push({
                'event': 'gaEvent',
                'gaEventCategory': filmInfo[loaded].title,
                'gaEventAction': 'Video Paused',
                'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1]),
                'gaEventValue': videos_status[e.target.id].current
            });
            break;
            // If the user ends playing the video, an Finish video will be pushed ( This equals to % played = 100 )  
        case 'ended':
            dataLayer.push({
                'event': 'gaEvent',
                'gaEventCategory': filmInfo[loaded].title,
                'gaEventAction': 'Video Finished',
                'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1])
            });
            break;
        default:
            break;
        }
    }
    // We need to configure the listeners
    // Let's grab all the the "video" objects on the current page     
    var videos = document.getElementsByTagName('video');
    for (var i = 0; i < videos.length; i++) {
        // In order to have some id to reference in our status object, we are adding an id to the video objects
        // that don't have an id attribute. 
        var videoTagId;
        if (!videos[i].getAttribute('id')) {
            // Generate a random alphanumeric string to use is as the id
            videoTagId = 'html5_video_' + Math.random().toString(36).slice(2);
            videos[i].setAttribute('id', videoTagId);
        }// Current video has alredy a id attribute, then let's use it <img draggable="false" class="emoji" alt="?" src="https://s.w.org/images/core/emoji/2/svg/1f642.svg">
        else {
            videoTagId = videos[i].getAttribute('id');
        }
        // Video Status Object declaration  
        videos_status[videoTagId] = {};
        // We'll save the highest percent mark played by the user in the current video.
        videos_status[videoTagId].greatest_marker = 0;
        // Let's set the progress markers, so we can know afterwards which ones have been already sent.
        videos_status[videoTagId]._progress_markers = {};
        for (j = 0; j < 100; j++) {
            videos_status[videoTagId].progress_point = divisor * Math.floor(j / divisor);
            videos_status[videoTagId]._progress_markers[videos_status[videoTagId].progress_point] = false;
        }
        // On page DOM, all players currentTime is 0 
        videos_status[videoTagId].current = 0;
        // Now we're setting the event listeners. 
        videos[i].addEventListener("play", eventHandler, false);
        videos[i].addEventListener("pause", eventHandler, false);
        videos[i].addEventListener("ended", eventHandler, false);
        videos[i].addEventListener("timeupdate", eventHandler, false);
        videos[i].addEventListener("ended", eventHandler, false);
    }
})();
</script>

</body>
</html>