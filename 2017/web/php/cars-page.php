<div class="bg-holder">
	<div class="bg fyc"></div>
	<div class="bg screenings"></div>
	<div class="bg synopsis"></div>
	<div class="bg score"></div>
	<div class="bg songs"></div>
	<div class="bg press"></div>
	<div class="bg photos"></div>
</div> 
<div class="limiter">
	<div class="consider clear">
		<div class="content">
			<div class="logo">
				<img src="/img/cars/cars-tt.png" alt="Cars 3">
			</div>
			<div class="first">
				<p>FOR YOUR CONSIDERATION IN ALL CATEGORIES</p>
				<h3>BEST ANIMATED FEATURE</h3>
			<!-- 	<div class="subline">PRODUCED BY</div> -->
				<div class="name">BRIAN FEE</div>
				<div class="name">KEVIN REHER, <span class="guild">p.g.a.</span></div>
			</div>
			<div class="left">
				<div>
					<h3>BEST DIRECTOR</h3>
					<div class="name">BRIAN FEE</div>
				</div>
				<div>
					<h3>BEST ADAPTED SCREENPLAY</h3>
					<div class="name">KIEL MURRAY<br/>BOB PETERSON<br/>MIKE RICH</div>
					<div class="subline">ORIGINAL STORY BY</div>
					<div class="name">
						BRIAN FEE<br/>
						BEN QUEEN<br/>
						EYAL PODELL &
						<br/>
						JONATHON E. STEWART
					</div>
				</div>
				<div>	
					<h3>BEST CINEMATOGRAPHY</h3>
					<div class="subline">DIRECTOR OF PHOTOGRAPHY: LIGHTING</div>
					<div class="name">KIM WHITE</div>
					<div class="subline">DIRECTOR OF PHOTOGRAPHY: CAMERA</div>
					<div class="name">JEREMY LASKY</div>
				</div>
				<div>	
					<h3>BEST FILM EDITING</h3>
					<div class="name">JASON HUDAK</div>
				</div>
				<div>	
					<h3>BEST ART DIRECTION</h3>
					<div class="subline">PRODUCTION DESIGNERS</div>
					<div class="name">WILLIAM CONE<br/>JAY SHUSTER</div>
				</div>		
			</div>
			<div class="right">
				<div>
					<h3>BEST SOUND MIXING</h3>
					<div class="subline">RE-RECORDING MIXERS</div>
					<div class="name">MICHAEL SEMANICK</div>
					<div class="name">NATHAN NANCE<br/>TOM MYERS</div>
					<div class="subline">ORIGINAL DIALOGUE MIXER</div>
					<div class="name">VINCE CARO</div>
				</div>
				<div>
					<h3>BEST SOUND EDITING</h3>
					<div class="subline">SOUND DESIGNER/SUPERVISING SOUND EDITOR</div>
					<div class="name">TOM MYERS</div>
					<div class="subline">SUPERVISING SOUND EDITOR</div>
					<div class="name">BRIAN CHUMNEY</div>
				</div>
				<div>	
					<h3>BEST VISUAL EFFECTS</h3>
					<div class="name">JON REISCH</div>
				</div>
				<div>	
					<h3>BEST ORIGINAL SCORE</h3>
					<div class="name">RANDY NEWMAN</div>
				</div>
				<div>	
					<h3>BEST ORIGINAL SONG</h3>
					<div class="name">"RUN THAT RACE"</div>
					<div class="subline">WRITTEN BY</div>
					<div class='name'>DAN AUERBACH</div>
					<div class="down5 name">"RIDE"</div>
					<div class="subline">WRITTEN BY</div>
					<div class="name">ZZ WARD<br/>EVAN BOGART &<br/>DAVE BASSETT</div>
				</div>
			</div>
		</div>
    </div>
	<div class="screenings">
		<div class="content">
			<div class="cities-list">
				<div class="city selected">Los Angeles</div>
				<div class="city">New York</div>
				<div class="city">San Francisco</div>
				<div class="city">London</div>
				
			</div>
			<div class="city-holder London-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder New_York-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder Los_Angeles-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder San_Francisco-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="disclaimer">You must be an invited member of a voting organization to attend <span class="nowrap">For Your Consideration screenings.</span> Your membership card is required for entry.</div>
		</div>
	</div>
	<div class="synopsis">
		<div class="content">
			<p>Blindsided by a new, faster generation of competitors, the legendary Lightning McQueen (Owen Wilson) is suddenly faced with irrelevance. To kick-start a new beginning, his sponsor pairs him with an eager young trainer, Cruz Ramirez (Cristela Alonzo) and their uneasy collaboration leads to unexpected adventures, impressive results, and the biggest showdown of McQueen’s career. Directed by Brian Fee (storyboard artist “Cars,” “Cars 2”), produced by Kevin Reher (“A Bug’s Life,” “La Luna” short) and co-produced by Andrea Warren (“LAVA” short).</p>
		</div>
	</div>
	
	<div class="score">
		<div class="content">
			<h2>BEST ORIGINAL SCORE</h2>
			<div class="score-name">
				BY RANDY NEWMAN
			</div>
			<div class="score-bio">
				<p>With songs that run the gamut from heartbreaking to satirical and a host of unforgettable film scores, RANDY NEWMAN (Original Score Composed & Conducted by) has used his many talents to create musical masterpieces widely recognized by generations of audiences.</p>
				<div class="read-more openOverlay" id="newman-bio">READ MORE</div>
			</div>
			<div class="track-list">
				<div class="scrollable">
				</div>
			</div>
		</div>
	</div>
	<div class="songs clear">
		<div class="content">
			<h2>BEST ORIGINAL SONG</h2>
			<div class="song">
				<audio>
					<source src="/media/audio/cars/01_Run_That_Race.mp3">
				</audio>
				<div data-song="Run That Race" class="play-btn"></div>
				<div class="song-title">“RUN THAT RACE”</div>
				<div class="written-by">WRITTEN BY <div>DAN AUERBACH</div></div>
				<div class="artist dan">
					<img src="/img/cars/dan_auerbach.jpg"/>
				</div>
			</div>
		
	
			<div class="song">
				<audio>
					<source src="/media/audio/cars/06_Ride.mp3">
				</audio>
				<div data-song="Ride" class="play-btn"></div>
				<div class="song-title">“RIDE”</div>
				<div class="written-by">WRITTEN BY <div>ZZ WARD, EVAN BOGART & DAVE BASSETT</div></div>
				<div class="artist zz">
					<img src="/img/cars/zz_ward.jpg"/>
				</div>
				<div class="artist evan">
					<img src="/img/cars/evan_bogart.jpg"/>
				</div>
				<div class="artist dave">
					<img src="/img/cars/dave_bassett.jpg"/>
				</div>
			</div>
		</div>
	</div>
	<div class="press">
		<div class="content">
			<div class="scroll-down"></div>
			<div class="scrollable">
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/forbes.png"/>
					</div>
					<div class="title">
						“Compelling, relevant, surprising and visually stunning.”
					</div>
					
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/village-voice.png"/>
					</div>
					<div class="title">
					“Electric. Smartly engineered.”
					</div>
					
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/indiewire.png"/>
					</div>
					<div class="title">
						“A Progressive Vision.”
					</div>
					
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/variety.png"/>
					</div>
					<div class="title">
						“Sweet and polished.”
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="photos">
		<div class="slider"></div>
		<div class="left-arrow arrow"></div>
		<div class="right-arrow arrow"></div>
		<div class="dots">
			
		</div>
	</div>
	<div class="accolades">
		<div class="banner"><img src="/img/cars/cars-accolade-banner.jpg"></div>
		<div class="content">
			<div class="award large" style="margin-right:2%;">
				<img src="/img/cars/awards/ANNIE-AWARDS.jpg">
			</div>
			<div class="award" style="width:41%;margin-top:3vh;">
				<img src="/img/cars/awards/VEW.jpg"/>
			</div>
			<br/>
			<div class="award">
				<img src="/img/cars/awards/AD.jpg">
			</div>
			<div class="award middle">
				<img src="/img/cars/awards/MPSE.jpg">
			</div>
			<div class="award">
				<img src="/img/cars/awards/CAS.jpg">
			</div>
			<br/>
			<div class="award">
				<img src="/img/cars/awards/NAACP.jpg"/>
			</div>
		</div>
	</div>
	<?php //if($isMobile) { ?>
		<footer>
			<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
			<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
			<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
			<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
			<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
			<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
			<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
		</footer>
		<?php //} ?>
</div>	