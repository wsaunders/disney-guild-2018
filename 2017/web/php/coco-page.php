<div class="bg-holder">
	<div class="bg fyc"></div>
	<div class="bg screenings"></div>
	<div class="bg synopsis"></div>
	<div class="bg song"></div>
	<div class="bg score"></div>
<!-- 	<div class="bg photos"></div>
	<div class="bg videos"></div>-->
	<div class="bg screenplay"></div> 
	<div class="bg accolades"></div>
</div> 
<div class="limiter">
	<div class="consider">
		<div class="award">
			<img src="/img/coco/consider-academy-winner.png"/>
		</div>
		<div class="content">
			<div class="logo">
				<img src="/img/coco/coco-tt.png" alt="Coco"/>
			</div>

			<div class="first">
				<p>FOR YOUR CONSIDERATION IN ALL CATEGORIES</p>
				<h3>BEST PICTURE</h3>
				<div class="subline">PRODUCED BY</div>
				<div class="name">DARLA K. ANDERSON, <span class="guild">p.g.a.</span></div>
				<h3>BEST ANIMATED FEATURE</h3>
				<div class="name">LEE UNKRICH | 
				DARLA K. ANDERSON, <span class="guild">p.g.a.</span></div>
			</div>
			<div class="left">
				<h3>BEST DIRECTOR</h3>
				<div class="name">LEE UNKRICH</div>
				<h3>BEST ORIGINAL SCREENPLAY</h3>
				<div class="name">ADRIAN MOLINA<br/>MATTHEW ALDRICH</div>
				<div class="subline">STORY BY</div>
				<div class="name">LEE UNKRICH<br/>JASON KATZ<br/>MATTHEW ALDRICH<br/>ADRIAN MOLINA</div>
				<h3>BEST CINEMATOGRAPHY</h3>
				<div class="subline">DIRECTOR OF CINEMATOGRAPHY: CAMERA</div>
				<div class="name">MATT ASPBURY</div>
				<div class="subline">DIRECTOR OF CINEMATOGRAPHY: LIGHTING</div>
				<div class="name">DANIELLE FEINBERG</div>
				<h3>BEST FILM EDITING</h3>
				<div class="name">STEVE BLOOM<br/>LEE UNKRICH, <span class="guild">ACE</span></div>
				<h3>BEST PRODUCTION DESIGN</h3>
				<div class="name">HARLEY JESSUP</div>
			</div>
			<div class="right">
				<h3>BEST SOUND MIXING</h3>
				<div class="subline">RE-RECORDING MIXERS</div>
				<div class="name">MICHAEL SEMANICK<br/>CHRISTOPHER BOYES</div>
				<div class="subline">ORIGINAL DIALOGUE MIXER</div>
				<div class="name">VINCE CARO</div>
				<h3>BEST SOUND EDITING</h3>
				<div class="subline">SOUND DESIGNER / SUPERVISING SOUND EDITORS</div>
				<div class="name">CHRISTOPHER BOYES<br/>JR GRUBBS</div>
				<h3>BEST VISUAL EFFECTS</h3>
				<div class="name">
					MICHAEL K. O'BRIEN<br/>
				</div>
				<h3>BEST ORIGINAL SCORE</h3>
				<div class="name">MICHAEL GIACCHINO</div>
				<h3>BEST ORIGINAL SONG</h3>
				<div class="song">"REMEMBER ME"</div>
				<div class="subline">MUSIC & LYRICS BY</div>
				<div class='name'>KRISTEN ANDERSON-LOPEZ<div class="and">AND</div>ROBERT LOPEZ</div>
			</div>
		</div>
	</div>
	<div class="screenings">
		<div class="guild-members-co">
			ATTENTION GUILD MEMBERS<br/>
			CLICK HERE
			<div>
			YOUR MEMBERSHIP CARD<br/> WILL ADMIT YOU TO<br/> THE FOLLOWING THEATERS
			</div>
		</div>
		<div class="pop-up">
			<div class="pop-up-close"></div>
			<div class="inside">
				<h3>GUILD MEMBERS</h3>
				<p>You may use your membership card to admit you and a guest to the following theatres in your city, subject to seating capacity/availability.
				<br/><br/>
				Cinemark does not allow a guest.</p>
				<ul>
					<li><span></span>ArcLight/Pacific will admit AMPAS, WGA, PGA, DGA (Monday–Thursday only/no holidays).
					</li>
					<li><span></span>AMC will admit AMPAS, BAFTA, ACE, ADG, ASC, CAS, DGA, HFPA, MPEG, MPSE, PGA, WGA, CDG, VES (Monday–Thursday only/no holidays- Los Angeles and NY Only).</li>

					<li><span></span>Regal will admit AMPAS, WGA, DGA (Monday–Thursday only/no holidays).</li>

					<li><span></span>Laemmle will admit AMPAS, DGA, WGA (Monday–Thursday only/no holidays).</li>

					<li><span></span>Cinemark will admit AMPAS, WGA, DGA, PGA (Members only/no guest, Monday–Thursday).</li>
				</ul>
			</div>
		</div>
		<div class="content">
	
			<div class="cities-list">
				<div class="city selected">Los Angeles</div>
				<div class="city">New York</div>
				<div class="city">San Francisco</div>
				<div class="city">London</div>
				
			</div>
			<div class="city-holder London-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder New_York-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder Los_Angeles-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder San_Francisco-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="disclaimer">
				You must be an invited member of a voting organization to attend <span class="nowrap">For Your Consideration screenings</span>. Your membership card is required for entry.
			</div>
			
		</div>
	</div>
	<div class="synopsis">
		<div class="content">

			<p>Despite his family’s frustrating, generations-old ban on music, young Miguel (Anthony Gonzalez) is inspired by his idol, the late, great musician  Ernesto de la Cruz (Benjamin Bratt)—and dreams of nothing else. Following a mysterious and otherworldly chain of events, Miguel meets charming trickster Hector (Gael García Bernal), and, together, they set off on an adventure of music and mystery, resulting in the most unusual family reunion.</p>
			<p>Directed by Lee Unkrich (“Toy Story 3”), co-directed by Adrian Molina (story artist “Monsters University”) and produced by Darla K. Anderson (“Toy Story 3”), Disney•Pixar’s “Coco” opens in U.S. theaters on <span class="nowrap">Nov. 22, 2017.</span></p>
		</div>
	</div>
	<div class="song">
		
		<div class="content">
			<div class="song">
				<!-- <h3>BEST ORIGINAL SONG</h3> -->
				<!-- <div class="quote">
					<p>“A BIG PLUS IS THE SONGS, PARTICULARLY<br/>
					‘REMEMBER ME’ FROM <i>FROZEN</i>'S OSCAR&reg;<br/>
					WINNERS ROBERT LOPEZ AND<br/>
					KRISTEN ANDERSON-LOPEZ.”</p>
					<div class="author" style="">- PETE HAMMOND, 
						<img class="auth-logo" style="width:80px;" src="/img/press/quote-deadline.png">
					</div>
				</div> -->
				<div class="award">
					<img src="/img/coco/coco-song-award.png"/>
				</div>
				<div class="artist">
					<img src="/img/coco/lopez.jpg">
				
				</div>
				<div class="song-title">
					<span>“REMEMBER ME”
						<audio><source src="/media/audio/coco/remember_me.mp3" type="audio/mp3"></audio>
						<div data-song="remember_me" class="play-btn"></div>
					</span>
				</div>
				<div class="credits">
					<div>MUSIC & LYRICS BY</div>
					KRISTEN ANDERSON-LOPEZ <span class="and">AND</span> ROBERT LOPEZ
				</div>
				
			</div>
		</div>
	</div>
	<div class="score">
		<div class="content">
			<h2>BEST ORIGINAL SCORE</h2>
			<div class="score-name">
				BY MICHAEL GIACCHINO
			</div>
			<div class="artist-pic"><img src="/img/coco/giacchino.jpg"/></div>
			
			<div class="track-list">
				<div class="scrollable">
				</div>
			</div>
			<div class="quote">
				<p>“Michael Giacchino scores the picture with his usual grace and sweep.”</p>
				<div class="author" style="">- MICHAEL PHILLIPS, 
					<img class="auth-logo" src="/img/press/quote-chicagotrib.png">
				</div>
			</div>
		</div>
	</div>
	<div class="photos">
		<div class="slider">
			
		</div>
		<div class="left-arrow arrow"></div>
		<div class="right-arrow arrow"></div>
		<div class="dots">
			
		</div>
	</div>
	<div class="videos">
		<div class="content">
			<div class="video-list">
				<div class="video-title selected">TRAILER #1</div>
			</div>
			<div class="video-container">
				<video poster="/img/coco/coco-video-preview.jpg" controls>
					<source src="/media/video/coco-trailer.mp4" type="video/mp4"/>
				</video>
				<div class="video-play-btn"></div>
			</div>
		</div>
	</div>
	<div class="screenplay">
		<div class='content'>
			<div class="download-button"><a download="COCO_Script.pdf" href="/media/scripts/COCO_Script.pdf"><img src="/img/coco/coco-download-btn.png"></a></div>
			<div class="text">
				<h2>BEST ORIGINAL SCREENPLAY</h2>
				<div class="written">WRITTEN BY</div>
				<div class="names"><div>ADRIAN MOLINA</div><div>MATTHEW ALDRICH</div></div>
				<div class="written">STORY BY</div>
				<div class="names"><div>LEE UNKRICH</div><div> JASON KATZ</div><br/><div>MATTHEW ALDRICH</div> <div>ADRIAN MOLINA</div></div>
			</div>
		</div>
	</div>
	<div class="accolades">
		<div class="banner"><img src="/img/coco/coco-accolades-banner.jpg"></div>
		<div class="content">
			<div class="special clear">
				<div class="award academy">
					<img src="/img/coco/awards/ACADEMY-winner.jpg">
				</div>
			<br/>
				<div class="award left annie ">
					<img src="/img/coco/awards/annie-winners.jpg">
				</div>
	
				<div class="award golden-globes right">
					<img src="/img/coco/awards/gg2.jpg">
				</div>
			<br/>
				<div class="award critics-choice left">
					<img src="/img/coco/awards/cca.jpg">
				</div>
				<div class="award pga right">
					<img src="/img/coco/awards/PGA-winner.jpg">
				</div>
			<br/>
				<div class="award adg left">
					<img src="/img/coco/awards/ADG-winner.jpg">
				</div>
				<div class="award bafta right">
					<img src="/img/coco/awards/BAFTA-winner.jpg">
				</div>	
			<br/>
				<div class="award ace left">
					<img src="/img/coco/awards/ACE-winner.jpg">
				</div>
				<div class="award right ves ">
					<img src="/img/coco/awards/ves-winner.jpg"/>
				</div>
			<br/>
				<div class="award cas left">
					<img src="/img/coco/awards/cas-winner.jpg">
				</div>
				<div class="award right mpse ">
					<img src="/img/coco/awards/mpse-winner.jpg"/>
				</div>
			<br/>
		</div>
		  <div class="press">
			<div class="award ny-critics">
				
				<img src="/img/coco/awards/NY-FILM-CRITICS-CIRCLE.jpg">
			</div>
			<div class="award nbr middle">
				
				<img src="/img/coco/awards/NATIONAL-BOARD.jpg">
			</div>
			<div class="award sf-critics">
			
				<img src="/img/coco/awards/SF.jpg">
			</div>
			
			<div class="award dc-critics">
			
				<img src="/img/coco/awards/DC-FILMS.jpg">
			</div>
			<div class="award philly middle">
				<img src="/img/coco/awards/PHILLY.jpg">
			</div>
			<div class="award boston-critics">
		
				<img src="/img/coco/awards/BOSTON-SOCIETY.jpg">
			</div>
			
			<div class="award ny-online">
				<img src="/img/coco/awards/NY-FILM-CRITICS.jpg">
			</div>
			<div class="award boston-online middle">
				<img src="/img/coco/awards/BOSTON-ONLINE.jpg">
			</div>
			<div class="award chicago">
				<img src="/img/coco/awards/chicago.jpg">
			</div>
			<div class="award dallas">
				<img src="/img/coco/awards/DALLAS.jpg">
			</div>
			<div class="award stlouis middle">
				<img src="/img/coco/awards/ST-LOUIS.jpg">
			</div>
			<div class="award kansas">
				<img src="/img/coco/awards/Kansas.jpg">
			</div>
			<div class="award phoenix ">
				<img src="/img/coco/awards/phoenix.jpg">
			</div>
			<div class="award indian middle">
				<img src="/img/coco/awards/Indiana.jpg">
			</div>
			<div class="award florida">
				<img src="/img/coco/awards/FLORIDA.jpg">
			</div>
			<div class="award north-texas">
				<img src="/img/coco/awards/NORTH-TEXAS.jpg">
			</div>
			<div class="award nevada middle">
				<img src="/img/coco/awards/NEVADA.jpg">
			</div>
			<div class="award online">
				<img src="/img/coco/awards/ONLINE.jpg">
			</div>
			<div class="award chic-ind">
				<img src="/img/coco/awards/chicago-independent.jpg">
			</div>
			<div class="award san-fran middle">
				<img src="/img/coco/awards/san-fran.jpg">
			</div>
			<div class="award">
				<img src="/img/coco/awards/COLUMBUS.jpg"/>
			</div>
		
			<div class="award">
				<img src="/img/coco/awards/HOUSTON.jpg">
			</div>
			<div class="award middle">
				<img src="/img/coco/awards/LAS-VEGAS.jpg">
			</div>
			<div class="award">
				<img src="/img/coco/awards/LA-ONLINE.jpg"/>
			</div>
			
			<div class="award">
				<img src="/img/coco/awards/NORTH-CAROLINA.jpg">
			</div>
			<div class="award middle">
				<img src="/img/coco/awards/OKLAHOMA.jpg">
			</div>
			<div class="award">
				<img src="/img/coco/awards/AUSTIN.jpg"/>
			</div>
		
			<div class="award">
				<img src="/img/coco/awards/GEORGIA.jpg">
			</div>
			<div class="award middle">
				<img src="/img/coco/awards/ATLANTA.jpg">
			</div>
			<div class="award">
				<img src="/img/coco/awards/WOMEN.jpg"/>
			</div>
		
			<div class="award">
				<img src="/img/coco/awards/PHOENIX-SOCIETY.jpg">
			</div>
			<div class="award middle">
				<img src="/img/coco/awards/DENVER.jpg">
			</div>
			<div class="award">
				<img src="/img/coco/awards/IOWA.jpg"/>
			</div>
		
		</div>
	  </div>
	</div>
	<?php //if($isMobile) { ?>
		<footer>
			<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
			<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
			<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
			<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
			<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
			<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
			<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
		</footer>
		<?php// } ?>
</div>