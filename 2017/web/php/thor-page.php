<div class="bg-holder">
	<div class="bg fyc"></div>
	<div class="bg screenings"></div>
	<div class="bg synopsis"></div>
	<div class="bg press"></div>
	<div class="bg photos"></div>
	<div class="bg score"></div>
	<div class="bg screenplay"></div>
</div> 
<div class="limiter">
	<div class="consider">
		<div class="content">
			<div class="logo">
				<img src="/img/thor/thor-tt.png" alt="Coco"/>
			</div>
			<div class="first">
				<p>FOR YOUR CONSIDERATION IN ALL CATEGORIES</p>
				<h3>BEST PICTURE</h3>
				<div class="subline">PRODUCED BY</div>
				<div class="name">KEVIN FEIGE, <span class="guild">p.g.a.</span></div>
			</div>
			<div class="left">
				<h3>BEST DIRECTOR</h3>
				<div class="name">TAIKA WAITITI</div>
				<h3>BEST ADAPTED SCREENPLAY</h3>
				<div class="name">ERIC PEARSON AND<br/>CRAIG KYLE &<br/>CHRISTOPHER L. YOST</div>
				
				<h3>BEST ACTOR</h3>
				<div class="name">CHRIS HEMSWORTH</div>
				<h3>BEST SUPPORTING ACTOR</h3>
				<div class="name">
					TOM HIDDLESTON<br/>
					MARK RUFFALO<br/>
					JEFF GOLDBLUM
				</div>
				<h3>BEST SUPPORTING ACTRESS</h3>
				<div class="name">
					CATE BLANCHETT<br/>
					TESSA THOMPSON
				</div>
				<h3>BEST CINEMATOGRAPHY</h3>
				<div class="name">JAVIER AGUIRRESAROBE, <span class="guild">ASC</span></div>
				<h3>BEST FILM EDITING</h3>
				<div class="name">JOEL NEGRON, <span class="guild">ACE</span><br/>ZENE BAKER, <span class="guild">ACE</span></div>
				<h3>BEST PRODUCTION DESIGN</h3>
				<div class="subline">PRODUCTION DESIGNERS</div>
				<div class="name">DAN HENNAH<br/>RA VINCENT</div>
				<div class="subline">SET DECORATOR</div>
				<div class="name">BEV DUNN</div>
			
			</div>
			<div class="right">
				<div>
					<h3>BEST COSTUME DESIGN</h3>
					<div class="name">MAYES C. RUBEO</div>
				</div>
				<div>
					<h3>BEST MAKEUP AND HAIRSTYLING</h3>
					<div class="name">ENZO MASTRANTONIO<br/>LUCA VANNELLA</div>
				</div>
				<div>
					<h3>BEST SOUND MIXING</h3>
					<div class="subline">RE-RECORDING MIXERS</div>
					<div class="name">LORA HIRSCHBERG<br/>JUAN PERALTA</div>
					<div class="subline">SOUND MIXER</div>
					<div class="name">GUNTIS SICS</div>
				</div>
				<div>
					<h3>BEST SOUND EDITING</h3>
					<div class="subline">SUPERVISING SOUND EDITORS</div>
					<div class="name">SHANNON MILLS<br/>DANIEL LAURIE</div>
				</div>
				<div>
					<h3>BEST VISUAL EFFECTS</h3>
					<div class="name">
						JAKE MORRISON<br/>
						CHAD WIEBE<br/>
						KYLE McCULLOCH<br/>
						BRUCE STEINHEIMER
					</div>
				</div>
				<div>
					<h3>BEST ORIGINAL SCORE</h3>
					<div class="name">MARK MOTHERSBAUGH</div>
				</div>
			</div>
		</div>

	</div>
	<div class="screenings">
		<div class="guild-members-co">
			ATTENTION GUILD MEMBERS<br/>
			CLICK HERE
			<div>
			YOUR MEMBERSHIP CARD<br/> WILL ADMIT YOU TO<br/> THE FOLLOWING THEATERS
			</div>
		</div>
		<div class="pop-up">
			<div class="pop-up-close"></div>
			<div class="inside">
				<h3>GUILD MEMBERS</h3>
				<p>You may use your membership card to admit you and a guest to the following theatres in your city, subject to seating capacity/availability.
				<br/><br/>
				Cinemark does not allow a guest.</p>
				<ul>
					<li><span></span>ArcLight/Pacific will admit AMPAS, WGA, PGA, DGA (Monday–Thursday only/no holidays).
					</li>
					<li><span></span>AMC will admit AMPAS, BAFTA, ACE, ADG, ASC, CAS, DGA, HFPA, MPEG, MPSE, PGA, WGA, CDG, VES (Monday–Thursday only/no holidays- Los Angeles and NY Only).</li>

					<li><span></span>Regal will admit AMPAS, WGA, DGA (Monday–Thursday only/no holidays).</li>

					<li><span></span>Laemmle will admit AMPAS, DGA, WGA (Monday–Thursday only/no holidays).</li>

					<li><span></span>Cinemark will admit AMPAS, WGA, DGA, PGA (Members only/no guest, Monday–Thursday).</li>
				</ul>
			</div>
		</div>
		<div class="content">
			<div class="cities-list">
				<div class="city selected">Los Angeles</div>
				<div class="city">New York</div>
				<div class="city">San Francisco</div>
				<div class="city">London</div>
				
			</div>
			<div class="city-holder London-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder New_York-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder Los_Angeles-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder San_Francisco-holder">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="disclaimer">You must be an invited member of a voting organization to attend <span class="nowrap">For Your Consideration screenings</span>. Your membership card is required for entry.</div>
			
		</div>
	</div>
	<div class="synopsis">
		<div class="content">
			In Marvel Studios’ “Thor: Ragnarok,” Thor is imprisoned on the other side of the universe without his mighty hammer and finds himself in a race against time to get back to Asgard to stop Ragnarok—the destruction of his homeworld and the end of Asgardian civilization—at the hands of an all-powerful new threat, the ruthless Hela. But first he must survive a deadly gladiatorial contest that pits him against his former ally and fellow Avenger—the Incredible Hulk!
		</div>
	</div>
	<div class="press">
		<div class="scroll-down">SCROLL DOWN FOR MORE</div>
		<div class="content">
			<div class="scrollable">
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/screen-intl.jpg"/>
					</div>
					<div class="title">
						“Waititi exhibits a droll sense of humour without losing sight of character dynamics, tonal sophistication or thematic undercurrents.”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/wired.jpg"/>
					</div>
					<div class="title">
						“‘Thor: Ragnarok’ is such a wonderful surprise, and the most effortlessly enjoyable hangout movie to emerge from the Marvel Cinematic Universe.”
					</div>
				</div>
				
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/the-wrap.svg"/>
					</div>
					<div class="title">
						“Waititi and screenwriters Eric Pearson, Craig Kyle and Christopher L. Yost know how to balance stakes and silliness, which is exactly what this movie needs.”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="max-height:50px;" src="/img/press/polygon.svg"/>
					</div>
					<div class="title">
						“Ragnarok sets the new standard by which the entire MCU will have to adhere to...”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="width:163px" class="verge" src="/img/press/theverge.svg"/>
					</div>
					<div class="title">
						“Waititi is the New Zealand-based filmmaker and comedian behind movies like ‘Hunt for the Wilderpeople’ and the vampire mockumentary ‘What We Do in the Shadows.’ ‘Thor: Ragnarok’ is his first Hollywood feature, but what he’s done with Ragnarok doesn’t just boil down to adding new characters or throwing in extra comedy. Instead, it’s an enthusiastic, hilarious reboot of the idea of what a Marvel movie can actually be, resulting in an effervescent, delightfully self-aware ride that was the most fun I’d had in a superhero movie in years.”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img src="/img/press/ign.svg"/>
					</div>
					<div class="title">
						“Composed by Devo’s Mark Mothersbaugh, providing the Marvel Cinematic Universe’s most distinctive original music yet.”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img class="parade" src="/img/press/parade.svg"/>
					</div>
					<div class="title">
						“The seriously fun script (by Eric Pearson, Craig Kyle and Christopher L. Yost) crackles with wit, and New Zealand director Taika Waititi (who also made the acclaimed indie hits ‘Hunt For the Wilderpeople’ and ‘What We Do In the Shadows’) gives Thor’s traditional, comic-book core a fresh, zippy new spin.”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img style="width:110px;" src="data:image/svg+xml;charset%3DUS-ASCII,%3Csvg%20id%3D%22Layer_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22776.692%22%20height%3D%22200%22%20viewBox%3D%220%200%20776.692%20200%22%3E%3Cpath%20fill%3D%22%23FFF%22%20d%3D%22M104.67%20198.318H43.262C14.942%20198.318%200%20177.305%200%20145.32V54.768C0%2022.798%206.78%200%2047.724%200h15.648c28.893%200%2041.498%2022.83%2041.498%2055.082V72.9H57.76V44.064c0-2.524-2.128-4.808-4.653-4.808h-.58c-2.523%200-4.855%202.283-4.855%204.808v108.563c0%203.088%202.625%205.03%205.15%205.03h51.23l.617%2040.66zM371.764%202.035v196.283h-47.11V109.59l-10.796%2088.728H282.25l-11.59-88.728v88.728h-46.95l-.158-196.283h62.404l11.7%2088.783L309.23%202.035h62.536zm118.824%200h47.67V157.66h32.806v40.658H490.59V2.035zm285.54%200L750.9%2096.81l25.793%20101.51h-49.348l-8.973-50.474-10.38%2050.473h-42.9l25.8-94.78L665.654%202.03h49.352l8.13%2049.63L733.23%202.03h42.9zM157.38%2044.982v110.052c0%202.972%202.41%205.38%205.645%205.38a5.38%205.38%200%200%200%205.38-5.38V44.982c0-2.973-2.41-5.385-5.642-5.385a5.387%205.387%200%200%200-5.383%205.385zm58.854%20112.957c0%2028.315-13.744%2042.06-42.066%2042.06h-22.434c-28.318%200-42.057-13.74-42.057-42.06V42.41c0-28.32%2013.736-42.06%2042.057-42.06h22.434c28.322%200%2042.064%2013.738%2042.064%2042.06l.002%20115.527zM427.248%2039.87v46.49l4.945.025c2.732%200%204.943-2.405%204.943-5.38V45.253c0-2.97-2.21-5.38-4.943-5.38h-4.945zm.162%2081.94v76.505h-47.67V2.035h62.25c28.32%200%2042.062%2013.738%2042.062%2042.058v35.66c0%2028.322-13.742%2042.06-42.062%2042.06h-14.58zM576.417%201.495h84.22v40.708h-36.495v35.932h35.098v40.708h-35.098v38.46h37.06v40.708h-84.784V1.5z%22%2F%3E%3C%2Fsvg%3E"/>
					</div>
					<div class="title">
						“…in Watiti's hands it becomes a dizzying array of BIG action sequences, intergalactic mayhem, and jokes on jokes on jokes”
					</div>
				</div>
				<div class="snippet">
					<div class="press-logo">
						<img class="cnet" src="/img/press/cnet.png"/>
					</div>
					<div class="title">
						“‘Ragnarok’ is a joy from start to finish.”
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="photos">
		<div class="slider">
			
		</div>
		<div class="left-arrow arrow"></div>
		<div class="right-arrow arrow"></div>
		<div class="dots">
			
		</div>
	</div>
	<div class="score">
		<div class="content">
			<div class="angled">
				<div class="inner">

					<div class="track-list">

						<div class="scrollable">
							<div class="artist-pic"><img src="/img/thor/mark-pic.png"/></div>
							<h3>BEST ORIGINAL SCORE</h3>
							<h5>BY MARK MOTHERSBAUGH</h5>
							
						</div>
					</div>
				</div>
			</div>
				<div class="featurette">
					<h4>IMAX&reg; SIGHT AND SOUND - MUSIC</h4>
					<h6>WATCH THE FEATURETTE</h6>
					<div class="video-container">
						<video controls>
							<source src="/media/video/thor_sightandsound_music_dom_np_YT.mp4" type="video/mp4">
						</video>
						<div class="poster"></div>
					</div>
					<div class="quote">
						<p>“‘Thor: Ragnarok’ is a score for the ages… exciting from start<br/> to finish, emotional and nostalgic, the third ‘Thor’ score<br/> really sets a new bar for spectacular and non stop action<br/> superhero scores.”</p>
						<span> - MIHNEA MANDUTEANU, SOUNDTRACK DREAMS</span>
					</div>
				</div>
		</div>
	</div>
	<div class="screenplay">
		<div class="quote">
			<p>“Waititi and screenwriters Eric Pearson, Craig Kyle and Christopher L. Yost know how to balance stakes and silliness, which is exactly what this movie needs.”</p>
			<div class="author">- ALONSO DURALDE, <img class="auth-logo newsweek" src="/img/press/quote-thewrap.png"></div>
		</div>
		<div class='content'>
			<div class="download-button"><a href="/media/scripts/thor_ragnarok.pdf" download="Thor_Ragnarok-FYC.pdf"><img src="/img/batb/download-btn.png"></a></div>
			<div class="text">
				<h2>BEST ADAPTED SCREENPLAY</h2>
				<div class="written">WRITTEN BY</div>
				<div class="names">
					<div>ERIC PEARSON <span class="and">AND</span><br/>CRAIG KYLE <span class="and">&</span> CHRISTOPHER L. YOST</div>
				</div>
			</div>
		</div>
	</div>
	<?php //if($isMobile) { ?>
		<footer>
			<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
			<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
			<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
			<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
			<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
			<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
			<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
		</footer>
	<?php//} ?>
</div>