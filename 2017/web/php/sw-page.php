<div class="bg-holder">
	<div class="bg fyc"></div>
	<div class="bg screenings"></div>
	<div class="bg synopsis"></div>
	<div class="bg score"></div>
	<div class="bg photos"></div>
	<div class="bg production"></div>
</div> 
<div class="limiter">
	<div class="consider">
		<div class="content">
			<div class="logo">
				<img src="/img/sw/sw-title_white.png" alt="Coco"/>
			</div>
			<div class="first">
				<p>FOR YOUR CONSIDERATION IN ALL CATEGORIES</p>
				<h3>BEST PICTURE</h3>
				<div class="subline">PRODUCED BY</div>
				<div class="name">KATHLEEN KENNEDY, <span class="guild">p.g.a.</span><br/>RAM BERGMAN, <span class="guild">p.g.a.</span></div>
			</div>
			<div class="left">
				<h3>BEST DIRECTOR</h3>
				<div class="name">RIAN JOHNSON</div>
				<h3>BEST ADAPTED SCREENPLAY</h3>
				<div class="name">RIAN JOHNSON</div>
				
				<h3>BEST ACTOR</h3>
				<div class="name">MARK HAMILL</div>
				<h3>BEST ACTRESS</h3>
				<div class="name">DAISY RIDLEY</div>
				<h3>BEST SUPPORTING ACTOR</h3>
				<div class="name">
					ADAM DRIVER<br/>
					JOHN BOYEGA<br/>
					OSCAR ISAAC<br/>
					BENICIO DEL TORO<br/>
					ANDY SERKIS<br/>
					DOMHNALL GLEESON
				</div>
				<h3>BEST SUPPORTING ACTRESS</h3>
				<div class="name">
					CARRIE FISHER<br/>
					GWENDOLINE CHRISTIE<br/>
					LAURA DERN<br/>
					KELLY MARIE TRAN<br/>
				</div>
				<h3>BEST CINEMATOGRAPHY</h3>
				<div class="name">STEVE YEDLIN, <span class="guild">ASC</span></div>
				<h3>BEST FILM EDITING</h3>
				<div class="name">BOB DUCSAY</div>
				
			
			</div>
			<div class="right">
				<div>
					<h3>BEST PRODUCTION DESIGN</h3>
					<div class="subline">PRODUCTION DESIGNER</div>
					<div class="name">RICK HEINRICHS</div>
					<div class="subline">SET DECORATOR</div>
					<div class="name">RICHARD ROBERTS</div>
				</div>
				<div>
					<h3>BEST COSTUME DESIGN</h3>
					<div class="name">MICHAEL KAPLAN</div>
				</div>
				<div>
					<h3>BEST MAKEUP AND HAIRSTYLING</h3>
					<div class="name">
						PETER SWORDS KING<br/>
						NEAL SCANLAN
					</div>
				</div>
				<div>
					<h3>BEST SOUND MIXING</h3>
					<div class="subline">RE-RECORDING MIXERS</div>
					<div class="name">
						REN KLYCE<br/>
						DAVID PARKER<br/>
						MICHAEL SEMANICK
					</div>
					<div class="subline">SOUND MIXER</div>
					<div class="name">STUART WILSON</div>
				</div>
				<div>
					<h3>BEST SOUND EDITING</h3>
					<div class="subline">SUPERVISING SOUND EDITORS</div>
					<div class="name">
						MATTHEW WOOD<br/>
						REN KLYCE
					</div>
				</div>
				<div>
					<h3>BEST VISUAL EFFECTS</h3>
					<div class="name">
						BEN MORRIS<br/>
						MIKE MULHOLLAND<br/>
						CHRIS CORBOULD<br/>
						NEAL SCANLAN
					</div>
				</div>
				<div>
					<h3>BEST ORIGINAL SCORE</h3>
					<div class="name">JOHN WILLIAMS</div>
				</div>
			</div>
		</div>
	</div>

	<div class="screenings">
		<div class="guild-members-co">
			ATTENTION GUILD MEMBERS<br/>
			CLICK HERE
			<div>
			YOUR MEMBERSHIP CARD<br/> WILL ADMIT YOU TO<br/> THE FOLLOWING THEATERS
			</div>
		</div>
		<div class="pop-up">
			<div class="pop-up-close"></div>
			<div class="inside">
				<h3>GUILD MEMBERS</h3>
				<p>You may use your membership card to admit you and a guest to the following theatres in your city, subject to seating capacity/availability.
				<br/><br/>
				Cinemark does not allow a guest.</p>
				<ul>
					<li><span></span>ArcLight/Pacific will admit AMPAS, WGA, PGA, DGA (Monday–Thursday only/no holidays).
					</li>
					<li><span></span>AMC will admit AMPAS, BAFTA, ACE, ADG, ASC, CAS, DGA, HFPA, MPEG, MPSE, PGA, WGA, CDG, VES (Monday–Thursday only/no holidays- Los Angeles and NY Only).</li>

					<li><span></span>Regal will admit AMPAS, WGA, DGA (Monday–Thursday only/no holidays).</li>

					<li><span></span>Laemmle will admit AMPAS, DGA, WGA (Monday–Thursday only/no holidays).</li>

					<li><span></span>Cinemark will admit AMPAS, WGA, DGA, PGA (Members only/no guest, Monday–Thursday).</li>
				</ul>
			</div>
		</div>
		<div class="content">
			<div class="cities-list">
				<div class="city selected">Los Angeles</div>
				<div class="city">New York</div>
				<div class="city">San Francisco</div>
				<div class="city">London</div>
				
			</div>
			<div class="city-holder London-holder ">
				<div class="scrollable">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder New_York-holder ">
				<div class="scrollable ">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder Los_Angeles-holder ">
				<div class="scrollable ">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
			<div class="city-holder San_Francisco-holder ">
				<div class="scrollable ">
					<img class="loader" src="/img/ui/loader.gif">
				</div>
			</div>
		<div class="disclaimer">You must be an invited member of a voting organization to attend <span class="nowrap">For Your Consideration screenings</span>. Your membership card is required for entry.</div>
		</div>
	</div>
	<div class="synopsis">
		<div class="content">
			In LucasFilm’s 'Star Wars: The Last Jedi',<br/> the Skywalker saga continues as the heroes of<br/> 'The Force Awakens' join the galactic legends<br/> in an epic adventure that unlocks age-old<br/> mysteries of the Force and shocking<br/> revelations of the past.
		</div>
	</div>
	<div class="score">
		<div class="quote">
			<p>“THE GLORIOUS SCORE HITS JUST THE RIGHT <br/>INSPIRATIONAL NOTES AND SENDS <br/>A CHILL DOWN OUR SPINE.”</p>
			<div class="author" style="">- RICHARD ROEPER, 
				<img class="auth-logo" src="/img/press/quote-chicsun.png">
			</div>
		</div>
		<div class="content">
			<h2>BEST ORIGINAL SCORE</h2>
			<div class="score-name">
				BY JOHN WILLIAMS
			</div>
			<div class="track-list">
				<div class="scrollable">
				</div>
			</div>
			
		</div>
	</div>
	<div class="photos">
		<div class="slider">
			
		</div>
		<div class="left-arrow arrow"></div>
		<div class="right-arrow arrow"></div>
		<div class="dots">
			
		</div>
	</div>
	<div class="production">
		<div class="content">
			<h2>PRODUCTION NOTES</h2>
			<a class="download" target="_blank" href="/media/pdf/SW_PRODUCTION_NOTES.pdf" download><img src="/img/ui/download-btn-mini.png"> DOWNLOAD</a>

			<div class="quote">
				<p>“THE EPIC YOU'VE BEEN LOOKING FOR.”</p>
				<div class="author">- PETER TRAVERS, <img class="auth-logo" src="/img/press/quote-rollingstone.png"></div>
			</div>
			<div class="qa">
				<h2>BAFTA Q & A</h2>
				<h3>WATCH THE FEATURETTE</h3>
				<div class="video-container">
					<video controls>
						<source src="http://disneystudiosawards.s3.amazonaws.com/videos/SW_BAFTA.mp4" type="video/mp4">
					</video>
					<img class="poster" src="/img/sw/bafta-poster.jpg">
				</div>
			</div>
		</div>
	</div>
	<div class="accolades">
		<div class="banner"><img src="/img/sw/sw-accolades-banner.jpg"></div>
		<div class="content">
			<div class="award academy" style="margin-top: 2vh;">
				<img src="/img/sw/awards/ACADEMY.jpg">
			</div>
			<br/>
			<div class="award large offset">
				<img src="/img/sw/awards/BAFTA.jpg"/>
			</div>
			<div class="award large">
				<img src="/img/sw/awards/VEW.jpg"/>
			</div>
			<br/>
			<div class="award">
				<img src="/img/sw/awards/adg.jpg"><br/>
				<img src="/img/sw/awards/CAS.jpg" >
			</div>
			<div class="award middle medium">
				<img src="/img/sw/awards/MUAHG.jpg"><br/>
					<img class="small" src="/img/sw/awards/vegas.jpg" >
			</div>
			<div class="award medium">
				<img src="/img/sw/awards/costume-design.jpg"><br/>
				<img class="small" src="/img/sw/awards/phoenix.jpg" >
			</div>
			<br/>
		
		</div>
	</div>
	<?php //if($isMobile) { ?>
	<footer>
		<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a>
		<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> 
		<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a>
		<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> 
		<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a>
		<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
		<div>&nbsp;&copy; 2017 Disney. All Rights Reserved.</div>
	</footer>
	<?php //} ?>
</div>