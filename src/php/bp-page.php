<div class="page-header">
	<div class="logo limiter"><img src="/img/bp/bp_tt.png"></div>
	<div class="nav hamburger">
		<div class="nav-link">CONSIDER</div>
		<div class="nav-link">SYNOPSIS</div>
		<div class="nav-link">SCREENINGS</div>
		<div class="nav-link">PRESS</div>
		<div class="nav-link">SCORE</div>
		<div class="nav-link">SONG</div>
		<div class="nav-link">VIDEOS</div>
		<div class="nav-link">ACCOLADES</div>
		<!-- <div class="nav-link">SCREENPLAY</div> -->
	</div>
</div>
<div class="film-strip">
	<div class="slider clear">
	
	</div>
	<div class="arrow" id="left-arrow"></div>
	<div class="arrow" id="right-arrow"></div>
</div>
<div class="subpages">
	<div class="limiter">
		<div class="consider">
			<div class="prime-quote">
				<div class="press-logo time"><img src="/img/press/Time-color.png"></div>
				<div class="name">Jamil Smith</div>
				<div class="quote">
					<span>“A vision of unmitigated excellence</span>, <br/><em>Black Panther</em> proves that making movies about black lives is part of <br/>showing that they matter.”
				</div>
				
			</div>
			<div class="content clear">
				<h3>FOR YOUR CONSIDERATION</h3>
					<div class="inline-clear">
						<div class="left">
							<div class="category">BEST PICTURE</div>
							<div class="name">Kevin Feige, <span class="lowercase">p.g.a.</span></div>

							<div class="category">BEST DIRECTOR</div>
							<div class="name">Ryan Coogler</div>

							<div class="category">BEST ADAPTED SCREENPLAY</div>
							<div class="name">Ryan Coogler &<br/>
							Joe Robert Cole</div>

							<div class="category">BEST ACTOR</div>
							<div class="name">Chadwick Boseman</div>

							<div class="category">BEST SUPPORTING ACTOR</div>
							<div class="name">
								Michael B. Jordan<br/>
								Martin Freeman<br/>
								Daniel Kaluuya<br/>
								Winston Duke<br/>
								Sterling K. Brown<br/>
								Forest Whitaker<br/>
								Andy Serkis
							</div>
						</div>
						<div class="left">
							<div class="category">BEST SUPPORTING ACTRESS</div>
							<div class="name">
								Lupita Nyong’o<br/>
								Danai Gurira<br/>
								Letitia Wright<br/>
								Angela Bassett
							</div>

							<div class="category">BEST CINEMATOGRAPHY</div>
							<div class="name">Rachel Morrison, <span class="guild">ASC</span></div>
								

							<div class="category">BEST FILM EDITING</div>
							<div class="name">
								Michael P. Shawver<br/>
								Debbie Berman, <span class="guild">ACE</span>
							</div>

							<div class="category">BEST PRODUCTION DESIGN</div>
							<div class="subline">Production Designer</div>
							<div class="name">Hannah Beachler</div>
							<div class="subline">Set Decorator</div>
							<div class="name">Jay Hart</div>
						</div>

						<div class="left">
							<div class="category">BEST COSTUME DESIGN</div>
							<div class="name">Ruth E. Carter</div>

							<div class="category">BEST MAKEUP & HAIRSTYLING</div>
							<div class="name">
								Joel Harlow<br/>
								Camille Friend<br/>
								Ken Diaz
							</div>

							<div class="category">BEST SOUND MIXING</div>
							<div class="subline">Re-Recording Mixers</div>
							<div class="name">
								Steve Boeddeker<br/>
								Brandon Proctor
							</div>
							<div class="subline">Production Sound Mixer</div>
							<div class="name">Peter Devlin</div>

							<div class="category">BEST SOUND EDITING</div>
							<div class="subline">Supervising Sound Editors</div>
							<div class="name">
								Benjamin A. Burtt<br/>
								Steve Boeddeker
							</div>
						</div>
						<div class="right">
							<div class="category">BEST VISUAL EFFECTS</div>
							<div class="name">
								Geoffrey Baumann<br/>
								Jesse James Chisholm<br/>
								Craig Hammack<br/>
								Dan Sudick
							</div>

							<div class="category">BEST ORIGINAL SCORE</div>
							<div class="name">Ludwig Göransson</div>

							<div class="category">BEST ORIGINAL SONG</div>
							<div class="name">“All The Stars”</div>
							<div class="subline">Music By</div> 
							<div class="name">
								Kendrick Lamar<br/>
								Mark “Sounwave” Spears<br/>
								Anthony “TOP DAWG” Tiffith<br>
			
								
								
							</div>
							<div class="subline">Lyrics by </div>
							<div class="name">
								Kendrick Lamar<br/>
								SZA<br/>
								Anthony “TOP DAWG” Tiffith
							</div>
						</div>
					</div>
			</div>
		</div>
		<div class="synopsis">
			<div class="prime-quote">
				<div class="press-logo ny-times"><img src="/img/press/NY_Times.jpg"></div>
				<div class="name">Manohla Dargis</div>
				<div class="quote">
					“A jolt of a movie, <span><em>Black Panther</em> creates wonder with great flair and feeling<br/></span> partly through something Hollywood rarely dreams of anymore: myth.”

				</div>
				
			</div>
			<div class="content clear">
				<p>BLACK PANTHER is director Ryan Coogler’s take on a modern African hero and a utopian vision of what an uncolonized Africa might look like.  The film explores the conflict between two powerful men, one African and one African-American, who are mirror images of each other, each grappling with his own history, home and very identity.  When Prince T’Challa (Chadwick Boseman) becomes king of the hidden, technologically advanced kingdom Wakanda, he is forced to defend his throne against rogue mercenary Erik Killmonger (Michael B. Jordan).  Wakanda is also alive with strong, intelligent women — from Wakanda’s elite all-female security force, led by Lupita Nyong’o and Danai Gurira, to T’Challa's own tech-savvy sister (Letitia Wright) and mother (Angela Bassett) — who are portrayed as equals to the men they protect and advise.</p>

				<p>From a screenplay written by Ryan Coogler and Joe Robert Cole, BLACK PANTHER also stars Forest Whitaker, Martin Freeman, Daniel Kaluuya, Winston Duke, Sterling K. Brown and Andy Serkis.</p>
			</div>
		</div>
		<div class="screenings">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/Rolling_Stone-color.png"></div>
				<div class="name">Peter Travers</div>
				<div class="quote">
					<span>“<em>Black Panther</em> is an exhilarating triumph on every level</span><br/> from writing, directing, 
					acting, production design, costumes, music, special effects to you name it. <br/>
					<span>A NEW FILM CLASSIC.”</span> 
				</div>
				
			</div>
			<div class="callout">
				OSCAR® BEST-PICTURE NOMINEE & SAG® BEST ENSEMBLE WINNER<br/>
				<span>“BLACK PANTHER”</span><br/> 
				RETURNS TO THE BIG SCREEN BEGINNING FEBRUARY 1st.<br/><br/>
 
				SEE THE MOVIE FOR FREE IN HONOR OF BLACK HISTORY MONTH <br/>AT PARTICIPATING AMC THEATRES<br/>
				<a target="_blank" href="https://www.weticketit.com/blackpanther/">GET TICKETS</a>
			</div>
			<div class="content">
				<div class="disclaimer">You must be an invited member of a voting organization to attend For Your Consideration screenings. <br/>Your membership card is required for entry.</div>
				<div class="cities-list"></div>
				<div class="screenings-holder">

				</div>
			</div>
		</div>
		<div class="press">
			
			<div class="content">
				<div class="quote special">
					<div class="press-image" style="height:30px"><img src="/img/press/NY_Times.jpg"></div>
					<div class="banner" style="line-height: 1em;">“DIRECTOR RYAN COOGLER AND HIS FEMALE DEPARTMENT HEADS HELPED BRING ‘BLACK PANTHER’ TO LIFE.”</div>
					<div class="quote-text">

						<a target="_blank" href="https://www.nytimes.com/2019/01/09/movies/black-panther-oscars-ryan-coogler.html">READ MORE</a></div>
				</div>
				<div class="quote special">
					<div class="press-image"><img src="/img/press/Time-color.png"></div>
					<div class="banner">No. 6 PERSON OF THE YEAR 2018<br/><span>RYAN COOGLER</span></div>
					<div class="quote-text"><span>“IN A YEAR MARKED BY DIVISION, THE ‘BLACK PANTHER’<br/></span>
						DIRECTOR PROVED THAT A MOVIE CAN BRING PEOPLE TOGETHER.”<br/>
						<a target="_blank" href="http://time.com/person-of-the-year-2018-ryan-coogler-runner-up/">READ MORE</a>
					</div>
				</div>
				<div class="quote special">
					<div class="press-image" style="height:28px"><img src="/img/press/LA_Times.png"></div>
					<div class="name">Joe Robert Cole</div>
					<div class="quote-text"><span>“Creating heroes — and inclusion — for all in 'Black Panther'”</span><br/><a target="_blank" href="https://www.latimes.com/entertainment/envelope/la-en-mn-writing-black-panther-20181114-story.html#nws=true">READ MORE</a></div>
					
				</div>
				<div class="quote special">
					<div class="quote-text">
						<span>WGA 3rd & Fairfax Podcast</span><br/>
						Hilliard Guess talks with Joe Robert Cole about his career and recent film, BLACK PANTHER<br/>
						<a href="http://hwcdn.libsyn.com/p/c/9/2/c92423e62970be0d/WGAW0127.mp3?c_id=29619533&cs_id=29619533&expiration=1546063406&hwt=5eddd04622f9a631c292d96a5b7d68fe">LISTEN HERE
						</a>
					</div>
				</div>
				<div class="quote">
					”<em>Black Panther</em> is an exhilarating triumph on every level from writing, directing, <br/>acting, production design, costumes, music, special effects to you name it. <br/>A NEW FILM CLASSIC.” 
					<div class="press-image rolling-stone"><img src="/img/press/Rolling_Stone-color.png"></div>
					<div class="name">Peter Travers</div>
				</div>
				<div class="quote">
					“A jolt of a movie, <em>Black Panther</em> CREATES WONDER WITH GREAT FLAIR AND FEELING <br/>partly through something Hollywood rarely dreams of anymore: myth.”
					<div class="press-image"><img src="/img/press/NY_Times.jpg"></div>
					<div class="name">Manohla Dargis</div>
				</div>
				<div class="quote">
					”A VISION OF UNMITIGATED EXCELLENCE, <em>Black Panther</em> proves that making <br/>movies about black lives is part of showing that they matter.”
					<div class="press-image"><img src="/img/press/Time-color.png"></div>
					<div class="name">Jamil Smith</div>
				</div>
				<div class="quote">
					“<em>BLACK PANTHER</em> IS HISTORIC, crashing Hollywood barriers that <br/>should have been shattered decades ago.”<br/>
					<div class="press-image"><img src="/img/press/IndieWire-logo.jpg"></div>
					<div class="name">ANNE THOMPSON</div>
				</div>
				
				<div class="quote">
					”<em>Black Panther</em> ​is a TRIUMPH OF UNIVERSAL APPEAL and demographic specificity.”
					<div class="press-image forbes"><img src="/img/press/Forbes.png"></div>
					<div class="name">Scott Mendelson</div>
				</div>
				
			</div>
		</div>
		<div class="score">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/TheWrap-Logo.jpg"></div>
				<div class="name">ALONSO DURALDE</div>
				<div class="quote">
					<span>“IT'S THRILLINGLY ALIVE. THE ECLECTIC AND VIBRANT MUSIC CHOICES;</span><br/>THE SCORE BY LUDWIG GÖRANSSON VACILLATES SMOOTHLY BETWEEN <br/>EUROPEAN STRINGS AND AFRICAN PERCUSSION AND WOODWINDS”
				</div>
				
			</div>
			<div class="content">
				
					<div class="category">BEST ORIGINAL SCORE</div>
					<div class="name">LUDWIG GÖRANSSON</div>

					<a href="https://disneystudiosawards.s3.amazonaws.com/mp3/BP_Suite_v2.mp3" class="overture" data-title="Highlights">Click here to listen to highlights from Ludwig Goransson’s Score</a>
					<div class="audio-player">
						<div class="song-title">Wakanda Origins</div>
					
						<audio controls controlsList="nodownload" autobuffer>
							<source src="" type="audio/mp3"/>
						</audio>
					

						<div class="track-list">
							<div class="col left"></div>
							<div class="col right"></div>
							
						</div>
					</div>
					
			</div>
		</div>
		<div class="song">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/vanity-fair.png"></div>
				<div class="name">LISA ROBINSON</div>
				<div class="quote">
					“PULITZER PRIZE-WINNING ‘POET LAUREATE OF HIP HOP’<br/>
					<span>KENDRICK LAMAR HAS MADE HISTORY WITH HIS MUSIC.”</span>
				</div>
				
			</div>
			<div class="content">
				
				<div class="song-div video">
					
						<div class="category">BEST ORIGINAL SONG</div>
						<div class="name">“ALL THE STARS”</div>
						<div class="video-player">
							<video controls controlsList="nodownload" poster="/img/bp/crushin.jpg">
								<source src="https://disneystudiosawards.s3.amazonaws.com/videos/BlackPanther-All_The_Stars.mp4">
							</video>
							<div class="poster"><img src="/img/bp/song-cover.jpg"><div class="play-btn"></div></div>
						</div>
						<div class="subline">MUSIC BY</div>
						<div class="sub-name">Kendrick Lamar, Mark “Sounwave” Spears, Anthony “TOP DAWG“ Tiffith<br/> </div>
						<div class="subline">LYRICS BY</div>
						<div class="sub-name">Kendrick Lamar, SZA, Anthony “TOP DAWG“ Tiffith</div>
		
					
				</div>
			
			</div>
		</div>
		<div class="videos">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/Rolling_Stone-color.png"></div>
				<div class="name">PETER TRAVERS</div>
				<div class="quote">
					“<span>An exhilarating triumph on every level.</span> <br/>What sneaks up and floors you is the film's racial conscience and profound, astonishing beauty. <br/><span>It’s history in the making.”</span>
				</div>
				
			</div>
			<div class="content">
				<h3>WATCH THE “WELCOME TO WAKANDA” FEATURETTE</h3>
				<div class="video-player">
					
				</div>
				<br/><br/><br/><br/>
				<h3>WATCH THE Q&A WITH DIRECTOR/WRITER RYAN COOGLER<br/><span style="font-size:0.7em">PRESENTED BY FILM COMMENT AND THE FILM SOCIETY OF LINCOLN CENTER</span></h3>
				<div class="video-player" style="overflow:hidden;padding-top: 56.25%">
					
				</div>
			</div>
		</div>
		<div class="accolades">
			<div class="content">
				<img src="/img/bp/awards/bp-awards5.png"/>
				
			</div>
		</div>
		<!-- <div class="screenplay">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/IndieWire-logo.jpg"></div>
				<div class="quote">
					“Fruitvale Station” and “Creed” writer-director Coogler’s thoughtful screenplay raises many <br/>questions about the role and responsibility of a rich nation in the world, as well as the ultimate <br/>consequences of neglecting and abandoning the less fortunate among us.
				</div>
				<div class="name">ANNE THOMPSON</div>
			</div>
			<div class="content">
				<h4>BEST ADAPTED SCREENPLAY</h4>
				<div class="name">RYAN COOGLER & JOE ROBERT COLE</div>
				<a class="view" target="_blank" href="//<?php echo $_SERVER['SERVER_NAME'] ?>/viewer.php">View Screenplay</a>
			</div>
		</div> -->
	</div>
</div>