<div class="page-header">
	<div class="logo limiter"><img src="/img/incredibles/incredibles_tt.png"></div>
	<div class="nav">
		<div class="nav-link">CONSIDER</div>
		<div class="nav-link">SYNOPSIS</div>
		<div class="nav-link">SCREENINGS</div>
		<div class="nav-link">PRESS</div>
		<div class="nav-link">SCORE</div>
		<div class="nav-link">ACCOLADES</div>
	</div>
</div>
<div class="film-strip">
	<div class="slider clear">
		

	</div>
	<div class="arrow" id="left-arrow"></div>
	<div class="arrow" id="right-arrow"></div>
</div>
<div class="subpages">
	<div class="limiter">
		<div class="consider">
			<div class="prime-quote">
				<div class="press-logo rolling-stone"><img src="/img/press/Rolling_Stone-color.png"></div>
				<div class="name">Peter Travers</div>
				<div class="quote">
					<span>“IT REALLY <em>IS</em> INCREDIBLE...<br/></span>
					EVERY BIT THE START-TO-FINISH SENSATION AS THE ORIGINAL.”
				</div>
				
			</div>
			<div class="content clear">
				<h3>FOR YOUR CONSIDERATION</h3>
					<div class="inline-clear">
						<div class="left">
							
							<div class="category">BEST PICTURE</div>
							<div class="name">
								John Walker, <span class="lowercase">p.g.a.</span> <br/>
								Nicole Paradis Grindle, <span class="lowercase">p.g.a.</span>
								
							</div>

							<div class="category">BEST ANIMATED FEATURE </div>
							<div class="name">
								Brad Bird<br/>
								John Walker, <span class="lowercase">p.g.a.</span>
								<br/>
								Nicole Paradis Grindle, <span class="lowercase">p.g.a.</span>
							</div>

							<div class="category">BEST DIRECTOR</div>
							<div class="name">Brad Bird</div>

							<div class="category">BEST ADAPTED SCREENPLAY</div>
							<div class="name">Brad Bird</div>
						</div>
						<div class="left">
							<div class="category">BEST CINEMATOGRAPHY</div>
							<div class="name">
								MAHYAR ABOUSAEEDI<br/>
								Erik Smitt
							</div>

							<div class="category">BEST FILM EDITING</div>
							<div class="name">Stephen Schaffer, <span class="guild">ACE</span></div>

							<div class="category">BEST PRODUCTION DESIGN</div>
							<div class="name">Ralph Eggleston</div>
						
							<div class="category">BEST SOUND MIXING</div>
							<div class="subline">Re-Recording Mixers</div>
							<div class="name">
								Michael Semanick<br/>
								Nathan Nance
							</div>
							<div class="subline">Original Dialogue Mixer</div>
							<div class="name">Vince Caro </div>
						</div>
						<div class="right">
							
						
						
							<div class="category">BEST SOUND EDITING</div>
							<div class="subline">Supervising Sound Editors </div>
							<div class="name">
								Coya Elliott<br/>
								Ren Klyce
							</div>

							<div class="category">BEST VISUAL EFFECTS</div>
							<div class="name">
								
								Rick Sayre<br/>
								
								Bill Watral<br/>
								Gordon Cameron<br/>
								Esdras Varagnolo
							</div>

							<div class="category">BEST ORIGINAL SCORE</div>
							<div class="name">Michael Giacchino</div>
						</div>
					</div>
			</div>
		</div>
		<div class="synopsis">
			<div class="prime-quote">
				<div class="press-logo la-times"><img src="/img/press/LA_Times.png"></div>
				<div class="name">Kenneth Turan</div>
				<div class="quote">
					<span>“<em>INCREDIBLES 2</em> is the superhero family saga we need right now.”</span>
					
				</div>
				
			</div>
			<div class="content clear">
				In INCREDIBLES 2, Helen (voice of Holly Hunter) is called on to lead a campaign to bring Supers back, while Bob (voice of Craig T. Nelson) navigates the day-to-day heroics of “normal” life at home with Violet (voice of Sarah Vowell), Dash (voice of Huck Milner) and baby Jack-Jack—whose super powers are about to be discovered. Their mission is derailed, however, when a new villain emerges with a brilliant and dangerous plot that threatens everything. But the Parrs don’t shy away from a challenge, especially with Frozone (voice of Samuel L. Jackson) by their side.
			</div>
		</div>
		<div class="screenings">
			<div class="prime-quote">
				<div class="press-logo time"><img src="/img/press/Time-color.png"></div>
				<div class="name">Stephanie Zacharek</div>
				<div class="quote">
					<span>“BOLD, RAPTUROUSLY ENTERTAINING.”</span>
					
				</div>
				
			</div>
			<div class="content">
				<div class="disclaimer">You must be an invited member of a voting organization to attend For Your Consideration screenings. <br/>Your membership card is required for entry.</div>
				<div class="cities-list"></div>
				<div class="screenings-holder">

				</div>
			</div>
		</div>
		<div class="press">
			<div class="content">
				<div class="quote">
					”PIXAR SEQUEL IS BOTH SUPER AND SUBVERSIVE”
					<div class="press-image rolling-stone"><img src="/img/press/Rolling_Stone-color.png"></div>
					<div class="name">PETER TRAVERS</div>
				</div>
				<div class="quote">
					“Boosted by central characters that remain <br/>VASTLY ENGAGING AND A DEEP SUPPLY OF WIT,<br/> <em>Incredibles 2</em> certainly proves worth the wait”
					<div class="press-image thr"><img src="/img/press/THR-color.png"></div>
					<div class="name">TODD McCARTHY</div>
				</div>
				<div class="quote">
					“It’s not only the <br/>
					BEST ANIMATED FILM OF THE YEAR,<br/> 
					IT'S ONE OF THE BEST FILMS OF THE YEAR,<br/>
					period.”
					<div class="press-image playlist"><img src="/img/press/THE_PLAYLIST.png"></div>
					<div class="name">DREW TAYLOR</div>
				</div>
			
				<div class="quote">
					“<em>INCREDIBLES 2</em> IS BOTH A DYNAMITE ACTION-COMEDY<br/>
					and different enough from both its predecessor and the last 13 years<br/>
					of superhero flicks to stand apart from the crowd. <br/>
					IT IS A FUNNY, THOUGHTFUL, AND THRILLING ADVENTURE<br/>
					 that artistically justifies itself without invalidating <br/>
					what came before. The best animated action movie ever made.”
					<div class="press-image forbes"><img src="/img/press/Forbes.png"></div>
					<div class="name">Scott Mendelson</div>
				</div>
			</div>
		</div>
		<div class="score">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/Observer.jpg"></div>
				<div class="name">OLIVER JONES</div>
				<div class="quote">
					“GIACCHINO’S MUSIC GLISTENS WITH BOTH<br/> <span>KNOWING IRONY</span> AND <span>GENUINE EMOTION</span>.”
					
				</div>
				
			</div>
			<div class="content">
				<div class="category">BEST ORIGINAL SCORE</div>
				<div class="name">MICHAEL GIACCHINO</div>

				<div class="audio-player">
						<div class="song-title">Episode 2</div>
					
						<audio controls controlsList="nodownload" autobuffer>
							<source src="" type="audio/mp3"/>
						</audio>
						<div class="track-list">
							<div class="col left"></div>
							<div class="col right"></div>
							
						</div>
				</div>
			</div>
		</div>
		<div class="accolades">
			<div class="content">
				

			
					<img src="/img/incredibles/awards/incredibles-awards2.png">
				
			</div>
		</div>
	</div>
</div>