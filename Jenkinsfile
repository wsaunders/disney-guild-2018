#!/usr/bin/groovy
@Library(['github.com/fabric8io/fabric8-pipeline-library@master', 'github.com/justicel/jenkins-pipeline-library@master'])
import org.yaml.snakeyaml.Yaml

def utils = new io.fabric8.Utils()

env.JOB_NAME = "${env.JOB_NAME}".toLowerCase()

def label = "buildpod.${env.JOB_NAME}.${env.BUILD_NUMBER}".replace('-', '_').replace('/', '_').replace('%2F', '_').replace('%', '_')

String buildImage = 'docker:17.12.1-ce-git'
String jnlpImage = 'jenkins/jnlp-slave:3.15-1'

withEnv(["KUBERNETES_NAMESPACE=default", "FABRIC8_DOCKER_REGISTRY_SERVICE_HOST=docker-cse.wds.io", "FABRIC8_DOCKER_REGISTRY_SERVICE_PORT=5000", "JOB_NAME=${env.JOB_NAME}".replace('/', '-').replace('%2F', '-').replace('%', '-') ]) {
  podTemplate(
        cloud: 'kubernetes',
        serviceAccount: 'jenkins',
        label: label,
        imagePullSecrets: ['docker-registry'],
        containers: [
            [
                name: 'jnlp',
                image: "${jnlpImage}",
                args: '${computer.jnlpmac} ${computer.name}',
                workingDir: '/home/jenkins/'
            ],
            [
                name: 'builder',
                image: "${buildImage}",
                command: 'sh -c',
                args: 'cat',
                ttyEnabled: true
            ]
        ],
        volumes: [
            secretVolume(secretName: 'jenkins-docker-cfg', mountPath: '/home/jenkins/.docker'),
            secretVolume(secretName: 'jenkins-docker-cfg', mountPath: '/root/.docker'),
            //emptyDirVolume(mountPath: '/tmp', memory: false),
            hostPathVolume(hostPath: '/tmp', mountPath: '/tmp'),
            hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
        ],
        envVars: [
            podEnvVar(key: 'DOCKER_API_VERSION', value: '1.23'),
            podEnvVar(key: 'DOCKER_HOST', value: 'unix:///var/run/docker.sock'),
            podEnvVar(key: 'DOCKER_CONFIG', value: '/home/jenkins/.docker/')
        ]
    ) {

      node(label) {

        def envDev = 'development'
        def envProd = 'production'
        def domainList = [development: ['dev-waltdisneystudiosawards.wds.io', 'dev.waltdisneystudiosawards.com'], production: ['waltdisneystudiosawards.wds.io', 'www.waltdisneystudiosawards.com', 'waltdisneystudiosawards.com']]
        def newVersion = ''

        checkout scm

        newVersion = sh( script: 'git rev-parse --short HEAD', returnStdout: true).toString().trim() + "_${env.BUILD_NUMBER}"
        def clusterImageName = "${env.FABRIC8_DOCKER_REGISTRY_SERVICE_HOST}:${env.FABRIC8_DOCKER_REGISTRY_SERVICE_PORT}/${env.KUBERNETES_NAMESPACE}/${env.JOB_NAME}:${newVersion}".toLowerCase()

        container(name: 'builder') {
          stage('Build Release') {
            echo "Build Version: ${newVersion}"
            echo "Image Name: ${clusterImageName}"
        
            sh "apk update && apk add procps"
            sh "docker build -f Dockerfile.prod -t ${clusterImageName} ." 
            sh "docker push ${clusterImageName}"
            sh "docker rmi ${clusterImageName}"
          }

          if (env.ENVIRONMENT == "development") {

            stage('Rollout to Development') {
              helmDeployment {
                namespace = envDev
                resourceRequestCPU = '200m'
                resourceRequestMemory = '128Mi'
                resourceLimitCPU = '200m'
                resourceLimitMemory = '128Mi'
                replicaCount = 2
                internalPort = 80
                healthUri = '/'
                healthPort = 80
                imageName = clusterImageName
                imagePullSecret = 'docker-registry'
                hostNames = domainList.development
                ingressAnnotations = ["kubernetes.io/ingress.class": "nginx", "kubernetes.io/tls-acme": "true", "kubernetes.io/service-upstream": "true"]
              }
            }

          } else {

            stage('Rollout to Production') {
              helmDeployment {
                namespace = envProd
                resourceRequestCPU = '500m'
                resourceRequestMemory = '512Mi'
                resourceLimitCPU = '500m'
                resourceLimitMemory = '512Mi'
                replicaCount = 2
                internalPort = 80
                healthUri = '/'
                healthPort = 80
                imageName = clusterImageName
                imagePullSecret = 'docker-registry'
                hostNames = domainList.production
                ingressAnnotations = ["kubernetes.io/ingress.class": "nginx", "kubernetes.io/tls-acme": "true"]
              }
            }
          }
        }//container
      }//node
    }//podtemplate
}//withenv
