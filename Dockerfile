FROM php:7.0.27-apache
RUN a2enmod headers
RUN a2enmod rewrite
RUN a2enmod deflate

ADD ./web /var/www/html
#ADD  htaccess /var/www/html/.htacccess
ADD apache.conf /etc/apache2/sites-enabled/000-default.conf