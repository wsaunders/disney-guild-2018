<?php
$domain = $_SERVER['HTTP_HOST'];
$film = $_GET['film'];
$url = '';
$json = '';
switch ($film) {
    case 'rogue-one':
        $url = 'https://spreadsheets.google.com/feeds/cells/1Vzhgftt_wDXIevk58PrQJkIZ97IuzPkyA9NWnl8hKXA/od6/public/basic?alt=json';
        break;
    case 'captain-america-civil-war':
        $url = 'https://spreadsheets.google.com/feeds/cells/1n35swNF5ZiLHPNnM6BWSSPE5o3JtDe2W27wi5dVuYgA/od6/public/basic?alt=json';
        break;
    case 'doctor-strange':
        $url = 'https://spreadsheets.google.com/feeds/cells/1kk6cefCkk-te4IZYzbcPuGq-rJrnJocNop0l1JJcZy4/od6/public/basic?alt=json';
        break;
    case 'finding-dory':
        $url = 'https://spreadsheets.google.com/feeds/cells/1RcEJ8eJlxsiz-ZYGPJeDhtZ5K5FDb7j6yMUwNhyQFKI/od6/public/basic?alt=json';
        break;
    case 'inner-workings':
        $url = 'https://spreadsheets.google.com/feeds/cells/1YUbYdU9JWeI0DZol0cbQUtCaTdF_RCSSMDfYjHXn0ik/od6/public/basic?alt=json';
        break;
    case 'moana':
        $url = 'https://spreadsheets.google.com/feeds/cells/1hL0b_y4qllD0FE0mJjGrzptjGOkDmIRnRHVadnKb1_A/od6/public/basic?alt=json';
        break;
    case 'piper':
        $url = 'https://spreadsheets.google.com/feeds/cells/1A9899U0FEfyfOU4bH1HXnNvnRjxj0PggEdyOKyytEb0/od6/public/basic?alt=json';
        break;
    case 'queen-of-katwe':
        $url = 'https://spreadsheets.google.com/feeds/cells/1Treuun4yiP1ojGRiL_groJiDinufHIeRAow8XgeKMkk/od6/public/basic?alt=json';
        break;
    case 'the-jungle-book':
        $url = 'https://spreadsheets.google.com/feeds/cells/19Zo7LS2h-jtkyk_gwUsY9kbOWlV3IFCvOmN8O5zF5hQ/od6/public/basic?alt=json';
        break;
    case 'zootopia':
        $url = 'https://spreadsheets.google.com/feeds/cells/1YH93MlLvZT1RkVNAfpDdEz7fWb_Tzrt2x8beJXpjh1w/od6/public/basic?alt=json';
        break;
}
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$json = curl_exec($ch);
curl_close($ch);

$decode = json_decode($json, true);

function getNext($decode, $key, $theArray, $rowLength) {
    $tempArray = array();
    if ($rowLength == 1) {
        array_push($tempArray, $decode['feed']['entry'][$key+1]['content']['$t']);
    } else {
        for ($i=1; $i < $rowLength+1; $i++) {
            if (isset($decode['feed']['entry'][$key+$i])) {
                array_push($tempArray, $decode['feed']['entry'][$key+$i]['content']['$t']);
            }
        }
    }
    array_push($theArray, $tempArray);
    return $theArray;
}

$screenings = array();
$breaking_news = array();
$topper_praise = array();
$topperPraiseDev = array();
$about_description = array();
$acclaim = array();
$acclaimDev = array();
$press = array();
$music = array();

$thePackage = new StdClass;
if ($decode['feed']['entry']) {
    foreach ($decode['feed']['entry'] as $key => $value) {
        if ($value['content']['$t'] === 'screenings') {
            $screenings = getNext($decode,$key,$screenings,1);
        }
        if ($value['content']['$t'] === 'breaking_news') {
            $breaking_news = getNext($decode, $key, $breaking_news,4);
        }
        if ($value['content']['$t'] === 'topper_praise') {
            $topper_praise = getNext($decode, $key, $topper_praise,2);
        }
        if (strpos($domain, 'dev.') !== false) {
            if ($value['content']['$t'] === 'topper_praise-dev') {
                $topper_praise = getNext($decode, $key, $topper_praise,2);
            }
        }
        if ($value['content']['$t'] === 'about_description') {
            $about_description = getNext($decode, $key, $about_description,1);
        }
        if ($value['content']['$t'] === 'acclaim') {
            $acclaim = getNext($decode, $key, $acclaim,2);
        }
        if (strpos($domain, 'dev.') !== false) {
            if ($value['content']['$t'] === 'acclaim-dev') {
                $acclaim = getNext($decode, $key, $acclaim,2);
            }
        }
        if ($value['content']['$t'] === 'press') {
            $press = getNext($decode, $key, $press,3);
        }
        if (strpos($domain, 'dev.') !== false) {
            if ($value['content']['$t'] === 'press-dev') {
                $press = getNext($decode, $key, $press,3);
            }
        }
        if ($value['content']['$t'] === 'music') {
            $music = getNext($decode, $key, $music,3);
        }
    }
}
$thePackage->screenings = $screenings;
$thePackage->breaking_news = $breaking_news;
$thePackage->topper_praise = $topper_praise;
$thePackage->about_description = $about_description;
$thePackage->acclaim = $acclaim;
$thePackage->press = $press;
$thePackage->music = $music;

header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');
echo json_encode($thePackage);
