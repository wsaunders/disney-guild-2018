<!DOCTYPE html>
<head>

</head>
<body>
	<style>
		@font-face {
			font-family: 'Baskerville';
			src: url('../fonts/Baskerville.eot');
			src: url('../fonts/Baskerville.eot?#iefix') format('embedded-opentype'),
				url('../fonts/Baskerville.woff2') format('woff2'),
				url('../fonts/Baskerville.woff') format('woff'),
				url('../fonts/Baskerville.svg#Baskerville') format('svg');
			font-weight: bold;
			font-style: normal;
		}
		html{
			height: 100%;
			
			-webkit-font-smoothing: antialiased;
		}
		body{
			background:#000;
			height: 100%;
			margin:-1px 0 0 0;
			padding-top: 1px;
		}
		.content{
			margin:2%;
			min-height: 95%;
			border: 1px solid #ecc67b;
			text-align: center;
			color:#ecc67b;
			font-family: 'Baskerville';
			position: relative;
		}
		.white{
			color: #fff;
		}
		h3{
			margin-top: 3%;
			font-size:31px;
			line-height: 1.4em;
		}
		h5{
			font-size: 36px;
			margin: 1em 1em 0em;
		}
		h6{
			margin-top: 0;
			margin-bottom: 1em;
			font-size: 27px;
		}
		.line{
			background: #ecc67b;
			width: 10%;
			font-size: 27px;
			margin:0 auto 1em;
			height: 2px;
		}
		div{
			font-size: 23px;
			line-height: 1.7em;
		}
		.footer{
			font-size:11px;
			margin-top: 20px;
			width: 100%;
			font-family: 'arial';
		}
		#rsvp-form{
			
		    
		    
		  
		    padding:30px;
		  	margin:30px auto 0;
		  	width: 80%;
		    max-width: 600px;
		    max-height: 90%;
		    outline: 2px solid;
		    box-shadow: 15px 15px 27px rgba(0,0,0,0.3);
		}
	    #rsvp-form h2{
	    	font-weight: 500;
	    	margin: 0;
	    	font-size: 46px;
	    	float: left;
	    	border-right: 2px solid #ecc67b;
	    	padding-right: 20px;
	    }
	    #rsvp-form .screening-info{
	    	float: left;
	    	margin-left: 10px;
	    	position: relative;
	    	top: 0px;
	    	margin-bottom: 20px;
	    	width: 65%;
	    }
	    #rsvp-form .movie{
			font-weight: 700;
			text-transform: uppercase;
		}
	    #rsvp-form	.date{
			text-transform: capitalize;
		}
	    #rsvp-form .theater{

		}
	    #rsvp-form .time{

		}
	    form{
	    	clear: left;
	    	
	    	display: block;
	    	font-family: 'Helvetica';
	    }
	    #rsvp-form .input{
			height: 30px;
			font-weight: 500;
			text-transform: uppercase;
			font-size: 18px;
			width: calc(50% - 17px);
			display: inline-block;
			font:inherit;
		    line-height: 50px;
			height: 50px;
			margin-bottom: 30px;
			position: relative;
	    }
	    #rsvp-form > div {
			cursor: pointer;
		}
		#rsvp-form span{
			margin-left: 10px;
		}
		
		#rsvp-form > div.error:not(.hasDropdown){
			font-size: 12px;
			color: #ff0000;
			position: absolute;
			left: 0;
			top: 100%;
			white-space: nowrap;
			line-height: 1.6em;
		}
		#rsvp-form .hasDropdown.error{
			color: #ff0000;
		}
		#rsvp-form .pushRight{
			margin-right: 24px;
			
		}
		#rsvp-form .fullWidth{
			width: 100%;
		}
		#rsvp-form .reception{
	        height: 90px;
	        line-height:30px;
	        display: none;
	    }
	    #rsvp-form .hasDropdown{
	        line-height: 50px;
	    }
	    #rsvp-form .hasDropdown select{
	    	display: none;
	    }	
		#rsvp-form 	input{
			width: 100%;
			height: 100%;
			border: 0;
			background: #fff;
			color: #000;
			font:inherit;
			color:inherit;
			text-transform: uppercase;
			padding:0 10px;
			box-sizing: border-box;

		}
		#rsvp-form  .error{
			color: #ff0000;
		}
		#rsvp-form div.errorMsg{
			font-size: 0.5em;
			position: absolute;
			color: #ff0000;
		}
		input:focus{
			outline:none;
		}
		#rsvp-form input::-webkit-input-placeholder{
			color: #00000075;
			font: inherit;
		}
		.error ::-webkit-input-placeholder{
			color: #ff0000;
		}
		#rsvp-form input,#rsvp-form .hasDropdown{
			
			border: 2px solid;
		}
		
		#rsvp-form .hasDropdown{
			background:url(../img/ui/dropdown-arrow.jpg) no-repeat;
			background-position: 95% center;
		}
		#rsvp-form .dropdown{
			position: absolute;
			max-height: 130px;
			overflow-y: auto;
			background: #fff;
		    width:100%;
			right: -1px;
			top: 100%;
			border: 1px solid;
			display: none;
			z-index: 1;
		}
		#rsvp-form .value{
			padding:0 20px 0 0;
			box-sizing: border-box;
			height: 30px;
			border-bottom: 1px solid;
			line-height: 30px;
			background: #EEE;
			text-align: right;
		}
		#rsvp-form .value:last-child{
			border-bottom:none;
		}
		#rsvp-form .value:hover{
			background: #153f7b;
			color: #fff;
		}
	    	
	    .submit-btn{
	    	background: #ecc67b;
	    	color: #fff;
	    	clear: left;
		    line-height: 43px;
		    padding: 5px 0px;
		    font-weight: 700;
		    cursor: pointer;
		    display: block;
		    text-align: center;
		}
		#rsvp-form .submit-btn:hover{
		    background: #deb24f;
	    }
	    #rsvp-message{
	 	    margin-top: 42px;
		    font-size: 14px;
		    line-height: 1.2em;
		    display: none;
		    text-align: center;
		    font-weight: 500;
		}
		#rsvp-message h3{
	    	font-weight: 300;
	    	border-bottom: 1px solid rgba(21, 63, 123, 0.3);
	    	padding-bottom: 12px;
	    	font-size: 35px;
	    	display: inline-block;
	    	margin: 0 auto;
	    	line-height: 1em;
	    }
		#rsvp-message h4{
	    	font-weight: 300;
	    	font-size: 20px;
		    line-height: 1.6em;
	    }
	    #rsvp-message div:first-of-type{
	    	font-weight: bold;
	    	font-size: 18px;
	    	text-transform: uppercase;
	    }
		   
	    .rsvp-close-btn{
	    	position: absolute;
	    	top: 10px;
	    	right: 10px;
	    	font-size: 10px;
	    	padding-right: 20px;
	    	cursor: pointer;
	    	font-weight: 700;
	    }
	    .rsvp-close-btn	.x{
    		color: #ecc67b;
		    position: absolute;
		    right: 0;
	        top: -9px;
    		font-size: 24px;
		    width: 20px;
		    height: 20px;
		    font-weight: 700;
		    transform: rotate(-45deg);
	    }
	    /*the container must be positioned relative:*/
.custom-select {
  position: relative;
  font-family: Arial;
  line-height: 50px;
  border: 2px solid #ecc67b;
}
.custom-select.error{
	border: 2px solid #FF0000;
	top: 0;
}
.dropdown div.errorMsg{
	top: 105%;
}
.custom-select select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
  background-color: #fff;
  color: #00000075;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 21px;
  right: 16px;
  width: 0;
  height: 0;
  border: 11px solid transparent;
  border-color: #ecc67b transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #ecc67b transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  
  padding: 0px 10px;
  text-align: left;
  cursor: pointer;
   line-height: 50px;
}
/*style items (options):*/
.select-items {
  position: absolute;
  background-color: #fff;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
  max-height: 200px;
  overflow: auto;
}
.select-items > div{
    border: 1px solid #ecc67b;
    line-height: 28px;
    font-size: 14px;

}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}
#add-to-calendar{
	background: @gold;
	color: #fff;
	clear: left;
    line-height: 43px;
    padding: 6px 0  4px;
    font-weight: 700;
    cursor: pointer;
    display: block;
    text-align: center;
    margin:29px auto 0;
    font-size: 18px;
    width: 212px;
    position: relative;
}
 #add-to-calendar  > a{
    	display: block;
    	height: 100%;
    }
    	#add-to-calendar a:focus{
    		outline: none;
    	}
    
    #add-to-calendar a:hover{
    	background: @gold;
    }
    #add-to-calendar a:hover ul{
    		display: block;
 
    }
    #add-to-calendar ul{
    	position: absolute;
    	top: 100%;
    	padding:0 0 0 15px;
    	width: 100%;
    	box-sizing: border-box;
    	margin: 0;
    	display: none;
    	background: #fff;
	    box-shadow: inset 0px 8px 10px -5px #ccc;
	    padding-top: 10px;
	    padding-bottom: 10px;
	    outline: 1px solid @gold;}
	 #add-to-calendar ul li{
		text-align: left;
		line-height: 1.4em;
	}
	#add-to-calendar ul li a{
		text-decoration: none;
		font-weight: 500;
		color: @gold;
	}
	#add-to-calendar ul li a:hover{
		color:@gold;
	}
    
    var {
    	display: none;
    }
    	
  
	</style>

	<div class="content">
		<h3>Kevin Feige, Louis D'Esposito<br/>and Victoria Alonso<br/>Invite You to Celebrate</h3>
		<img src="img/special/marvel-10years.png">
		<h5>Saturday, October 27</h5>
		<h6 class="white">6:00 PM to 8:00 PM Reception</h6>

		<div class="line"></div>
		<div>The London Hotel Rooftop</div>
		<div class="white">1020 N. San Vicente Blvd., West Hollywood, CA 90069</div>
		<div>Complimentary Valet Provided</div>
		<div class='form-holder'>
			<form id="rsvp-form">
				<input name="screening_id" id="screening-id" value="0" type="hidden" maxlength="3">
				<input name="reception_id" id="reception-id" value="4" type="hidden" maxlength="3">
				<input name="rsvp-type" id="rsvp-type" value="2" type="hidden" maxlength="1">
					<input  name="referrer" value="ampas" type="hidden">
					<div class="input pushRight">
						<input type="text" id="first_name" name="first_name" placeholder="First Name" maxlength="15">
						<div class="errorMsg"></div>
					</div>
					<div class="input">
						<input type="text" id="last_name" name="last_name" placeholder="Last Name" maxlength="15">
						<div class="errorMsg"></div>
					</div>
					<br/>
					<div class="input fullWidth">
						<input type="email" id="email" name="email" placeholder="Email Address" maxlength="50">
						<div class="errorMsg"></div>
					</div>
					<div class="input pushRight dropwdown">
						<div class="custom-select">

							<select name="guests" id="guests">
								<option value="0">GUESTS: 0</option>
								<option value="0">GUESTS: 0</option>
								<option value='1'>GUESTS: 1</option>
							</select>
						</div>
						<div class="errorMsg"></div>
					</div>
					
					<div class="input dropwdown">
				
						<div class="custom-select">
							<select name="guild" id="guild" class="required" required>
								<option value="">GUILDS</option>
								<?php
									$guilds = array('AMPAS', 'AACTA', 'ACE', 'ADG', 'AFI',  'ASC', 'ASIFA', 'BAFTA', 'BFCA','BFCC', 'CAS', 'CDG', 'CSA', 'DGA', 'FIND','GALECA', 'HPA', 'HFPA', 'ICG', 'ILM', 'LAFCA','LAOFCS', 'Local 695','Local 706', 'MPEG', 'MPSE', 'MUAHS', 'NAACP', 'NSFC', 'NYFCC', 'NYFCO', 'OFCS', 'PGA', 'SAG/AFTRA', 'SAG Awards','SAG/AFTRA Film Society','SAG Foundation', 'SCL', 'USC Scripters', 'SDSA', 'SFFCC','SMPTE','NBR', 'SOC', 'VES','WFCC', 'WGA', 'WIA', 'WIF');
									for($i = 0;$i < count($guilds); $i++){
										echo "<option value='".$guilds[$i]."'>".$guilds[$i]."</option>";
									}
								?>
							</select>
						</div>
						<div class="errorMsg"></div>
					</div>
					<div class="input fullWidth reception">
						<label>RSVP TO:</label>
						<div class="hasDropdown"><span>SCREENING AND RECEPTION</span>
							<div class="dropdown">
								<div class="selected value">SCREENING AND RECEPTION</div>
								<div class="value">SCREENING ONLY</div>
								<div class="value">RECEPTION ONLY</div>
							</div>
						</div>
					</div>
					<div class="submit-btn" id="rsvp-submit">CONFIRM RSVP</div>
			</form>
			<div id="rsvp-message">

			</div>
		</div>
		<div class="footer white">&copy; 2018 MARVEL</div>
	</div>
	<script>
	(function(){
		if (window.addtocalendar)
			if(typeof window.addtocalendar.start == "function") return;
			if (window.ifaddtocalendar == undefined) { 
   			window.ifaddtocalendar = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
            var h = d[g]('body')[0];h.appendChild(s); 
        }
	})();
	</script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('form').on('submit',function(e){
				e.preventDefault();
			});
			$('body').on('focus', 'input', function(){
				$(this).removeClass('error').next('.error').html('');
			})
			var processing = false;
			$('body').on('click','#rsvp-submit', function(){
				if(!processing){
					processing = true;
					var form = $('#rsvp-form');
					var error = false;
					if($('#first_name').val()==''){
						$('#first_name').addClass('error').next('.errorMsg').html('Field Required.');
						error = true;
					}
					console.log("error:",error)
					if($('#last_name').val()==''){
						$('#last_name').addClass('error').next('.errorMsg').html('Field Required.');
						error=true;
					}
					console.log("error:",error)
					if(!$('#email').val().match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
						$('#email').addClass('error').next('.errorMsg').html('Invalid Email');
						error = true;
					}
					console.log("error:",error)
					if($('#guild').val()==''){
						$('#guild').parent().addClass('error').next('.errorMsg').html('Please choose a guild affiliation.');
						error = true;
					}
					console.log("error:",error)
					if($('#guests').val()==''){
						$('#guests').val('0');
					}else{
						for(var i = 0;i<Number($('#guests').val());i++) {
							if($('.guestname').eq(i).find('input').val()==''){
								error = true;
								$('.guestname').eq(i).find('input').addClass('error').next('.errorMsg').html('Field Required.');
							}
						}
					}
					console.log("error:",error)
					if(!error){
						var url = 'disney-ampas';
						if(location.host=='dev-waltdisneystudiosawards.wds.io' || location.host=='www.dev-waltdisneystudiosawards.wds.io')
							url = 'dev-disneystudiosawards.wds.io';
						if(location.host=='waltdisneystudiosawards.com' || location.host=='www.waltdisneystudiosawards.com')
							url ='disneystudiosawards.com';
						var data = form.serialize();
						
						$.post('//'+url+'/admin/attendees/register', data).done(function(data){
							console.log(data);
							processing = false;
							form.hide().next().show().html(data.message);
							if(data.err!='true'){
								form[0].reset();
								$('#guests').val('');
								$('#guild').val('');
								form.find('.guests').find('span').html('GUESTS');
								form.find('.guilds').find('span').html('GUILD');
							}
							
				          	addtocalendar.load();
				            
						
						}).fail(function(data){
							console.log(data);
							console.log('Fail')
							processing = false;
						})
					}else{
						processing = false;
					}
				}
			});
		});
		var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);


	</script>
</body>
</html>