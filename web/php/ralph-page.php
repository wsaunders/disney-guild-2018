<div class="page-header">
	<div class="logo limiter"><img src="/img/ralph/ralph_tt.jpg"></div>
	<div class="nav">
		<div class="nav-link">CONSIDER</div>
		<div class="nav-link">SYNOPSIS</div>
		<div class="nav-link">SCREENINGS</div>
		<div class="nav-link">SCORE</div>
		<div class="nav-link">SONG</div>
		<div class="nav-link">ACCOLADES</div>
	</div>
</div>
<div class="film-strip">
	<div class="slider clear">
		
		
	</div>
	<div class="arrow" id="left-arrow"></div>
	<div class="arrow" id="right-arrow"></div>
</div>
<div class="subpages">
	<div class="limiter">
		<div class="consider">
			
			<div class="content clear">
				<h3>FOR YOUR CONSIDERATION</h3>
					<div class="inline-clear">
						<div class="left">
							<div>
								<div class="category">BEST PICTURE</div>
								<div class="name">Clark Spencer, <span class="lowercase">p.g.a.</span></div>
							</div>
							<div>
								<div class="category">BEST ANIMATED FEATURE </div>
								<div class="name">Rich Moore<br/>
								Phil Johnston<br/>
								Clark Spencer, <span class="lowercase">p.g.a.</span>
								</div>
							</div>
							<div>
								<div class="category">BEST DIRECTOR</div>
								<div class="name">Rich Moore & <br>Phil Johnston</div>
							</div>
							<div>
								<div class="category">BEST ADAPTED SCREENPLAY</div>
								<div class="name">Phil Johnston<br/> Pamela Ribon</div>
								<!-- <div class="subline">Story By</div>
								<div class="name">Rich Moore<br/> Jim Reardon<br/> Josie Trinidad</div> -->
							</div>
							<div>
								<div class="category">BEST CINEMATOGRAPHY</div>
								<div class="name">Nathan Detroit Warner<br/>
								Brian Leach
								</div>
							</div>
							
						</div>
						<div class="left">

							<div>
								<div class="category">BEST FILM EDITING</div>
								<div class="name">Jeremy Milton, <span class="guild">ACE</span></div>
							</div>
							<div>
								<div class="category">BEST PRODUCTION DESIGN</div>
								<div class="name">Cory Loftis</div>
							</div>
							<div>
								<div class="category">BEST SOUND MIXING</div>
								<div class="subline">Re-Recording Mixers</div>
								<div class="name">David E. Fluhr, <span class="guild">CAS</span><br/>
								Gabriel Guy, <span class="guild">CAS</span>
								</div>
								<div class="subline">Original Dialogue Mixer</div>
								<div class="name">Doc Kane, <span class='guild'>CAS</span></div>
							</div>
							<div>
								<div class="category">BEST SOUND EDITING</div>
								<div class="subline">Sound Designer / Supervising Sound Editor</div>
								<div class="name">Addison Teague</div>
								<div class="subline">Supervising Sound Editor</div>
								<div class="name">Jacob Riehle</div>
							</div>
							<div>
								<div class="category">BEST VISUAL EFFECTS</div>
								<div class="name">Scott Kersavage</div>
							</div>
							
						</div>
						<div class="right">
							

							<div>
								<div class="category">BEST ORIGINAL SCORE</div>
								<div class="name">Henry Jackman</div>
							</div>
							<div>
								<div class="category">BEST ORIGINAL SONGS</div>
								
								<div class="name">“A Place Called Slaughter Race”</div>
								<div class="subline">Music By </div>
								<div class="name">Alan Menken</div>
								<div class="subline">Lyrics by </div>
								<div class="name">Phil Johnston<br/> Tom M<span class="lowercase">ac</span>Dougall</div>
								<div class="subline">Performed by </div>
								<div class="name">Sarah Silverman<br/> Gal Gadot<br/> <span class="and">and</span> Cast</div>
								<div class="category"></div>
									<div class="name">“Zero”</div>
								<div class="subline">Written and Performed by </div>
								<div class="name">Imagine Dragons</div>
							</div>
							
						</div>

					</div>
			</div>
		</div>
		<div class="synopsis">
			
			<div class="content clear">
				<p>Ralph and best friend Vanellope leave the comforts of the arcade behind and must risk it all in an attempt to save her game. Their quest takes them to the uncharted, expansive and thrilling world of the internet—a world Vanellope wholeheartedly embraces. So much so that Ralph worries he may lose the only friend he’s ever had. In way over their heads, the two rely on the citizens of the internet—the Netizens—to help navigate their way, including Yesss (voice of Taraji P. Henson), the heart and soul of the trend-making site “BuzzzTube,” and Shank (voice of Gal Gadot), a tough-as-nails driver from a gritty online racing game. Directed by Rich Moore (“Zootopia,” “Wreck-It Ralph”) and Phil Johnston (co-writer “Wreck-It Ralph,” “Zootopia,” writer, “Cedar Rapids”), and produced by Clark Spencer (“Zootopia,” “Wreck-It Ralph,” “Bolt,” “Lilo & Stitch”), RALPH BREAKS THE INTERNET hits theaters on Nov. 21, 2018.</p>

			</div>
		</div>
		<div class="screenings">
			<div class="guild-members-co">
				ATTENTION<br/> AMPAS AND GUILD MEMBERS<br/>
				CLICK HERE
				<div>
				YOUR MEMBERSHIP CARD<br/> WILL ADMIT YOU TO<br/> THE FOLLOWING THEATERS
				</div>
			</div>
			<div class="pop-up">
				<div class="pu-wrapper">
					<div class="pop-up-close">+</div>
					<div class="inside">
						<h3>GUILD MEMBERS</h3>
						<p>You may use your membership card to admit you and a guest to the following theatres in your city, subject to seating capacity/availability.
						<br/><br/>
						Cinemark does not allow a guest.</p>
						<ul>
							<li><strong>ArcLight/Pacific</strong> will admit AMPAS, HFPA, PGA (Monday–Thursday only/no holidays).
							</li>
							<li><strong>AMC</strong> will admit AMPAS, ACE, ADG, ASC, BAFTA, CAS, HFPA, MPEG, MPSE, PGA, BFCA, VES (Monday–Thursday only/no holidays- Los Angeles and NY Only).</li>

							<li><strong>Regal</strong> will admit AMPAS, PGA (Monday–Thursday only, October 1st - Awards).</li>

							<li><strong>Laemmle</strong> will admit AMPAS (Monday–Thursday only).</li>

							<li><strong>Cinemark</strong> will admit AMPAS, WGA, PGA (Monday–Thursday).</li>
						</ul>
						<div class="restrictions">
							<h5>Restrictions</h5>
								<ul>
								<li>Guild passes will be issued Monday – Thursday only on all titles.</li>
								<li>All guild passes are limited to available seating. If capacity is expected to exceed 80% of auditorium seats, tickets will not be issued.</li>
								<li>Tickets may not be issued more than 2 hours before showtime (1 hour for Dine-In and Recliner seating locations).</li>
								<li>Tickets will be issued to the guild member plus one guest for all guilds.</li>
								<li>Guild passes are only approved for the Los Angeles and New York markets.
								AMC reserves the right to modify, amend, suspend, or terminate these benefits at any time.</li>
								<li>All requests must be approved by an AMC VP of Studio Partnerships prior to advertising. AMC’s restrictions should be included in all print advertising.</li>
								</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="disclaimer">You must be an invited member of a voting organization to attend For Your Consideration screenings. <br/>Your membership card is required for entry.</div>
				<div class="cities-list"></div>
				<div class="screenings-holder">

				</div>
			</div>
		</div>
		
		<div class="score">
			
			<div class="content">
				<div class="category">BEST ORIGINAL SCORE</div>
				<div class="name">HENRY JACKMAN</div>

				<div class="audio-player">
						<div class="song-title">Friendship Montage</div>
					
						<audio controls controlsList="nodownload" autobuffer>
							<source src="" type="audio/mp3"/>
						</audio>
						<div class="track-list">
							<div class="col left"></div>
							<div class="col right"></div>
							
						</div>
				</div>
			</div>
		</div>
		<div class="song">
			<div class="content">
				<div class="audio-player">
					<div class="song-title">A Place Called Slaugher Race</div>
					<div class="song-credit">Sarah Silverman, Gal Gadot, and Cast</div>
					
				</div>
				<div class="song-div">
					<div class="cover">
						<img src="/img/ralph/soundtrack_cover.jpg">
					</div>
					<div>
						<div class="category">BEST ORIGINAL SONG</div>
						<div class="name">“A PLACE CALLED<br/> SLAUGHTER RACE”</div>
						<div class="subline">MUSIC BY</div>
						<div class="sub-name">Alan Menken</div>
						<div class="subline">LYRICS BY</div>
						<div class="sub-name">Phil Johnston, Tom MacDougall</div>
						<div class="subline">PERFORMED BY</div>
						<div class="sub-name">Sarah Silverman, Gal Gadot, and Cast</div>
					<!-- 	<div class="play" data-id="0">Play ”A Place Called<br/> Slaughter Race”</div> -->
					</div>
				</div>
				
			</div>
		</div>
		<div class="accolades"">
			<div class="content">
				<img src="/img/ralph/awards/ralph-awards2.png"/>
			</div>
		</div>
	</div>
</div>