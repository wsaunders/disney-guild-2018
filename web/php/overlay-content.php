			<div class="newman-bio">
				<div class="scrollable">
					<p>With songs that run the gamut from heartbreaking to satirical and a host of unforgettable film scores, RANDY NEWMAN (Original Score Composed & Conducted by) has used his many talents to create musical masterpieces widely recognized by generations of audiences.</p>
					<p>After starting his songwriting career as a teenager, Newman launched into recording as a singer and pianist in 1968 with his self-titled album Randy Newman. Throughout the 1970s he released several other acclaimed albums such as: 12 Songs, Sail Away, and Good Old Boys. In addition to his solo recordings and regular international touring, Newman began composing and scoring for films in the 1980s. The list of movies he has worked on includes “The Natural, Awakenings,” “Ragtime,” all three “Toy Story” pictures, “Seabiscuit,” “James and the Giant Peach,” “A Bug’s Life,” “Cars” and “Monsters University.” </p>
					<p>The highly praised 2008 Nonesuch Records release Harps and Angels was Newman’s first album of new material since 1999. </p>
					<p>In 2011, Nonesuch released a live CD and DVD recorded at London’s intimate LSO St. Luke’s, an 18th-century Anglican church that has been restored by the London Symphony Orchestra, where he was accompanied by the BBC Concert Orchestra, conducted by Robert Ziegler.</p>
					<p>The Randy Newman Songbook Vol. 3, which is the third in a series of new solo piano/vocal recordings of his songs from his six-decade career, was released in 2016, along with a boxed set of all three Songbook albums.</p>
					<p>Additionally, Newman released “Putin,” a satirical song about the infamous Russian leader, in the fall of 2016. The song is from his forthcoming album of new material, due from Nonesuch in spring 2017.</p>
					<p>Newman’s many honors include six Grammys®, three Emmys®, and two Academy Awards®, as well as a star on the Hollywood Walk of Fame. He was inducted into the Songwriters Hall of Fame in 2002 and the Rock and Roll Hall of Fame in 2013—the same year he was given an Ivor Novello PRS for Music Special International Award. Newman was presented with a PEN New England Song Lyrics of Literary Excellence Award in June 2014.</p>
				</div>
			</div>
			<div class="back-to-life">
				<div class="video-container">
					<h3>“BACK TO LIFE” FEATURETTE</h3>
					<video preload="auto" controls poster="/img/batb/back-to-life-poster.jpg">
						<source src="/media/video/back-to-life.mp4" type="video/mp4"/>
					</video>
					<div class="video-play-btn"></div>
				</div>
			</div>
			<div class="batb-synopsis">
				<p>The story and characters audiences know and love come to spectacular life in Disney’s live-action adaptation “Beauty and the Beast,” a stunning, cinematic event celebrating one of the most beloved tales ever told. “Beauty and the Beast” is the fantastic journey of Belle, a bright, beautiful and independent young woman who is taken prisoner by a Beast in his castle. Despite her fears, she befriends the castle’s enchanted staff and learns to look beyond the Beast’s hideous exterior and realize the kind heart of the true Prince within. The film stars: Emma Watson as Belle; Dan Stevens as the Beast; Luke Evans as Gaston, the handsome, but shallow villager who woos Belle; Kevin Kline as Maurice, Belle’s father; Josh Gad as LeFou, Gaston’s long-suffering aide-de-camp; Ewan McGregor as Lumière, the candelabra; Stanley Tucci as Maestro Cadenza, the harpsichord; Audra McDonald as Madame de Garderobe, the wardrobe; Gugu Mbatha-Raw as Plumette, the feather duster; Hattie Morahan as the Enchantress; and Nathan Mack as Chip, the teacup; with Ian McKellen as Cogsworth, the mantel clock; and Emma Thompson as the teapot, Mrs. Potts.<br/><br/>
				Directed by Bill Condon based on the 1991 animated film, “Beauty and the Beast,” the screenplay is written by Stephen Chbosky and Evan Spiliotopoulos and produced by Mandeville Films’ David Hoberman, p.g.a. and Todd Lieberman, p.g.a. with Jeffrey Silver, Thomas Schumacher and Don Hahn serving as executive producers. Alan Menken, who won two Academy Awards® (Best Original Score and Best Song) for the animated film, provides the score, which includes new recordings of the original songs written by Menken and Howard Ashman, as well as three new songs written by Menken and Tim Rice.
				</p>
			</div>
			<div class="women-of">
				<div class="scrollable">
					<img src="/img/batb/makingof/women-photo.jpg" alt="Women of Beauty and the Beast">
					<h3>THE WOMEN OF 'BEAUTY AND THE BEAST’</h3>
					<p>In a fitting tribute to the story’s notion of female empowerment, all the department heads on the design team – many of whom are frequent collaborators of director Bill Condon – are female, as was the film editor and the casting director, and all are at the top of their game creatively. Greenwood and Spencer have worked together for almost two decades and both have worked with Durran and Shircore before, and each was passionate about the project, sharing ideas and information between the other department heads, which helped to create seamless integration.</p>
					<p>As production designer, Greenwood is essentially in charge of every visual aspect of the film, and with “Beauty and the Beast” she was striving for a timeless, European feel in the tradition of the great Hollywood romances. The story is set in a specific time and place – mid-18th century France – as opposed to an undated alternate fairy tale universe, and while each department’s work was influenced, in part, by the 1991 animated film, the sets, props, costumes and hair and make-up were authentic to 18th century life in France. Since the story is, in fact, a fairy tale, there was some freedom to visually interpret the period so as to provide a somewhat original look.</p>
					<p>“The goal isn’t to have the audience think, ‘That looks just like the castle in the animated film,’” says Greenwood. “Instead, you want the audience to feel that this is, in fact, the Beast’s castle, because every detail faithfully supports the story they know and love.”</p>
					<p>Over 1,000 crew members worked around the clock to build and decorate all the mammoth sets, providing an incredible amount of hand-detailed artistry. According to Emma Watson, “I’ve worked on films before where the craftsmanship has been amazing, but this film has been really special because they’ve taken something which is so well known and loved and somehow managed to keep what we know and love but also expand on it and give it more detail and more depth and more layers. Everyone felt there was so much more to explore, and fortunately that was something we could do with the current technology and craftsmanship and means of storytelling that we didn’t have before.” </p>
					<p>The fictional town of Villeneuve, the village where Belle and her father live, was built on the backlot at Shepperton. For the production’s largest set (measuring 28,787 square feet), Greenwood and her team drew inspiration from the village of Conques in Southern France. Included in the town, which was named after the author of the original “Beauty and the Beast” story, Gabrielle-Suzanne Barbot de Villeneuve, is Belle’s cottage, a school house, a dress shop, a village tavern, a church and the village square. </p>
					<p>For the film’s epic opening number, “Belle,” which takes place in Villeneuve, more than 150 extras, hundreds of animals, 28 wagons and countless props and set decorations were used, each with an incredible amount of detail. </p>
					<p>The art department spent months researching period architecture and interior design to create the look of the Prince/Beast’s castle. In the end, it was a combination of different architectural styles, but the majority was French Rococo, a style prevalent in 1740s France used in the design of such notable structures as the Palace of Versailles. “Rococo was a French design style where the motif was quite extreme,” says Greenwood. “It was a very short-lived design theme because it was so intense and excessive and very expensive, but it did have a big impact on the overall visual look of our film.”</p>
					<p>One significant difference between this castle and the castle from the animated film is its evolving look. Greenwood explains, “The castle in the animated film does not change over the course of the story, but because we’re working within a live-action format we were able to show the castle reacting to the effects of the spell as time goes by. With Rococo, everything is very exuberant, but also very organic, and what we wanted to convey in our designs was it slowly growing and stretching – post enchantment – which is reflected in the castle’s frost, topiaries, architecture and plaster moldings.”</p>
					<p>The castle’s ballroom is another massive set. The floor is made from 12,000 square feet of faux marble and its design is based on a pattern found on the ceiling of the Benedictine Abbey in the Czech Republic.</p>
					<p>Also included are ten glass chandeliers – each measuring 14 feet x 7 feet – which are based on actual chandeliers from Versailles which were then frosted, covered in fabric and candlelit.</p>
					<p>Belle’s bedroom, like the ballroom, is located in the benevolent enchantment area of the castle and is designed to appeal to every little girl as the ideal fairy tale bedroom. The west wing, where the Beast often retreats, is the epicenter of the enchantment and is designed in Italian baroque, which is more sinister and dark in appearance.</p>
					<p>The castle’s library is based on the design of a celebrated library in Portugal and is a key setting and relevant to an important theme in the story: the thirst for knowledge and the vital role books play in feeding the imagination. The floor is made from approximately 2,000 square feet of faux marble and features thousands of books which were created especially for the production. </p>
					<p>The enchanted forest that surrounds the Beast’s castle was built on stage H, the largest stage at Shepperton, measuring 9,600 square feet. The forest, which took 15 weeks to complete, includes real trees, hedges, a frozen lake, a set of 29-foot high ice gates and approximately 20,000 icicles.</p>
					<p>“All the sets were truly magical and incredibly lavish,” says visual effects producer Steve Gaub (“Unbroken,” “TRON: Legacy”), “And Sarah and her team did an amazing job. Everything felt very old Hollywood, which is appropriate because were looking to create something classic which could sit beside the original.”</p>
					<p>Designing costumes befitting a fairy tale world is a prodigious undertaking, but it is one Jacqueline Durran took in stride. Her department, which was comprised of embroiders, milliners, jewelers, painters and textile artists, began working three months prior to principal photography. This was due, in part, to the challenge they gave themselves to design and create ethical and sustainable costumes made from fair-trade fabrics (meaning the use of organic materials from suppliers that pay their employees a fair wage and are considerate of the environment), which they achieved. And, working in tandem with Eco Age and the Green Carpet Challenge, the department used natural and low impact dyes (carefully disposing of any waste water) and printed with traditional wood blocks.</p>
					<p>Durran designed everything from the peasant costumes for all the villagers to the elaborate ball gowns worn by 35 debutantes at the Prince’s ball, but her biggest hurdle was creating the dress Belle wears when she dances with the Beast in the castle ballroom. Because of the iconic association with the yellow dress and the character, the design process was lengthy, involving numerous discussions as to its look, color and the material used.</p>
					<p>“The dress was always going to be yellow in our film as an homage to the animation,” says Durran. “What we tried to do was re-interpret it and flesh it out a bit by adding more texture and making it feel like a real living costume.” In the end, the dress was created from multiple layers of feather light satin organza dyed yellow (180 feet in total), which was cut broadly in a circular shape and required 3,000 feet of thread. </p>
					<p>The top two layers were printed with gold leaf filigree in a pattern matching the ballroom’s Rococo floor and accentuated with 2,160 Swarovski crystals. In the story, Garderobe, the wardrobe, takes gold gilding from the ceiling of Belle’s bedroom and sprinkles it onto the dress. The dress, which took over 238 hours to make and of which multiple copies were made, did not require a corset or cage so as to give Emma Watson a greater amount of movement, as this Belle is more active than the Belle from the animated film. </p>
					<p>“It was definitely an interesting challenge,” says Watson, “The dress itself is so iconic because it is part of that romantic scene in the story. The dress went through a lot of different iterations, but, in the end, we decided the most important thing was that the dress dance beautifully. We wanted it to feel like it could float, like it could fly.”
					<p>Durran agrees, saying, “We actually took this into consideration when designing all of Belle’s costumes. We didn’t want her to be a delicate princess but an active heroine, so the blue dress and apron she wears at the beginning of the film was designed with pockets where she could place a book and to be worn with bloomers and a bodice.”</p>
					<p>This Belle is seen working with and riding her horse, Philippe, and wears boots in those scenes (as opposed to delicate feminine shoes). “We really wanted to expand Belle’s persona in the film and wanted to make sure she came across as a genuine equestrian and horsewoman,” says Watson, “So we made sure she had proper footwear and hiked up one side of her skirt so she could ride western style and that it looked easy for her.”</p>
					<p>The design on the gown Belle wears at the end of the film, once the spell has been lifted, is a print taken from an original 18th century apron that Durran bought when she was a student. The design was hand painted on a canvas and enlarged and printed digitally. “The expectations for all of Belle’s costumes were quite high,” says Durran, “But we ended up with some beautiful dresses that reference the animated film but are still unique to this one.”</p>
					<p>“One of the wonderful things about working with Jacqueline is that she is so incredibly collaborative,” says Watson. “I was just blown away by how much input she wanted from me…she really wanted to understand how I perceived the character inside and out. It was such a special experience for me as an actress, and such a great way to build and understand a character through that process.”</p>
					<p>For the Prince’s costume worn in the opening sequence of the film, Durran created a coat and waistcoat embellished with thousands of Swarovski crystals, which was then scanned by the visual effects department and applied to the computer-generated Beast.
					<p>“Jackie is a goddess,” says producer David Hoberman. “She had a really tough job, not only because of the sheer volume of costumes in this movie, but because of the iconic wardrobe from the animated film. </p>
					<p>She wanted to take into consideration and be somewhat faithful to the costumes worn by all the animated characters, but wanted to create something original as well, and she managed to come up with something completely beautiful and completely her own.”</p>
				</div>
			</div>
			<div class="production-design">
				<div class="slider">
					<div class="photo viewing"><img src="/img/batb/makingof/production-design1.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/production-design2.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/production-design3.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/production-design4.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/production-design5.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/production-design6.jpg"></div>
				</div>
				<div class="left-arrow arrow"></div>
				<div class="right-arrow arrow"></div>
			</div>
			<div class="costume-design">
				<div class="slider">
					<div class="photo viewing"><img src="/img/batb/makingof/costume-design1.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/costume-design2.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/costume-design3.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/costume-design4.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/costume-design5.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/costume-design6.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/costume-design7.jpg"></div>
				</div>
				<div class="left-arrow arrow"></div>
				<div class="right-arrow arrow"></div>
			</div>
			<div class="makeup">
				<div class="slider">
					<div class="photo viewing"><img src="/img/batb/makingof/makeup1.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup2.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup3.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup4.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup5.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup6.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup7.jpg"></div>
					<div class="photo"><img src="/img/batb/makingof/makeup8.jpg"></div>
				</div>
				<div class="left-arrow arrow"></div>
				<div class="right-arrow arrow"></div>
			</div>
			<div class="dressing-the-part">

				<div class="video-container">
				<h3>“DRESSING THE PART” FEATURETTE</h3>
					<video controls="true" poster="/img/batb/dressing-the-part-poster.jpg">
						<source src="/media/video/dressing-the-part.mp4" type="video/mp4"/>
					</video>
					<div class="video-play-btn"></div>
				</div>
			</div>