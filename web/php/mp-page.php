<div class="page-header">
	<div class="logo limiter"><img src="/img/mp/mp_tt.jpg"></div>
	<div class="nav">
		<div class="nav-link">CONSIDER</div>
		<div class="nav-link">SYNOPSIS</div>
		<div class="nav-link">SCREENINGS</div>
		<div class="nav-link">SCORE</div>
		<div class="nav-link">SONGS</div>
		<div class="nav-link">VIDEOS</div>
		<div class="nav-link">ACCOLADES</div>
	</div>
</div>
<div class="film-strip">
	<div class="slider clear">
		

	</div>
	<div class="arrow" id="left-arrow"></div>
	<div class="arrow" id="right-arrow"></div>
</div>
<div class="subpages">
	<div class="limiter">
		<div class="consider">
			
			<div class="content clear">
				<h3>FOR YOUR CONSIDERATION</h3>
					<div class="inline-clear">
						<div class="left">
							<div class="category">BEST PICTURE</div>
							<div class="name">
								John D<span class="lowercase">e</span>Luca, <span class="lowercase">p.g.a.</span><br/>
								Rob Marshall,  <span class="lowercase">p.g.a.</span><br/>
								Marc Platt,  <span class="lowercase">p.g.a.</span>
							</div>
							<div class="category">BEST DIRECTOR</div>
							<div class="name">Rob Marshall</div>

							<div class="category">BEST ADAPTED SCREENPLAY</div>
							<div class="name">David Magee</div>
							<div class="subline">SCREEN STORY BY</div>
							<div class="name">DAVID MAGEE &<br/>
								ROB MARSHALL &<br/>
								JOHN D<span class="lowercase">e</span>LUCA</div>

							<div class="category">BEST ACTRESS</div>
							<div class="name">Emily Blunt</div>

							<div class="category">BEST ACTOR</div>
							<div class="name">Lin-Manuel Miranda</div>

							

							
						</div>
						<div class="left">
							<div class="category">BEST SUPPORTING ACTOR</div>
							<div class="name">
								Ben Whishaw<br/>
								Dick Van Dyke<br/>
								Colin Firth
							</div>
							<div class="category">BEST SUPPORTING ACTRESS</div>
							<div class="name">
								Emily Mortimer<br/>
								Meryl Streep<br/>
								Angela Lansbury
							</div>
							<div class="category">BEST CINEMATOGRAPHY</div>
							<div class="name">Dion Beebe, <span class="guild">ACS, ASC</span></div>
								

							<div class="category">BEST FILM EDITING</div>
							<div class="name">Wyatt Smith, <span class="guild">ACE</span></div>

							<div class="category">BEST PRODUCTION DESIGN</div>
							<div class="subline">Production Designer</div>
							<div class="name">John Myhre</div>
							<div class="subline">Set Decorator</div>
							<div class="name">Gordon Sim</div>
						

							
						</div>
						<div class="left">
							<div class="category">BEST COSTUME DESIGN</div>
							<div class="name">Sandy Powell</div>
							<div class="category">BEST MAKEUP & HAIRSTYLING</div>
							<div class="name">Peter Swords King</div>
							<div class="category">BEST SOUND MIXING</div>
							<div class="subline">Re-Recording Mixers</div>
							<div class="name">
								Mike Prestwood Smith<br/>
								Michael Keller
							</div>
							<div class="subline">Production Sound Mixer</div>
							<div class="name">Simon Hayes, <span class="guild">AMPS, CAS</span></div>
						
							<div class="category">BEST SOUND EDITING</div>
							<div class="subline">Supervising Sound Editors</div>
							<div class="name">
								Renée Tondelli<br/>
								Eugene Gearty
							</div>
							<div class="category">BEST VISUAL EFFECTS</div>
							<div class="name">
								Matt Johnson<br/>
								Steve Warner<br/>
								Jim Capobianco<br/>
								Kyle M<span class="lowercase">c</span>Culloch
							</div>
						</div>
						<div class="right">
							<div class="category">BEST ORIGINAL SCORE</div>
							<div class="name">Marc Shaiman</div>

							<div class="category">BEST ORIGINAL SONGS</div>
							<div class="name">“THE PLACE WHERE LOST THINGS GO”</div>
							<div class="subline">MUSIC BY</div>
							<div class="name">MARC SHAIMAN</div>
							<div class="subline">LYRICS BY</div>
							<div class="name">
								SCOTT WITTMAN AND<br/>
								MARC SHAIMAN
							</div>
							<div class="name" style="margin-top: 14px">“Trip a Little<br/>Light Fantastic”</div>
							<div class="subline">Music by </div>
							<div class="name">Marc Shaiman</div>
							<div class="subline">Lyrics by </div>
							<div class="name">Scott Wittman AND<br/>Marc Shaiman</div>
							<div class="category"></div>
							
						</div>
					</div>
			</div>
		</div>
		<div class="synopsis">
			
			<div class="content clear">
				<p>In Disney’s MARY POPPINS RETURNS, an all-new original musical and sequel, Mary Poppins is back to help the next generation of the Banks family find the joy and wonder missing in their lives following a personal loss. Emily Blunt (“A Quiet Place,” “The Girl on the Train”) stars as the practically perfect nanny with unique magical skills who can turn any ordinary task into an unforgettable, fantastic adventure, and Lin-Manuel Miranda (“Hamilton,” “Moana”) plays her friend Jack, an optimistic street lamplighter who helps bring light—and life—to the streets of London.</p>
 
				<p>MARY POPPINS RETURNS is directed by Rob Marshall (“Into the Woods,” “Chicago”). The screenplay is by David Magee (“Life of Pi”) and the screen story is by Magee & Rob Marshall & John DeLuca (“Into the Woods”) based upon the Mary Poppins stories by PL Travers. The producers are John DeLuca, p.g.a., Rob Marshall, p.g.a. and Marc Platt, p.g.a. (“La La Land”) with Callum McDougall serving as executive producer. The music score is by Marc Shaiman (“Hairspray”) and the film features all-new original songs with music by Shaiman and lyrics by Scott Wittman (“Smash”) and Shaiman. The film also stars Ben Whishaw (“Spectre”) as Michael Banks; Emily Mortimer (“Hugo”) as Jane Banks; Julie Walters (“Harry Potter” films) as the Banks’ housekeeper Ellen; Pixie Davies, Nathanael Saleh and introducing Joel Dawson as the Banks’ children, with Colin Firth (“The King’s Speech”) as Fidelity Fiduciary Bank’s William Weatherall Wilkins; and Meryl Streep (“Florence Foster Jenkins”) as Mary’s eccentric cousin, Topsy. Angela Lansbury appears as the Balloon Lady, a treasured character from the PL Travers books and Dick Van Dyke is Mr. Dawes, Jr., the retired chairman of the bank now run by Firth’s character.
				</p>

			</div>
		</div>
		<div class="screenings">
			<div class="guild-members-co">
				ATTENTION<br/> AMPAS AND GUILD MEMBERS<br/>
				CLICK HERE
				<div>
				YOUR MEMBERSHIP CARD<br/> WILL ADMIT YOU TO<br/> THE FOLLOWING THEATERS
				</div>
			</div>
			<div class="pop-up">
				<div class="pu-wrapper">
					<div class="pop-up-close">+</div>
					<div class="inside">
						<h3>GUILD MEMBERS</h3>
						<p>You may use your membership card to admit you and a guest to the following theatres in your city, subject to seating capacity/availability.
						<br/><br/>
						Cinemark does not allow a guest.</p>
						<ul>
							<li><strong>ArcLight/Pacific</strong> will admit AMPAS,, DGA, and WGA + GUEST (Mon - Thurs only/no holidays).
							</li>
							<li><strong>AMC</strong> will admit AMPAS, ACE, ADG, ASC, BAFTA, CAS, DGA, HFPA, MPEG, MPSE, PGA, WGA, BFCS, CDG, VES (Mon–Thurs only/no holidays - Los Angeles and NY Only).</li>

							<li><strong>Regal</strong> will admit AMPAS, DGA, PGA, WGA (Mon - Thurs only, October 1st - Awards).</li>

							<li><strong>Laemmle</strong> will admit AMPAS, DGA, and WGA + GUEST (Mon - Thurs only).</li>

							<li><strong>Cinemark</strong> will admit AMPAS, WGA, DGA, PGA, and SAG NOM. (Mon - Thurs only).</li>
						</ul>
						<div class="restrictions">
							<h5>Restrictions</h5>
								<ul>
								<li>Guild passes will be issued Monday – Thursday only on all titles.</li>
								<li>All guild passes are limited to available seating. If capacity is expected to exceed 80% of auditorium seats, tickets will not be issued.</li>
								<li>Tickets may not be issued more than 2 hours before showtime (1 hour for Dine-In and Recliner seating locations).</li>
								<li>Tickets will be issued to the guild member plus one guest for all guilds.</li>
								<li>Guild passes are only approved for the Los Angeles and New York markets.</li>
								<li>AMC reserves the right to modify, amend, suspend, or terminate these benefits at any time.</li>
								<li>All requests must be approved by an AMC VP of Studio Partnerships prior to advertising. AMC’s restrictions should be included in all print advertising.</li>
								</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="disclaimer">You must be an invited member of a voting organization to attend For Your Consideration screenings. <br/>Your membership card is required for entry.</div>
				<div class="cities-list"></div>
				<div class="screenings-holder">

				</div>
			</div>
		</div>
		<div class="score">
			<div class="content">
			
				<div class="category">BEST ORIGINAL SCORE</div>
				<div class="name">MARC SHAIMAN</div>
				<a href="https://disneystudiosawards.s3.amazonaws.com/mp3/02_Overture.mp3" class="overture" data-title="Overture">Click here to listen to Marc Shaiman’s Overture</a>
				<br/>
				<div class="audio-player">
					<div class="song-title">(Underneath the) Lovely London Sky</div>
				
					<audio controls controlsList="nodownload" autobuffer>
						<source src="" type="audio/mp3"/>
					</audio>
				

					<div class="track-list">
						<div class="col left"></div>
						<div class="col right"></div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="songs">
			
			<div class="content">
			
				
				<div class="song-div video">
					<div class="category">BEST ORIGINAL SONG</div>
					<div class="name">“THE PLACE WHERE LOST THINGS GO”</div>
					<div class="video-player">
						
					</div>
					
					<div class="subline">MUSIC BY</div>
					<div class="sub-name">Marc Shaiman</div>
					<div class="subline">LYRICS BY</div>
					<div class="sub-name">Scott Wittman and Marc Shaiman</div>
						
					
				</div>
			
				<div class="song-div video">
					<div class="category">BEST ORIGINAL SONG</div>
					<div class="name">“TRIP A LITTLE LIGHT FANTASTIC”</div>
					<div class="video-player">
						
					</div>
				
					<div class="subline">MUSIC BY</div>
					<div class="sub-name">Marc Shaiman</div>
					<div class="subline">LYRICS BY</div>
					<div class="sub-name">Scott Wittman and Marc Shaiman</div>
						
					
				</div>
			</div>
		</div>
		<div class="videos">
			<div class="prime-quote">
				<div class="press-logo"><img src="/img/press/chicago-sun-times.png"></div>
				<div class="name">RICHARD ROPER</div>
				<div class="quote">
					<span>“WHAT A MAGNIFICENT AND JOY-INDUCING PRESENT.</span><br/>BIG OF HEART, LAVISHLY STAGED, BEAUITFULLY PHOTOGRAPHED<br/> AND BRIMMING WITH SHOW-STOPPING MUSICAL NUMBERS.”
				</div>
				
			</div>
			<div class="content">
				<h3>WATCH THE Q&A WITH MODERATOR JAMES CORDEN AND THE CAST & CREW</h3>
				<div class="video-player">
					
				</div>
				<br/><br/><br/>
				<h3>GO BEHIND THE SCENES: MAKING THE IMPOSSIBLE POSSIBLE</h3>
				<div class="video-player">
					
				</div>
			</div>
		</div>
		<div class="accolades">
			<div class="content">
				<img src="/img/mp/awards/mpr-awards2.png">
			</div>
		</div>
	</div>
</div>