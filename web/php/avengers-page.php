<div class="page-header">
	<div class="logo limiter"><img src="/img/avengers/avengers_tt.jpg"></div>
	<div class="nav">
		<div class="nav-link">CONSIDER</div>
		<div class="nav-link">SYNOPSIS</div>
		<div class="nav-link">SCREENINGS</div>
		<div class="nav-link">PRESS</div>
		<div class="nav-link">ACCOLADES</div>
	</div>
</div>
<div class="film-strip">
	<div class="slider clear">
		
		

	</div>
	<div class="arrow" id="left-arrow"></div>
	<div class="arrow" id="right-arrow"></div>
</div>
<div class="subpages">
	<div class="limiter">
		<div class="consider">
			
			<div class="content clear">
				<h3>FOR YOUR CONSIDERATION</h3>
				<div class="inline-clear">
					<div class="left">
						<div class="category">BEST DIRECTOR</div>
						<div class="name">Anthony and Joe Russo</div>

						<div class="category">BEST ADAPTED SCREENPLAY</div>
						<div class="name">Christopher Markus &<br/>
						Stephen M<span class="lowercase">c</span>Feely</div>

						<div class="category">BEST FILM EDITING</div>
						<div class="name">Jeffrey Ford, <span class="guild">ACE</span><br/>
						Matthew Schmidt</div>


						<div class="category">BEST CINEMATOPGRAPHY</div>
						<div class="name">Trent Opaloch</div>
					</div>
					<div class="left">

						<div class="category">BEST PRODUCTION DESIGN</div>
						<div class="name">Charles Wood</div>
						<div class="subline">Set Decorator</div>
						<div class="name">Leslie A. Pope</div>

						<div class="category">BEST COSTUME DESIGN</div>
						<div class="name">Judianna Makovsky</div>
					
						<div class="category">BEST MAKEUP & HAIRSTYLING</div>
						<div class="name">Janine Thompson<br/>
						John Blake<br/>
						Brian Sipe</div>
					
						<div class="category">BEST VISUAL EFFECTS</div>
						<div class="name">
							Dan Deleeuw<br/>
							Kelly Port<br/>
							Russell Earl<br/>
							Dan Sudick
						</div>
					</div>
					<div class="right">
						<div class="category">BEST SOUND MIXING</div>
						<div class="subline">Re-Recording Mixers</div>
						<div class="name">Tom Johnson<br/>
						Juan Peralta</div>
						<div class="subline">Production Sound Mixer</div>
						<div class="name">John Pritchett, <span class="guild">CAS</span></div>

						<div class="category">BEST SOUND EDITING</div>
						<div class="subline">Supervising Sound Editors</div>
						<div class="name">Shannon Mills<br/>
						Daniel Laurie</div>

						<div class="category">BEST ORIGINAL SCORE</div>
						<div class="name">Alan Silvestri</div>
					</div>					
					
				</div>
			</div>
		</div>
		<div class="synopsis">
			
			<div class="content clear">
				An unprecedented cinematic journey ten years in the making and spanning the entire Marvel Cinematic Universe, Marvel Studios’ AVENGERS: INFINITY WAR brings to the screen the ultimate, deadliest showdown of all time. The Avengers and their Super Hero allies must be willing to sacrifice all in an attempt to defeat the powerful Thanos before his blitz of devastation and ruin puts an end to the universe. 
			</div>
		</div>
		<div class="screenings">
			
			<div class="content">
				<div class="disclaimer">You must be an invited member of a voting organization to attend For Your Consideration screenings. <br/>Your membership card is required for entry.</div>
				<div class="cities-list"></div>
				<div class="screenings-holder">

				</div>
			</div>
		</div>
		<div class="press">
			<div class="content">
				<div class="quote special">
					<div class="press-image" style="height:42px;"><img src="/img/press/THR-color.png"></div>
					<div class="name"></div>
					<div class="quote-text"><span>“How ‘Avengers: Infinity War’ VFX Teams brought Josh Brolin’s Thanos to Life”<br/></span>
						<a target="_blank" href="https://www.hollywoodreporter.com/behind-screen/how-vfx-teams-brought-josh-brolins-thanos-life-infinity-war-1178231">READ MORE</a>
					</div>
				</div>
			</div>
		</div>
		<div class="accolades">
			<div class="content">
				<img src="/img/avengers/awards/avengers-awards3.png"/>
			</div>
		</div>
		
	</div>
</div>