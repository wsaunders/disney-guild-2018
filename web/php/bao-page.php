<div class="page-header">
	<div class="logo limiter"><img src="/img/bao/bao_tt.png"></div>
	<div class="nav">
		<div class="nav-link">CONSIDER</div>
		<div class="nav-link">SYNOPSIS</div>
		<div class="nav-link">PRESS</div>
	</div>
</div>
<div class="film-strip">
	<div class="slider clear">
		
	</div>
	<div class="arrow" id="left-arrow"></div>
	<div class="arrow" id="right-arrow"></div>
</div>
<div class="subpages">
	<div class="limiter">
		<div class="consider">
			
			<div class="content clear">
				<img class="oscar" src="/img/bao/oscar.png">
				<h3>FOR YOUR CONSIDERATION</h3>
					<div>
						<div class="category">BEST ANIMATED SHORT</div>
						<div class="subline">DIRECTED BY</div>
						<div class="name">DOMEE SHI</div>
						<div class="subline">PRODUCED BY</div>
						<div class="name">BECKY NEIMAN-COBB</div>
					</div>
			</div>
		</div>
		<div class="synopsis">
			
			<div class="content clear">
				In Disney•Pixar’s BAO, an aging Chinese mom suffering from empty-nest syndrome gets another chance at motherhood when one of her dumplings springs to life as a lively, giggly dumpling boy. Mom excitedly welcomes this new bundle of joy into her life, but Dumpling starts growing up fast, and Mom must come to the bittersweet revelation that nothing stays cute and small forever. This short film from Pixar Animation Studios and director Domee Shi explores the ups and downs of the parent-child relationship through the colorful, rich and tasty lens of the Chinese immigrant community in Canada.

			</div>
		</div>
		<!-- <div class="screenings">
			
			<div class="content">
				<div class="disclaimer">You must be an invited member of a voting organization to attend For Your Consideration screenings. <br/>Your membership card is required for entry.</div>
				<div class="cities-list"></div>
				<div class="screenings-holder">

				</div>
			</div>
		</div> -->
		<div class="press">
			<div class="content">
				<div class="quote">
					"But more than just a Pixar appetizer, THE SHORT IS A WHIMSICAL LOVE LETTER to mothers<br/> as well as food. It just happens to be wrapped in a package so adorable you want to eat it."
					<div class="press-image"><img src="/img/press/LA_Times.png"></div>
					<div class="name" style="margin-top:5px">Tracy Brown</div>
				</div>
				<div class="quote">
					"I’d also argue that <em>BAO</em> IS THE BEST PIXAR SHORT IN SEVERAL YEARS<br/>because of the qualities that distinguish it from its predecessors: its relative scope and darkness,<br/> and its comparatively naturalistic look, as well as its cultural specificities."
					<div class="press-image slate"><img src="/img/press/Slate.png"></div>
					<div class="name">Inkoo Kang</div>
				</div>
				<div class="quote">
					“A quirky and gorgeous-looking slice-of-life short…”
					<div class="press-image"><img src="/img/press/IndieWire.jpg"></div>
					<div class="name">Bill Desowitz</div>
				</div>
				<div class="quote">
					"Warning: watching this may cause squeals of delight<br/> or maybe an involuntary ‘awww’ to escape from your mouth."
					<div class="press-image slash-film"><img src="/img/press/Slash_Film.png"></div>
					<div class="name">Ben Pearson</div>
				</div>
				<div class="quote">
					<span>“...INCREDIBLY BOLD"</span>
					<div class="press-image"><img src="/img/press/thrillist.png"></div>
					<div class="name">Esther Zuckerman</div>
				</div>
				<div class="quote">
					"I've never cried as hard and as many times during a film as I did watching <em>Bao</em>...<br/>A MUST-SEE EXPERIENCE."
					<div class="press-image"><img src="/img/press/Digital_Spy.png"></div>
					<div class="name" style="margin-top:5px">Jess Lee</div>
				</div>
			</div>
		</div>
	</div>
</div>