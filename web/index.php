<?php require_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();
?>
<!DOCTYPE html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W972SMX');</script> 
<!-- End Google Tag Manager -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=eE5evalxQk">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=eE5evalxQk">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=eE5evalxQk">
<link rel="manifest" href="/manifest.json?v=eE5evalxQk">
<link rel="mask-icon" href="/safari-pinned-tab.svg?v=eE5evalxQk" color="#5bbad5">
<link rel="shortcut icon" href="/favicon.ico?v=eE5evalxQk">
<meta name="theme-color" content="#ffffff">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta name="description" property="og:description" content="Walt Disney Studios 2018 For Your Consideration"/>
<meta property="og:image" content="https://waltdisneystudiosawards.com/img/preview-image.jpg"/>
<title>Disney Guild Awards 2018</title>

<link rel="stylesheet" type="text/css" href="/css/bcabc0c659-style.min.css">

</head>
<body>
<!-- 	<div id="middle" style="
    position: fixed;
    height: 100%;
    width: 2px;
    background: #000;
    z-index: 1000;
    left: 50%;
    transform: translateX(-50%);
"></div> -->
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W972SMX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="wrapper">

		<div class="fyc-banner clear">
			<!-- <a href="/" class="start"><img src="/img/ui/disney-logo.png" alt="Disney Logo"/></a> -->
			<span class="nowrap start"><t>F</t><t>O</t><t>R</t><t> </t><t>Y</t><t>O</t><t>U</t><t>R</t><t> </t><t>C</t><t>O</t><t>N</t><t>S</t><t>I</t><t>D</t><t>E</t><t>R</t><t>A</t><t>T</t><t>I</t><t>O</t><t>N</t></t></span>
		</div>
		<div class="title start">
			
		</div>
		<div class="limiter">
			<div class="film start" id="bp-box"></div>
			<div class="film start" id="incredibles-box"></div>
			<div class="film start" id="avengers-box"></div>
			<div class="film start" id="ralph-box"></div>
			<div class="film start" id="mp-box"></div>
			<div class="film start" id="bao-box"></div>
		</div>
	</div>
	<header>
		<div class="fyc-banner clear">
			<!--<a id="logo" href="/"><img src="/img/ui/disney-logo.svg" alt="Disney Logo"/></a> -->

			<span class="nowrap"><t>F</t><t>O</t><t>R</t><t> </t><t>Y</t><t>O</t><t>U</t><t>R</t><t> </t><t>C</t><t>O</t><t>N</t><t>S</t><t>I</t><t>D</t><t>E</t><t>R</t><t>A</t><t>T</t><t>I</t><t>O</t><t>N</t></t></span><div class="title">AWARDS 2018</div>
		</div>
		 <nav id="menu">
			<ul>
				<li class="avengers-btn">Avengers: Infinity War</li>
				<li class="bao-btn">BAO</li>
				<li class="bp-btn">Black Panther</li>
				<li class="incredibles-btn">Incredibles 2</li>
				<li class="mp-btn">Mary Poppins Returns</li>
				<li class="ralph-btn">Ralph Breaks The Internet</li>
			</ul>
		</nav> 
	</header>
	<div id="page-holder">
		<div class="back-btn mobile">< BACK</div>
		<div class="page" id="bp-page">
			<?php include('php/bp-page.php') ?>
		</div>
		<div class="page" id="incredibles-page">
			<?php include('php/incredibles-page.php') ?>
		</div>
		<div class="page" id="ralph-page">
			<?php include('php/ralph-page.php') ?>
		</div>
		<div class="page" id="avengers-page">
			<?php include('php/avengers-page.php') ?>
		</div>
		<div class="page" id="bao-page">
			<?php include('php/bao-page.php') ?>
		</div>
		<div class="page" id="mp-page">
			<?php include('php/mp-page.php') ?>
		</div>
	</div>
	<div id="rsvp-form">
		<div class="rsvp-close-btn">CLOSE<div class="x">+</div></div>
		
		<form>
			<div class="clear">
				<h2>RSVP</h2>
				<div class="screening-info">
					<div class="movie">Movie Title</div>
					<div>
						<div class="date">Movie Date</div> 
						<div class="theater"> Movie Location </div>
					</div>
				</div>
			</div>
			<div class="london">
				Please RSVP to the following email address: <a href="mailto:disney@premiercomms.com">disney@premiercomms.com</a><br/><br/>
				Or call <a href="tel:44 20 7292 7394">+44 20 7292 7394</a>
			</div>
			<div class="not-london">
				<input  name="screening_id" id="screening-id" value="" type="hidden" maxlength="15">
				<input  name="reception_id" id="reception-id" value="" type="hidden" maxlength="15">
				<input  name="referrer" value="ampas" type="hidden">
				<div class="input pushRight">
					<input type="text" id="first_name" name="first_name" placeholder="First Name" maxlength="15">
					<div class="error"></div>
				</div>
				<div class="input">
					<input type="text" id="last_name" name="last_name" placeholder="Last Name" maxlength="15">
					<div class="error"></div>
				</div>
				<br/>
				<div class="input fullWidth">
					<input type="email" id="email" name="email" placeholder="Email Address" maxlength="50">
					<div class="error"></div>
				</div>
				<div class="input pushRight">
					<div class="hasDropdown guests"><span>GUESTS</span>
						<div class="dropdown">
						</div>
					</div>
					<input type="hidden" id="guests" name="guests" value=""/>
				</div>
				
				<div class="input">
					<div class="guilds hasDropdown"><span>GUILD</span>
						<div class="dropdown">
							
						</div>
					</div>
					
					<div class="error"></div>
					<input type="hidden" id="guild" name="guild"/>
				</div>
				<div class="input fullWidth reception">
					<label>RSVP TO:</label>
					<input id="rsvp-type" type="hidden" name="rsvp-type" value="0">
					<div class="hasDropdown"><span>SCREENING AND RECEPTION</span>
						<div class="dropdown">
							<div class="selected value">SCREENING AND RECEPTION</div>
							<div class="value">SCREENING ONLY</div>
							<div class="value">RECEPTION ONLY</div>
						</div>
					</div>
				</div>
				<div class="submit-btn" id="rsvp-submit">CONFIRM RSVP</div>
			</div>
		</form>
		<div id="rsvp-message">

		</div>
	</div>
	<div id="footer">
		<div>Walt Disney Studios Motion Pictures Awards Office  <br class="mobile"><span class="desktop">•</span>  500 South Buena Vista Street Burbank, CA 91521-0222<br/>
		PHONE: 818.560.6212 or 855.560.6212 <br class="mobile"> <span class="desktop">•</span>  EMAIL: DISNEYAWARDSOFFICE@DISNEY.COM</div>
	
		<footer>
			<a href="http://corporate.disney.go.com/corporate/terms.html" target="_blank">Terms of Use</a> <span class="desktop">|</span> 
			<a target="" href="http://help.disney.com/articles/en_US/FAQ/Legal-Notices?ppLink=pp_wdig">Legal Notices</a> <span class="desktop">|</span> 
			<a href="http://corporate.disney.go.com/corporate/pp.html" target="_blank">Privacy Policy</a> <span class="desktop">|</span> 
			<a target="" href="https://disneyprivacycenter.com/notice-to-california-residents/">Your California Privacy Rights</a> <span class="desktop">|</span> 
			<a target="" href="https://disneyprivacycenter.com/kids-privacy-policy/english/">Children's Online Privacy Policy</a> <span class="desktop">|</span> 
			<a target="" href="http://preferences-mgr.truste.com/?type=disneycolor&amp;affiliateId=115">Interest-Based Ads</a>
			<!-- <br class="mobile"> -->
			<span class="copyright">&copy; 2018 <span>Disney</span>. All Rights Reserved.</span>
		</footer>
	</div>
	
		
	<!-- <img class="overlay-img" src="/img/overlay-1.jpg"/> -->
	<?PHP if($_SERVER['HTTP_HOST'] == 'www.waltdisneystudiosawards.com' || $_SERVER['HTTP_HOST']=='waltdisneystudiosawards.com') { ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108370396-1"></script>
	<script>
		if(location.host == 'www.waltdisneystudiosawards.com'|| location.host=='waltdisneystudiosawards.com'){
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-108370396-1');
		}
	</script>
	<?php } ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script>
	(function(){
		if (window.addtocalendar)
			if(typeof window.addtocalendar.start == "function") return;
			if (window.ifaddtocalendar == undefined) { 
   			window.ifaddtocalendar = 1;
            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
            s.type = 'text/javascript';s.charset = 'UTF-8';s.async = true;
            s.src = ('https:' == window.location.protocol ? 'https' : 'http')+'://addtocalendar.com/atc/1.5/atc.min.js';
            var h = d[g]('body')[0];h.appendChild(s); 
        }
	})();
	</script>
	<script>
		<?php 
		$isMobile = ($detect->isMobile() == 1 ? 'true':'false');
		$isTablet = ($detect->isTablet()==1 ? 'true':'false');
			echo 'var isMobile = '.$isMobile .';';
			echo 'var isTablet ='. $isTablet.';'; 
		?>
	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/plugins/CSSPlugin.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/easing/EasePack.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenLite.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TimelineLite.min.js"></script>

	<script src="/js/bcabc0c659-scripts.min.js"></script>
	<script>
// Let's wrap everything inside a function so variables are not defined as globals 
(function() {
    // This is gonna our percent buckets ( 10%-90% ) 
    var divisor = 10;
    // We're going to save our players status on this object. 
    var videos_status = {};
    // This is the funcion that is gonna handle the event sent by the player listeners 
    function eventHandler(e) {
        switch (e.type) {
            // This event type is sent everytime the player updated it's current time, 
            // We're using for the % of the video played. 
        case 'timeupdate':
            // Let's set the save the current player's video time in our status object 
            videos_status[e.target.id].current = Math.round(e.target.currentTime);
            // We just want to send the percent events once 
            var pct = Math.floor(100 * videos_status[e.target.id].current / e.target.duration);
            for (var j in videos_status[e.target.id]._progress_markers) {
                if (pct >= j && j > videos_status[e.target.id].greatest_marker) {
                    videos_status[e.target.id].greatest_marker = j;
                }
            }
            // current bucket hasn't been already sent to GA?, let's push it to GTM
            if (videos_status[e.target.id].greatest_marker && !videos_status[e.target.id]._progress_markers[videos_status[e.target.id].greatest_marker]) {
                videos_status[e.target.id]._progress_markers[videos_status[e.target.id].greatest_marker] = true;
                dataLayer.push({
                    'event': 'gaEvent',
                    'gaEventCategory': filmInfo[loaded].title,
                    'gaEventAction': 'Video Progress %' + videos_status[e.target.id].greatest_marker,
                    // We are using sanitizing the current video src string, and getting just the video name part
                    'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1])
                });
            }
            break;
            // This event is fired when user's click on the play button
        case 'play':
            dataLayer.push({
                'event': 'gaEvent',
                'gaEventCategory': filmInfo[loaded].title,
                'gaEventAction': 'Video Played',
                'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1])
            });
            break;
            // This event is fied when user's click on the pause button
        case 'pause':
            dataLayer.push({
                'event': 'gaEvent',
                'gaEventCategory': filmInfo[loaded].title,
                'gaEventAction': 'Video Paused',
                'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1]),
                'gaEventValue': videos_status[e.target.id].current
            });
            break;
            // If the user ends playing the video, an Finish video will be pushed ( This equals to % played = 100 )  
        case 'ended':
            dataLayer.push({
                'event': 'gaEvent',
                'gaEventCategory': filmInfo[loaded].title,
                'gaEventAction': 'Video Finished',
                'gaEventLabel': decodeURIComponent(e.target.currentSrc.split('/')[e.target.currentSrc.split('/').length - 1])
            });
            break;
        default:
            break;
        }
    }
    // We need to configure the listeners
    // Let's grab all the the "video" objects on the current page     
    var videos = document.getElementsByTagName('video');
    for (var i = 0; i < videos.length; i++) {
        // In order to have some id to reference in our status object, we are adding an id to the video objects
        // that don't have an id attribute. 
        var videoTagId;
        if (!videos[i].getAttribute('id')) {
            // Generate a random alphanumeric string to use is as the id
            videoTagId = 'html5_video_' + Math.random().toString(36).slice(2);
            videos[i].setAttribute('id', videoTagId);
        }// Current video has alredy a id attribute, then let's use it <img draggable="false" class="emoji" alt="?" src="https://s.w.org/images/core/emoji/2/svg/1f642.svg">
        else {
            videoTagId = videos[i].getAttribute('id');
        }
        // Video Status Object declaration  
        videos_status[videoTagId] = {};
        // We'll save the highest percent mark played by the user in the current video.
        videos_status[videoTagId].greatest_marker = 0;
        // Let's set the progress markers, so we can know afterwards which ones have been already sent.
        videos_status[videoTagId]._progress_markers = {};
        for (j = 0; j < 100; j++) {
            videos_status[videoTagId].progress_point = divisor * Math.floor(j / divisor);
            videos_status[videoTagId]._progress_markers[videos_status[videoTagId].progress_point] = false;
        }
        // On page DOM, all players currentTime is 0 
        videos_status[videoTagId].current = 0;
        // Now we're setting the event listeners. 
        videos[i].addEventListener("play", eventHandler, false);
        videos[i].addEventListener("pause", eventHandler, false);
        videos[i].addEventListener("ended", eventHandler, false);
        videos[i].addEventListener("timeupdate", eventHandler, false);
        videos[i].addEventListener("ended", eventHandler, false);
    }
})();
</script>

</body>
</html>